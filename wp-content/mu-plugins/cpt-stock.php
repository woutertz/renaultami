<?php
/*
Plugin Name: Stock Custom Post Type
Plugin URI: https://automotivemarketinginnovators.com
Description: Custom post type for Renault AMI
Author: Wouter Van de Wille
Version: 1.0
Author URI: http://www.wouterwideweb.be*/


/**
 *
 * Register post type
 *
 */

function register_stock_type()
{
    $label_singular = 'Stock Car';
    $label_plural = 'Stock Cars';

    register_post_type(
        'stock',
        array(
            'label' => $label_plural,
            'description' => '',
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'query_var' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'stock',
                'with_front' => false,
            ),
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail',
                'custom-fields',
            ),
            'labels' => array(
                'name' => $label_plural,
                'singular_name' => $label_singular,
                'menu_name' => $label_plural,
                'add_new' => 'Add New',
                'add_new_item' => 'Add New ' . $label_singular,
                'edit' => 'Edit',
                'edit_item' => 'Edit ' . $label_singular,
                'new_item' => 'New ' . $label_singular,
                'view' => 'View ' . $label_singular,
                'view_item' => 'View ' . $label_singular,
                'search_items' => 'Search ' . $label_plural,
                'not_found' => 'No ' . $label_plural . ' Found',
                'not_found_in_trash' => 'No ' . $label_plural . ' Found in Trash',
                'parent' => 'Parent ' . $label_singular,
            )
        )
    );
}

add_action('init', 'register_stock_type');




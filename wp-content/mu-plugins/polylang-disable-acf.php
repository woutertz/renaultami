<?php
/**
 * Disables ACF to fix Polylang Pro breaking the cloned Field groups
 */
add_action(
    'after_setup_theme',
    function () {
        $instance = PLL_Integrations::instance();
        if (property_exists($instance, 'acf'))
          remove_action('init', array($instance->acf, 'init'));
    },
    20
);

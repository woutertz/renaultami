<?php

namespace App;

use App\Controllers\Stock\Carflow;
use App\Controllers\Stock\Fastback;
use App\Controllers\Stock\StockController;
use App\Controllers\Stock\StockVehicle;
use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {

    $api_key = WP_ENV === 'production' ? 'AIzaSyA11MBVJDR5ExHW1rj46RhlWfPvgapqrPI' : '';
    wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . $api_key, [], null, true);

    wp_enqueue_style('sage/main.css', asset_path('styles/' . get_theme_mod('theme_brand_setting') . '.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    if (env('WP_ENV') !== 'production') {
        wp_enqueue_script('sage/subtheme.js', asset_path('scripts/' . get_theme_mod('theme_brand_setting') . '.js'), ['sage/main.js'], null, true);
    }

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'topbar_navigation' => __('Topbar Navigation', 'sage'),
        'mobile_navigation' => __('Mobile Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ];
    register_sidebar([
            'name' => __('Primary', 'sage'),
            'id' => 'sidebar-primary'
        ] + $config);
    register_sidebar([
            'name' => __('Footer', 'sage'),
            'id' => 'sidebar-footer'
        ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});


/**
 *Google maps API key for ACF
 */
add_action('acf/init', function () {
    $api_key = WP_ENV === 'production' ? 'AIzaSyA11MBVJDR5ExHW1rj46RhlWfPvgapqrPI' : '';
    acf_update_setting('google_api_key', $api_key);
    add_option('stock_integration_last_sync', 0);
});

function registerWhoops()
{
    $whoops = new \Whoops\Run;
    $handler = new \Whoops\Handler\PrettyPageHandler;

    $handler->setEditor('phpstorm');
    $whoops->pushHandler($handler);
    $whoops->register();
}

// if( WP_DEBUG ) registerWhoops();

/**
 * Ami backend white labeling
 */
add_action('login_enqueue_scripts', function () {
    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-ami.png');
            height: 150px;
            width: 150px;
            background-size: contain;
            background-repeat: no-repeat;
            padding-bottom: 10px;
        }
    </style>
<?php });


add_filter('login_headerurl', function () {
    return home_url();
});


add_filter('login_headertext', function () {
    return 'Automotive Marketing Innovators';
});

add_action('wp_before_admin_bar_render', function () {
    ?>
    <style type="text/css">
        body #wpadminbar #wp-admin-bar-my-sites > .ab-item:before {
            content: "";
            top: 2px;
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-ami-white.png') !important;
            display: block;
            width: 32px;
            height: 32px;
            background-size: 80% auto;
            background-position: center center;
            background-repeat: no-repeat;
        }

        body #wpadminbar .quicklinks li .blavatar {
            display: none;
        }
    </style>
<?php });

// Add Caldera forms
foreach (glob(get_template_directory() . '/forms/*.php') as $form) {
    require_once($form);
}

/**
 * ACF Button: remove_stock
 */
add_action('acfe/fields/button/name=remove_stock', function ($field, $post_id) {
    StockController::removeStockCars();

    wp_send_json_success('Success!');
}, 10, 2);

/**
 * ACF Button: stock_integration_sync_now
 */
add_action('acfe/fields/button/name=stock_integration_sync_now', function ($field, $post_id) {
    ami_start_stock_sync();

    wp_send_json_success('Success!');
}, 10, 2);

/**
 * Caldera forms on Submit Complete
 */
add_action(
    'caldera_forms_submit_complete',
    function ($form) {

        // put form field data into an array $data
        $form_data = array();
        foreach ($form['fields'] as $field_id => $field) {
            $form_data[$field['slug']] = \Caldera_Forms::get_field_data($field_id, $form);
        }

        // get concerning post
        $post = array_key_exists('post', $form_data) ? get_post($form_data['post']) : null;
        $isVO = $post && $post->post_type == 'stock';

        //  ugly but don't want to mess anything up lolzz
        $marketingoptin = strtolower($form_data['marketingoptin']) == 'y' ? 'Y' : 'N';

        // create base data
        $client = [
            'title' => $form_data['title'],
            'firstName' => $form_data['firstname'],
            'lastName' => $form_data['lastname'],
            'language' => strtoupper(pll_current_language('slug')),
            'email' => $form_data['email'],
            'emailMarketingOptin' => $marketingoptin,
            'mobilePhone' => $form_data['mobilephone'],
            'phoneMarketingOptin' => $marketingoptin,
            'termOfUseAgreement' => $form_data['tosoptin'] != null
        ];

        $leadInfo = [
            'context' => get_site_url(),
            'leadCountry' => 'Belgium',
            'leadFormName' => 'Dealer_' . get_field('dealer_name', 'options'),
            'leadProvider' => 'AMI',
            'leadSource' => 'INTERNET',
            'platformBrand' => strtoupper(get_theme_mod('theme_brand_setting')),
            'typeOfInterest' => $isVO ? 'VO' : 'VN',
        ];
        $dealer = [];

        switch (strtolower($form['name'])) {
            case 'quote':
            case 'offer':
            case 'custom quote':
                $leadInfo['subTypeOfInterest'] = 'Quote request';
                break;
            case 'test ride':
            case 'testride':
            case 'test_ride':
            case 'custom test ride':
                $leadInfo['subTypeOfInterest'] = 'TestDrive Request';
                break;
            case 'contact':
                $leadInfo['typeOfInterest'] = 'APV';
                $leadInfo['subTypeOfInterest'] = 'After Sales Request';
                $client['comment'] = $form_data['question'];
                break;
            case 'atelier':
                $leadInfo['typeOfInterest'] = 'APV';
                $leadInfo['subTypeOfInterest'] = 'After Sales Request';
                $date = strtotime($form_data['date']);
                $client['appointmentDate'] = date('Y-m-d', $date);
                $client['appointmentStartTime'] = $form_data['time'];
                break;
            default:
                return;
        }

        $bir = get_field('bir_number', $form_data['location']);

        if (!$bir) {
            $concession = get_field('primary_concession', 'options');
            $bir = get_field('bir_number', $concession);
        }

        if (!$bir) {
            return;
        }

        $dealer['dealerOfInterest'] = $bir;


        $data = [
            'client' => $client,
            'leadInfo' => $leadInfo,
            'dealer' => $dealer
        ];

        if ($leadInfo['typeOfInterest'] != 'APV') {

            $data['vehicle'] = [
                'brandOfInterest' => $isVO ? get_field('make', $post) : title_case(get_theme_mod('theme_brand_setting')),
            ];
            $data['vehicle']['modelOfInterest'] = $post->post_title;

            if ($isVO) {
                $data['client']['comment'] = get_the_permalink($post);
            }
        }

        error_log('RLeads Request sending: ' . json_encode($data));
        if (WP_ENV !== 'production') {
            error_log('[Disabled] Not sending Rleads request on non-production applications');
            return;
        }

        $response = wp_remote_post(RLEADS_URL, [
            'method' => 'POST',
            'timeout' => 45,
            'redirection' => 5,
            'blocking' => true,
            'headers' => [
                'APIKEY' => RLEADS_KEY,
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode($data),
            'cookies' => []
        ]);

        error_log('RLeads response: ' . json_encode($response));

        // todo find a way to show that something went wrong in the frontend.
        if (is_wp_error($response) || $response['response']['code'] >= 300) {
            error_log('RLeads: COULD NOT SEND MESSAGE TO RLEADS');
            return $response['body']['message'] || __('Something went wrong.', 'ami');
        }
    },
    55
);

/**
 * Add the Stock synchronization action
 */
add_action('ami_cron_stock_sync', function () {
    ami_start_stock_sync();
});

/**
 * If not scheduled yet, schedule a stock sync every hour (but only trigger at 12:00 and 20:00 from server cron)
 */
add_action('init', function () {
    if (!wp_next_scheduled('ami_cron_stock_sync')) {
        wp_schedule_event(time(), 'hourly', 'ami_cron_stock_sync');
    }
});

/**
 *  Unschedule the cron when deactivating the plugin
 */
register_deactivation_hook(__FILE__, function () {
    $timestamp = wp_next_scheduled('ami_cron_stock_sync');
    wp_unschedule_event($timestamp, 'ami_cron_stock_sync');
});

add_action('rest_api_init', function () {
    register_rest_route('ami/v1', '/concession/(?P<id>\d+)/phone', [
        'method' => 'GET',
        'callback' => 'App\rest_concession_get_phone'
    ]);
    register_rest_route('ami/v1', '/model/(?P<id>\d+)/thumbnail', [
        'method' => 'GET',
        'callback' => 'App\rest_model_get_thumbnail'
    ]);
    register_rest_route('ami/v1', '/clear_transients', [
        'method' => 'GET',
        'callback' => 'App\rest_clear_transients'
    ]);
});

function rest_concession_get_phone(\WP_Rest_Request $request)
{
    $concession_id = (int)$request['id'];
    return get_field('phone', $concession_id);
}

function rest_model_get_thumbnail(\WP_Rest_Request $request)
{
    $id = $request['id'];
    return get_the_post_thumbnail_url($id);
}

function rest_clear_transients()
{
//            WHERE `option_name` LIKE '%transient_%'
    global $wpdb;
    $sql = "SELECT `option_name` AS `name`
            FROM  $wpdb->options
            WHERE `option_name` LIKE '%options%'
            ORDER BY `option_name`";

    $results = $wpdb->get_results($sql);

    return $results;
    $transients = collect($results)
        ->filter(fn($v) => 0 !== strpos($v->name, '_transient_timeout_'))
        ->map(fn($v) => str_replace('_transient_', '', $v->name))
        ->toArray();

    foreach ($transients as $transient)
        delete_transient($transient);

    return $transients;
}

/**
 * Check for migrations
 */
add_action('init', function () {
    require_once 'acf_migrations/version.php';
    $last_synced_version = get_option('acf_migration_version');
    $downgrade = $last_synced_version && $last_synced_version >= $ACF_MIGRATE_VERSION;

    if ($last_synced_version == $ACF_MIGRATE_VERSION) return;

    error_log(sprintf('Migrating acf fields %s -> %s', $last_synced_version, $ACF_MIGRATE_VERSION));
    update_option('acf_migration_version', $ACF_MIGRATE_VERSION);

    try {
        foreach (glob(get_template_directory() . '/../app/acf_migrations/20*.php') as $file) {
            include_once $file;
            if ($VERSION > $last_synced_version) up();
            elseif ($downgrade && $VERSION > $ACF_MIGRATE_VERSION && $VERSION <= $last_synced_version) down();
        }
    } catch (\Exception $e) {
        update_option('acf_migration_version', $last_synced_version);
    }
});

/**
 * Disable certain fields to everyone but admins
 */
add_filter('acf/prepare_field', function ($field) {
    if (acf_is_admin_field_only($field) && !is_admin()) {
        $field['readonly'] = 1;
        $field['instructions'] = sprintf('<span style="color: red">%s</span>', pll__('Dit veld is enkel voor administrators.'));
    }

    return $field;
});

add_filter('acf/validate_value', function ($valid, $value, $field, $input_name) {
    if (acf_is_admin_field_only($field) && !is_admin()) {
        if ($value != get_field($field['key'], $_POST['post_ID']))
            return __('Je bent niet gemachtigd dit veld aan te passen.', 'jointswp');
    }
    return $valid;
}, 10, 4);


<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory() . '/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory() . '/index.php';
    }

    return $comments_template;
}, 100);

/**
 * FacetWP Query fix
 */
add_filter('facetwp_is_main_query', function ($is_main_query, $query) {
    if ('concession' == $query->get('post_type')) {
        $is_main_query = false;
    }
    return $is_main_query;
}, 10, 2);


/**
 ** output only total results
 **/

add_filter('facetwp_result_count', function ($output, $params) {
    $output = $params['total'];
    return $output;
}, 10, 2);


add_filter('facetwp_sort_options', function ($options, $params) {
    unset($options['date_desc']);
    unset($options['date_asc']);
    unset($options['title_desc']);
    unset($options['title_asc']);

    $options['meta_value_num_asc'] = array(
        'label' => pll__('Kilometerstand (Minste)'),
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'mileage',
            'order' => 'ASC',
        )
    );
    $options['meta_value_num_desc'] = array(
        'label' => pll__('Kilometerstand (Meeste)'),
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'mileage',
            'order' => 'DESC',
        )
    );

    $options['year_desc'] = array(
        'label' => pll__('Jaar (Nieuwste)'),
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'year',
            'order' => 'DESC',
        )
    );
    $options['year_asc'] = array(
        'label' => pll__('Jaar (Oudste)'),
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'year',
            'order' => 'ASC',
        )
    );
    $options['price_desc'] = array(
        'label' => pll__('Prijs (Hoogste)'),
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'price',
            'order' => 'DESC',
        )
    );
    $options['price_asc'] = array(
        'label' => pll__('Prijs (Laagste)'),
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'price',
            'order' => 'ASC',
        )
    );
    return $options;
}, 10, 2);


add_filter('caldera_forms_phone_js_options', function ($options) {
    //Use ISO_3166-1_alpha-2 formatted country code
    $options['initialCountry'] = 'BE';
    return $options;
});


add_filter('facetwp_facet_sources', function ($sources) {
    $sources['posts']['choices']['post_name'] = 'Post Slug';
    return $sources;
});

/*
* Model dropdown image and title
* Example to auto populate an option based field in caldera forms
 * This example uses a by slug. method. this way you can populate the
 * field by simply giving it the desired slug. in this example: gdp
 *
 * To populate based on type where {type} is dropdown, radio, checkbox etc...
 * add_filter('caldera_forms_render_get_field_type-{type}', 'cf_get_data_json');
 */
// TODO: Clean up, double shit with one big 'caldera forms render get field' filter


/**
 * Renders models in test ride form dropdown
 */
add_filter('caldera_forms_render_get_field_slug-model_dropdown_test_ride', function ($field) {


    $models = get_field('models_dropdown', 'options');

    if (!empty($models)) {
        foreach ($models as $model) {
            $field['config']['option'][$model] = array(
                'value' => $model,
                'label' => get_the_title($model)
            );
        }
    } else {
        $models = get_posts(array(
            'post_type' => 'model',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        ));
        if (!empty($models)) {
            foreach ($models as $model) {
                $field['config']['option'][$model->ID] = array(
                    'value' => $model->ID,
                    'label' => $model->post_title
                );
            }
        }
    }

    // return the field to the form
    return $field;
});

// Add filter on the fetch field config to populate
add_filter('caldera_forms_render_get_field_slug-model_dropdown_quote', function ($field) {

    $models = get_field('models_dropdown', 'options');

    if (!empty($models)) {
        foreach ($models as $model) {
            $field['config']['option'][$model] = array(
                'value' => $model,
                'label' => get_the_title($model)
            );
        }
    } else {
        $models = get_posts(array(
            'post_type' => 'model',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        ));
        if (!empty($models)) {
            foreach ($models as $model) {
                $field['config']['option'][$model->ID] = array(
                    'value' => $model->ID,
                    'label' => $model->post_title
                );
            }
        }
    }

    // return the field to the form
    return $field;
});

/**
 * Limit remark field of the contact form
 */
add_filter( 'caldera_forms_field_attributes', function( $attrs, $field, $form ){
    if( 'fld_5544254' == $field['ID'] ){
        $attrs[ 'maxlength' ] = 4096;
    }

    return $attrs;

}, 20, 3 );

add_filter('facetwp_facet_render_args', function ($args) {

    $new_args = false;

    if ('condition' == $args['facet']['name']) {

        $translations = array(
            'New' => pll__('New'),
            'Second Hand' => pll__('Second Hand'),
            'Demo' => pll__('Demo')
        );

        $new_args = true;
    }

    if ('transmission' == $args['facet']['name']) {

        $translations = array(
            'manual' => pll__('manual'),
            'automatic' => pll__('automatic')
        );

        $new_args = true;
    }

    if (in_array($args['facet']['name'], ['fuel', 'stock_box_fr_fuel', 'stock_box_nl_fuel'])) {

        $translations = array(
            'benzine' => pll__('benzine'),
            'diesel' => pll__('diesel')
        );

        $new_args = true;
    }

    if ('price' == $args['facet']['name']) {
        if (pll_current_language() == 'fr') {
            $args['facet']['suffix'] = $args['facet']['prefix'];
            $args['facet']['prefix'] = '';
        }
    }

    if ($new_args) {
        if (!empty($args['values'])) {
            foreach ($args['values'] as $key => $val) {
                $display_value = $val['facet_display_value'];
                if (isset($translations[$display_value])) {
                    $args['values'][$key]['facet_display_value'] = $translations[$display_value];
                }
            }
        }
    }

    return $args;
});


// add_filter( 'the_seo_framework_rel_canonical_output', 'test' );

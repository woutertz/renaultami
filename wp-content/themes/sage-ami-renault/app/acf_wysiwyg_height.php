<?php
/**
 * Add height field to ACF WYSIWYG
 */
//
//add_action('acf/render_field_settings/type=wysiwyg', function ($field) {
//    acf_render_field_setting($field, array(
//        'label' => __('Height of Editor'),
//        'instructions' => __('Height of Editor after Init'),
//        'name' => 'wysiwyg_height',
//        'type' => 'number',
//    ));
//});


/**
 * Render height on ACF WYSIWYG
 */
add_action('acf/render_field/type=wysiwyg', function ($field) {
    $field_class = '.acf-' . str_replace('_', '-', $field['key']);
    ?>
    <style type="text/css">
        <?php echo $field_class; ?>
        iframe {
            min-height: 100px;
            height: 150px;
        }
    </style>
    <script type="text/javascript">
    jQuery(window).load(function () {
        jQuery('<?php echo $field_class; ?>').each(function () {
            jQuery('.mce-edit-area iframe').height( 100 );
        });
    });
    </script>
    <?php
});

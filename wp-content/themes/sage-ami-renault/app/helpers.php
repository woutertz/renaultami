<?php

namespace App;

use App\Controllers\Stock\StockController;
use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                    ];
                })
                ->concat([
                    "{$template}.blade.php",
                    "{$template}.php",
                ]);
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

/**
 * Stock integration sync
 */
function ami_start_stock_sync()
{
    StockController::log(sprintf('[%s] Start Stock Integration: %s', date("m F H:i"), get_field('dealer_name', 'options')));

    $todo = get_transient('stock_sync_todo');
    if (!$todo) {
        error_log('[Transient main] refetching integrations todo');
        $todo = get_field('stock_integrations', 'options');
        set_transient('stock_sync_todo', $todo);
    }

    foreach ($todo as $key => $integration) {
        switch ($integration['type']) {
            case 'fastback':
                StockController::log('- Sync FastBack');
                StockController::syncFromFastback($integration['fastback_url'], $integration['concessions']);
                break;
            case 'carflow':
                StockController::log('- Sync Carflow');
                StockController::syncFromCarflow(
                    $integration['languages'],
                    $integration['concessions']
                );
                break;
            case 'renaultmotors':
                StockController::log('- Sync RenaultMotors');
                StockController::syncFromRenaultMotors();
                break;
            default:
                StockController::log('- No integration selected');
        }
        unset($todo[$key]);
        set_transient('stock_sync_todo', $todo);
    }

    delete_transient('stock_sync_todo');
}

/**
 * Get language independant permalink
 */
function pll_permalink($slug, $fragments = '')
{
    return get_the_permalink(
            pll_get_post(
                get_page_by_path($slug)->ID
            )
        ) . $fragments;
}

function is_admin()
{
    $user = wp_get_current_user();
    return in_array('administrator', (array)$user->roles);
}

/**
 * Check if this field is admin only
 */
function acf_is_admin_field_only($field)
{
    return in_array($field['key'], [
        'field_5f6c7a6db56c5',
        'field_5fae41061d8da',
        'field_5fae41101d8db'
    ]);
}

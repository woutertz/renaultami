<?php

$VERSION = 1;

/**
 * Migrate the Stock Integrations to a repeater
 */
function up()
{
    migrate_stock_integrations();
    migrate_stock_model_integration_id();
}

/**
 * Migrate the stock integrations repeater to a single value
 */
function down()
{
    migrate_stock_integrations(false);
    migrate_stock_model_integration_id(false);
}

function migrate_stock_integrations($up = true)
{
    if ($up) {
        // up
        $data = [
            'type' => get_field('stock_integration_type', 'options'),
            'fastback_url' => get_field('fastback_url', 'options'),
            'carflow_implementation' => get_field('carflow_implementation', 'options'),
            'concessions' => get_field('concession_integration_ids', 'options'),
            'carflow_function_code' => get_field('carflow_function_code', 'options'),
            'languages' => get_field('stock_languages', 'options'),
        ];

        if ($data['type'] == 'wordpress')
            return;

        update_field('stock_integrations', [$data], 'options');
    } else {
        // down
        $repeater = get_field('stock_integrations', 'options');
        if (!$repeater || $repeater[0]['type'] == 'wordpress') return;

        update_field('stock_integration_type', $repeater[0]['type'], 'options');
        update_field('fastback_url', $repeater[0]['fastback_url'], 'options');
        update_field('carflow_implementation', $repeater[0]['carflow_implementation'], 'options');
        update_field('concession_integration_ids', $repeater[0]['concessions'], 'options');
        update_field('carflow_function_code', $repeater[0]['carflow_function_code'], 'options');
        update_field('stock_languages', $repeater[0]['languages'], 'options');
    }

}

function migrate_stock_model_integration_id($up = true)
{
    $integrations = get_field('stock_integrations', 'options');
    if (
        get_field('stock_integration_type', 'options') === 'wordpress'
        || ($integrations && $integrations[0]['type'] === 'wordpress')
    )
        return;

    $query = new \WP_Query([
        'post_type' => 'stock',
        'posts_per_page' => -1,
    ]);

    foreach ($query->posts as $stock) {
        if ($up) {
            update_field(
                'integration_id',
                get_field('stock_integration_type', 'options') . '-' . get_field('fastback_id', $stock),
                $stock
            );
        } else {
            $id = explode('-',
                get_field('integration_id', $stock)
            );

            if (count($id) > 1)
                update_field('fastback_id', last($id), $stock);
        }
    }
}


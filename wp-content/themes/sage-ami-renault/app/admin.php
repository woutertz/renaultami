<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

add_action("wp_ajax_sync_carflow", __NAMESPACE__ . "\\save_carflow_file");
add_action("wp_ajax_nopriv_sync_carflow", __NAMESPACE__ . "\\save_carflow_file");

function save_carflow_file()
{
    file_put_contents(
        sprintf('/tmp/carflow_%s.xml', strtolower(trim(get_field('dealer_name', 'options')))),
        stripcslashes($_POST['vehicles'])
    );

    update_option('stock_integration_last_sync', microtime(true) * 1000);

    return wp_send_json('Alrighty');
}

<?php

namespace App;

add_action('customize_register', function () {

    global $wp_customize;

    // Theme variation setting
    class Select_Control extends \WP_Customize_Control
    {
        public $type = 'select';

        public function render_content()
        {
            $themes = array(
                'renault' => 'Renault',
                'dacia' => 'Dacia',
            );
            ?>
            <label>
                <span class="customize-control-title">
                    <?php echo esc_html($this->label); ?>
                </span>
                <select <?php $this->link(); ?>>
                    <?php foreach ($themes as $theme => $name) { ?>
                        <option
                            value="<?= $theme ?>" <?php if ($this->value() == $theme) echo 'selected="selected"'; ?>>
                            <?= $name ?>
                        </option>
                    <?php } ?>
                </select>
            </label>
            <?php
        } //render_content()
    } //Select_Control()

    $wp_customize->add_section('dealership_settings', array('title' => 'Dealership Settings'));

    $wp_customize->add_setting('theme_brand_setting', array('default' => 'renault'));

    $wp_customize->add_control(new Select_Control($wp_customize, 'theme_brand_setting', array(
        'label' => 'Brand',
        'section' => 'dealership_settings',
        'setting' => 'theme_brand_setting'
    )));
});

<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class SingleStock extends Controller
{
    public function specs()
    {

        global $post;

        $specifications_group_id = 182; // Post ID of the specifications field group.
        $specifications_fields = [];


        $fields = acf_get_fields($specifications_group_id);

        foreach ($fields as $field) {
            if ($field['name'] == 'technical_spec_groups') {
                $specifications_fields = $field['sub_fields'];
            }
        }

        return App::array_to_object($specifications_fields);

    }

    public function technicalSpecs()
    {
        $specs = get_field('technical_spec_groups');

        return $specs;
    }
}

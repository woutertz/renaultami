<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class TemplateCompare extends Controller
{
    public function comparedCars()
    {
        if ($cars = filter_input(INPUT_GET, 'q', FILTER_SANITIZE_STRING)) {
            $cars_array = explode(',', $cars);

            $query = new WP_Query(array(
                'post_type' => 'stock',
                'posts_per_page' => -1,
                'post__in' => $cars_array
            ));

            $cars_return = [];

            foreach ($query->posts as $i => $car) {
                $car = [
                    'id' => $car->ID,
                    'fields' => get_fields($car->ID)
                ];

                array_push($cars_return, $car);
            }

            return count($cars_return) ? $cars_return : false;
        } else {
            return false;
        }
    }

    public function specs()
    {

        global $post;

        $specifications_group_id = 182; // Post ID of the specifications field group.
        $specifications_fields = [];


        $fields = acf_get_fields($specifications_group_id);

        foreach ($fields as $field) {
            if ($field['name'] == 'technical_spec_groups') {
                $specifications_fields = $field['sub_fields'];
            }
        }

        return App::array_to_object($specifications_fields);

    }
}

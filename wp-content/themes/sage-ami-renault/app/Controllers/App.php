<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use function App\asset_path;
use WP_Query;

class App extends Controller
{

    protected $acf = true;


    public function brandLogo()
    {
        return asset_path('images/' . get_theme_mod('theme_brand_setting') . '/logo.svg');
    }

    public function markerIcon()
    {
        return asset_path('images/' . get_theme_mod('theme_brand_setting') . '/map-marker.svg');
    }

    public function brandName()
    {
        return ucfirst(get_theme_mod('theme_brand_setting'));
    }

    public function randomModel()
    {
        $query = new WP_Query(array(
            'post_type' => 'model',
            'posts_per_page' => -1,
            'fields' => 'ids',
            'post_status' => 'publish',
            'orderby' => 'rand'
        ));

        return ($query->posts[0]);
    }

    public function options()
    {
        $options = new \stdClass();

        // Todo: Refactor
        $options->sitemap = get_field('sitemap', 'options');
        $options->button_link = get_field('button_link', 'options');
        $options->copyright_text = get_field('copyright_text', 'options');
        $options->models_navigation = get_field('models_navigation', 'options');
        $options->models_page = get_field('models_page', 'options');
        $options->contact_page = get_field('contact_page', 'options');
        $options->stock_page = get_field('stock_page', 'options');
        $options->quote_form_shortcode = get_field('quote_form_shortcode', 'options');
        $options->test_ride_form_shortcode = get_field('test_ride_form_shortcode', 'options');
        $options->takeover_url = get_field('takeover_url', 'options');
        $options->models_mobile_navigation = get_field('models_mobile_navigation', 'options');
        $options->contact_form_image = get_field('contact_form_image', 'options');
        $options->atelier_form_image = get_field('atelier_form_image', 'options');
        $options->quote_form_text = get_field('quote_form_text', 'options');
        $options->quote_form_image = get_field('quote_form_image', 'options');
        $options->test_ride_form_text = get_field('test_ride_form_text', 'options');
        $options->test_ride_form_image = get_field('test_ride_form_image', 'options');
        $options->legal_fallback = get_field('legal_fallback', 'options');
        $options->legal_fixed = get_field('legal_fixed', 'options');
        $options->legal_fixed_optional = get_field('legal_fixed_optional', 'options');
        $options->form_disclaimer = get_field('form_disclaimer', 'options');
        $options->compare_page = get_field('compare_page', 'options');
        $options->alternative_brand_link = get_field('alternative_brand_link', 'options');
        $options->financial_disclaimer_fallback = get_field('financial_disclaimer_fallback', 'options');
        $options->carflow_plugin = get_field('stock_integration_type', 'options') == 'carflow' &&
            get_field('carflow_implementation', 'options') == 'plugin';
        $options->stock_last_sync = get_option('stock_integration_last_sync');
        $options->carflow_function_code = get_field('carflow_function_code', 'options');
        $options->ajax_url = admin_url('admin-ajax.php');
        $options->dealer_renault_stock_name = get_field('dealer_renault_stock_name', 'options');
        $options->live_chat_link = get_field('live_chat_link', 'options');
        $options->modals_hide_phonenumber = get_field('modals_hide_phonenumber', 'options');

        return self::array_to_object($options);
    }

    public function dealership()
    {
        $dealership = new \stdClass();

        $dealership->name = get_field('dealer_name', 'options');
        $dealership->logo = get_field('dealer_logo', 'options');
        $dealership->social = get_field('dealer_social', 'options');
        $dealership->primary_concession = get_field('primary_concession', 'options');
        $dealership->header_script = get_field('header_script', 'options');
        $dealership->body_script = get_field('body_script', 'options');
        // $dealership->opening_hours = get_field('opening_hours_menu', $dealership->primary_concession);

        return self::array_to_object($dealership);
    }

    public function openingHoursToday()
    {
        $primary_concession = get_field('primary_concession', 'options');
        $opening_hours = get_field('opening_hour_sets', $primary_concession);
        $day_index = date("N");
        $today = self::array_to_object($opening_hours[0]['opening_hours_menu'][$day_index - 1]);

        $opening_hours_text = '';

        if ($today->time_blocks) {
            foreach ($today->time_blocks as $block) {
                $opening_hours_text .= $block->opening_time . ' - ' . $block->closing_time;

                if (next($today->time_blocks)) {
                    $opening_hours_text .= ' & ';
                }
            }
        }

        $today->opening_text = $opening_hours_text;

        return $today;
    }

    public function openingHoursAll()
    {
        $primary_concession = get_field('primary_concession', 'options');
        $opening_hours_sets = get_field('opening_hour_sets', $primary_concession);
        $opening_hours_sets = self::array_to_object($opening_hours_sets);


        foreach ($opening_hours_sets as $set) {

            foreach ($set->opening_hours_menu as $day) {
                $opening_hours_text = '';
                if ($day->open && $day->time_blocks) {
                    foreach ($day->time_blocks as $block) {
                        $opening_hours_text .= $block->opening_time . ' - ' . $block->closing_time;

                        if (next($day->time_blocks)) {
                            $opening_hours_text .= '<br/>';
                        }
                    }
                    $day->opening_hours_text = $opening_hours_text;
                }
            }
        }

        return $opening_hours_sets;
    }

    public static function openingHoursAllByID($id)
    {
        $opening_hours_sets = get_field('opening_hour_sets', $id);
        $opening_hours_sets = self::array_to_object($opening_hours_sets);


        foreach ($opening_hours_sets as $set) {

            foreach ($set->opening_hours_menu as $day) {
                $opening_hours_text = '';
                if ($day->open && $day->time_blocks) {
                    foreach ($day->time_blocks as $block) {
                        $opening_hours_text .= $block->opening_time . ' - ' . $block->closing_time;

                        if (next($day->time_blocks)) {
                            $opening_hours_text .= '<br/>';
                        }
                    }
                    $day->opening_hours_text = $opening_hours_text;
                }
            }
        }

        return $opening_hours_sets;
    }


    public function allConcessions()
    {
        $query = new WP_Query(array(
            'post_type' => 'concession',
            'posts_per_page' => -1,
            'fields' => 'ids',
            'post_status' => 'publish',
            'orderby' => 'menu_order'
        ));
        return ($query->posts);
    }

    public function hasOneConcession()
    {
        return count($this->allConcessions()) <= 1;
    }

    /**
     * @param $index
     * @return mixed
     */
    public static function day_of_week($index)
    {
        $days = [
            'nl' =>
                [
                    'Maandag',
                    'Dinsdag',
                    'Woensdag',
                    'Donderdag',
                    'Vrijdag',
                    'Zaterdag',
                    'Zondag'
                ],
            'fr' =>
                [
                    'Lundi',
                    'Mardi',
                    'Mercredi',
                    'Jeudi',
                    'Vendredi',
                    'Samedi',
                    'Dimanche'
                ]
        ];

        return $days[pll_current_language()][$index];
    }

    public static function get_translation_url($lang)
    {
        $translations = pll_the_languages(array('raw' => 1));
        return $translations[$lang]['url'];
    }

    /**
     * @param $array
     * @return object
     */
    public
    static function array_to_object($array)
    {
        return json_decode(json_encode($array));
    }

    /**
     * @param $string
     * @return string
     */
    public
    static function wysiwyg_strip($string)
    {
        return strip_tags(
            html_entity_decode($string), // first decode &lt;br$gt; to <br>
            '<strong><br><sup>'
        );
    }

    /**
     * @param $number
     * @return string
     */
    public
    static function number_to_money($number)
    {
        if (pll_current_language() == 'nl') {
            return '€ ' . number_format($number, 0, ',', '.');
        }
        return number_format($number, 0, ',', '.') . ' €';
    }

    /**
     * @param $string
     * @return string|string[]
     */
    public
    static function sluggify($string)
    {
        return preg_replace('/\s*/m', '', strtolower(strip_tags(htmlspecialchars_decode($string))));
    }

    public static function non_breaking_hyphens($string)
    {
        return str_replace("-", "‑", $string);
    }


    public
    function latestNews()
    {
        $query = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 2,
            'fields' => 'ids',
            'post_status' => 'publish',
            'order_by' => 'menu_order'
        ));
        return ($query->posts);
    }

    public static function getForm($name)
    {
        return sprintf('[caldera_form id="%s"]', $name);
    }

}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class TemplateArchiveStock extends Controller
{
    public function stock()
    {
        $args = array(
            'post_type' => 'stock',
            'posts_per_page' => 20,
            'facetwp' => true,
        );
        return new WP_Query($args);
    }

    public function stockFilters()
    {
        return ['make', 'model', 'condition', 'price', 'mileage', 'year', 'transmission', 'fuel'];
    }

    private function getFromFastback($url)
    {
        $response = wp_remote_get($url);
        $vehicles = json_decode(json_encode(simplexml_load_string($response['body'])), true);

        $dealers = [];

        foreach ($vehicles['vehicle'] as $vehicle) {
            $dealer = $vehicle['dealer']['dealer_name'];

            if (!starts_with(strtolower($dealer), ['renault', 'dacia']))
                continue;

            if (!key_exists($dealer, $dealers)) $dealers[$dealer] = [];
            $dealers[$dealer][] = $vehicle;
        }


        return $dealers;
    }
}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class TemplateHome extends Controller
{
    public function stock()
    {
        $args = array(
            'post_type' => 'stock',
            'posts_per_page' => -1,
            'facetwp' => true,
        );
        return new WP_Query($args);
    }

    public function stockParameters()
    {
        return str_replace(['stock_box_nl_', 'stock_box_fr_'], '', $_SERVER['QUERY_STRING']);
    }
}

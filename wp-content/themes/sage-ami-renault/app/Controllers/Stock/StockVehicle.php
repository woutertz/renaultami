<?php


namespace App\Controllers\Stock;


class StockVehicle
{
    private $id;
    private $carpass_link;
    private $concessions;
    private $condition;
    private $description;
    private $emission;
    private $image_urls;
    private $make;
    private $mileage;
    private $model;

    /**
     * @var float|null
     */
    private $old_price;
    private $price;
    private $price_advantage;
    private $price_recycling_bonus;
    private $specific_options;
    private $technical_spec_groups;
    private $type;
    private $year;

    private $language = null;

    public function getTitle(): string
    {
//        return $this->getId();
        return sprintf(
            '%s %s',
            $this->make,
            $this->model
        );
    }

    /**
     * @return String
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param String $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCarpassLink(): ?string
    {
        return $this->carpass_link;
    }

    /**
     * @param string $carpass_link
     */
    public function setCarpassLink($carpass_link): void
    {
        $this->carpass_link = (string)$carpass_link;
    }

    /**
     * @return mixed
     */
    public function getConcessions(): ?array
    {
        return $this->concessions;
    }

    /**
     * @param array $concessions
     */
    public function setConcessions(array $concessions): void
    {
        $this->concessions = $concessions;
    }

    /**
     * @return string
     */
    public function getCondition(): ?string
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     */
    public function setCondition($condition): void
    {
        $this->condition = (string)$condition;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description): void
    {
        $this->description = (string)$description;
    }

    /**
     * @return string
     */
    public function getEmission(): ?string
    {
        return $this->emission;
    }

    /**
     * @param string $emission
     */
    public function setEmission($emission): void
    {
        $this->emission = (string)$emission;
    }

    /**
     * @return array|null
     */
    public function getImageUrls()
    {
        return $this->image_urls;
    }

    /**
     * @param array $image_urls
     */
    public function setImageUrls($image_urls): void
    {
        $this->image_urls = $image_urls;
    }

    /**
     * @return string
     */
    public function getMake(): ?string
    {
        return $this->make;
    }

    /**
     * @param string $make
     */
    public function setMake(string $make): void
    {
        $this->make = (string)$make;
    }

    /**
     * @return int
     */
    public function getMileage(): ?int
    {
        return $this->mileage;
    }

    /**
     * @param int $mileage
     */
    public function setMileage($mileage): void
    {
        $this->mileage = (int)$mileage;
    }

    /**
     * @return string
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel($model): void
    {
        $this->model = (string)$model;
    }

    /**
     * @return float|null
     */
    public function getOldPrice(): ?float
    {
        return $this->old_price;
    }

    /**
     * @param float|null $old_price
     */
    public function setOldPrice(?float $old_price): void
    {
        $this->old_price = $old_price;
    }


    /**
     * @return flaot
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price): void
    {
        $this->price = floatval($price);
    }

    /**
     * @return float|null
     */
    public function getPriceAdvantage()
    {
        return $this->price_advantage;
    }

    /**
     * @param float $price_advantage
     */
    public function setPriceAdvantage($price_advantage): void
    {
        $price_advantage = floatval($price_advantage);
        if ($price_advantage > 0) {
            $this->price_advantage = $price_advantage;
        }
    }

    /**
     * @return mixed
     */
    public function getPriceRecyclingBonus()
    {
        return $this->price_recycling_bonus;
    }

    /**
     * @param mixed $price_recycling_bonus
     */
    public function setPriceRecyclingBonus($price_recycling_bonus): void
    {
        $this->price_recycling_bonus = $price_recycling_bonus;
    }


    /**
     * @return mixed
     */
    public function getSpecificOptions()
    {
        return $this->specific_options;
    }

    /**
     * @param mixed $specific_options
     */
    public function setSpecificOptions($specific_options): void
    {
        $this->specific_options = $specific_options;
    }

    /**
     * @return array
     */
    public function getTechnicalSpecGroups(): array
    {
        return $this->technical_spec_groups ?? [];
    }

    /**
     * @param array $technical_spec_groups
     */
    public function setTechnicalSpecGroups($technical_spec_groups): void
    {
        $this->technical_spec_groups = $technical_spec_groups;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type): void
    {
        $this->type = (string)$type;
    }

    /**
     * @return string
     */
    public function getYear(): ?string
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear($year): void
    {
        $this->year = (string)$year;
    }

    /**
     * @return string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language): void
    {
        $this->language = explode(':', $language)[0];
    }


}

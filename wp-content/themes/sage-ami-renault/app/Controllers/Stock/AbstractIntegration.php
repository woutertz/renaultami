<?php

namespace App\Controllers\Stock;

abstract class AbstractIntegration
{

    abstract public function getVehicles(): array;

    public function loopXmlFile($file, $firstNode, $cb): array
    {
//        $this->log('parsing file ' . $file);
        preg_match('/_(\w\w)-be\.xml$/', $file, $matches);

        $language = count($matches) > 1 ? $matches[1] : null;

        $reader = new \XMLReader();
        $reader->open($file);

        $doc = new \DOMDocument();
        $out = [];

        // move to first vehicle node
        while ($reader->read() && $reader->name !== $firstNode) {
            continue;
        }

        while ($reader->name === $firstNode) {
            $node = simplexml_import_dom($doc->importNode($reader->expand(), true));

//            $out[] = $this->mapNodeToStockVehicle($node, $language ?? null);
            $cb($node, $language);

            $reader->next($firstNode);
        }
        $reader->close();
//        unlink($file);

        return $out;
    }
}

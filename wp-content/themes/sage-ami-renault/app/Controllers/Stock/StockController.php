<?php

namespace App\Controllers\Stock;

class StockController
{
    static function log($text)
    {
        file_put_contents(
            'stock_integration.log',
            $text . "\n",
            FILE_APPEND
        );
    }

    static function removeStockCars()
    {
        foreach (get_posts(['post_type' => 'stock', 'numberposts' => -1, 'post_status' => ['publish', 'draft']]) as $stock_to_remove) {
            wp_delete_post($stock_to_remove->ID, true);
        }
    }

    static function syncFromCarflow($languages, $concession_ids)
    {
        $vehicles = get_transient('stock_sync_carflow');

        if (!$vehicles) {
            error_log("[Transient Carflow] Reloading vehicles from file");
            $carflow = new Carflow(
                $concession_ids,
                $languages
            );

            $vehicles = $carflow->getVehicles();
        } else {
            error_log('[Transient carflow] Cars to check: ' . count($vehicles));
        }

        StockController::updateWPStock($vehicles, 'carflow');

        delete_transient('stock_sync_carflow');
    }

    static function syncFromFastback($fastback_url, $concession)
    {
        $vehicles = get_transient('stock_sync_fastback');

        if (!$vehicles) {
            error_log("[Transient Fastback] Reloading vehicles from file");
            $fb = new Fastback($fastback_url, $concession);
            $vehicles = $fb->getVehicles();
        } else {
            error_log('[Transient fastback] Cars to check: ' . count($vehicles));
        }

        StockController::updateWPStock($vehicles, 'fastback');

        delete_transient('stock_sync_fastback');
    }

    static function syncFromRenaultMotors()
    {
        $vehicles = get_transient('stock_sync_GRM');

        if (!$vehicles) {
            $stock = new RenaultMotors();
            $vehicles = $stock->getVehicles();
        }

        StockController::updateWPStock($vehicles, 'GRM');

        delete_transient('stock_sync_GRM');
    }

    static private function updateWPStock($vehicles, $type_of_integration)
    {
        $currentStock = get_transient("stock_sync_${type_of_integration}_current");

        if (!$currentStock) {
            $currentStock = StockController::getCurrentStockWithIds($type_of_integration);
            set_transient("stock_sync_${type_of_integration}_current", $currentStock);
        } else {
            error_log('[Transient current] Current stock left: ' . count($currentStock));
        }

        foreach ($vehicles as $i => $vh) {
            StockController::log(sprintf('- Checking vehicle %s (%s)', $vh->getTitle(), $vh->getId()));
            if (!key_exists($vh->getId(), $currentStock)) {
                StockController::log('-- Creating new post');
                $post_id = wp_insert_post([
                    'post_title' => $vh->getTitle(),
                    'post_type' => 'stock',
                    'post_status' => 'publish'
                ]);
                update_field('integration_id', $vh->getId(), $post_id);

            } else {
                $post_id = $currentStock[$vh->getId()];
                unset($currentStock[$vh->getId()]);
                set_transient("stock_sync_${type_of_integration}_current", $currentStock);

                StockController::log('-- Updating post');
            }

            StockController::updateStockVehicle($vh, $post_id);
            unset($vehicles[$i]);

            set_transient("stock_sync_${type_of_integration}", $vehicles);
        }

        // remove remaining stock
        StockController::log('');
        StockController::log('Removing old stock');

        foreach ($currentStock as $key => $id) {
            StockController::log('Removing ' . $key);
            wp_delete_post($id, true);
        }

        StockController::log('Updating Facet');
        FWP()->indexer->index();

        StockController::log('Done syncing');
        delete_transient("stock_sync_${type_of_integration}_current");
    }

    static private function getCurrentStockWithIds($type_of_integration): array
    {
        $current_stock = [];
        foreach (get_posts(['post_type' => 'stock', 'numberposts' => -1, 'post_status' => 'publish']) as $stock) {
            $id = get_field('integration_id', $stock->ID);

            if ($id) {
                if (str_starts_with($id, $type_of_integration)) {
                    $current_stock[$id] = $stock->ID;
                }
            } else {
                StockController::log(sprintf('- No integration id found for %s', $stock->post_title));
            }
        }

        return $current_stock;
    }


    static private function updateStockVehicle(StockVehicle $vehicle, int $post_id)
    {
        wp_update_post([
            'ID' => $post_id,
            'post_title' => $vehicle->getTitle()
        ]);

        // try to find the model by using the specified integration_name field
        $models = get_posts([
            'post_type' => 'model',
            'numberposts' => -1,
            'meta_key' => 'integration_name',
            'meta_value' => $vehicle->getModel(),
            'fields' => 'ids',
            'lang' => $vehicle->getLanguage() ?? join(',', pll_languages_list())
        ]);

        if ($models) {
            StockController::log('--> linking to model ' . json_encode($models));
            update_field('model', $models, $post_id);
        }

        // language
        if ($vehicle->getLanguage()) {
            pll_set_post_language($post_id, $vehicle->getLanguage());
            StockController::log('--> Setting language to ' . $vehicle->getLanguage());
        }

        // info
        update_field('make', $vehicle->getMake(), $post_id);
        update_field('description', $vehicle->getDescription(), $post_id);
        update_field('type', $vehicle->getType(), $post_id);
        update_field('carpass_link', $vehicle->getCarpassLink(), $post_id);
        if (!empty($vehicle->getConcessions()))
            update_field('concessions', $vehicle->getConcessions(), $post_id);

        update_field('condition', $vehicle->getCondition(), $post_id);

        update_field('year', $vehicle->getYear(), $post_id);
        update_field('mileage', $vehicle->getMileage(), $post_id);
        update_field('emission', $vehicle->getEmission(), $post_id);

        update_field('old_price', $vehicle->getOldPrice(), $post_id);
        update_field('price_advantage', $vehicle->getPriceAdvantage(), $post_id);
        update_field('price', $vehicle->getPrice(), $post_id);
        update_field('price_recycling_bonus', $vehicle->getPriceRecyclingBonus(), $post_id);

        // images
        update_field('image_urls', $vehicle->getImageUrls(), $post_id);

        $option_offset = min(10, floor(count($vehicle->getSpecificOptions()) / 2));
        update_field('car_option_sets', [
            ['title' => '', 'car_options' => array_slice($vehicle->getSpecificOptions(), 0, $option_offset)]
        ], $post_id);
        update_field('specific_options', array_slice($vehicle->getSpecificOptions(), $option_offset), $post_id);

        update_field('technical_spec_groups', $vehicle->getTechnicalSpecGroups(), $post_id);
    }
}

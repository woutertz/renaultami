<?php


namespace App\Controllers\Stock;

class RenaultMotors extends AbstractIntegration
{
    private $url = 'https://grm-test.spider-projects.com/API/Dealercom/CarsToPublish/Dealerc0m%24%E2%82%ACcreT';
    private $filename = '/tmp/renaultmotors.json';

    public function __construct()
    {
        $this->downloadFile();
    }

    private function downloadFile()
    {
        // try 3 times
        $retry = 1;
        while ($retry > 0) {
            try {
                $response = wp_remote_get($this->url)['body'];

                $response = json_decode($response);
                if ($response->Success) break;

            } catch (\Exception $e) {
                StockController::log('download failed.. Retry ' . ($retry + 1));
            }

            $retry++;
        }

        file_put_contents($this->filename, json_encode($response->Object));

//        if (!$response->Success) {
//            error_log('Could not get Renault Motors vehicles: ' . $response->ErrorMessage);
//            die(1);
//        }
    }

    /**
     * @override
     */
    public function getVehicles(): array
    {

        $data = json_decode(
            file_get_contents($this->filename)
        );

        $vehicles = [];
        for ($i = 0; $i < count($data); $i++) {
            $vehicles[] = $this->mapNodeToStockVehicle(
                $data[$i]
            );
        }

        return $vehicles;
    }

    protected function getOptions($options): array
    {
        $out = [];
        foreach ($options as $opt)
            $out[] = ['option' => (string)$opt];

        return $out;
    }

    public function getTechnicalSpecGroups($data): array
    {
        return [
            'engine' => [
//                'number_of_forward_gears' => $number_of_forward_gears[0],
//                'cylinder_capacity' => intval($data->engine_size),
//                'drive_type' => (string)$data->transmission,
                'transmission' => starts_with(strtolower($data->Transmission), 'auto') ? 'automatic' : 'manual',
//                'maximum_power_kw_hp' => sprintf('%s (%s)', $data->kw, $data->hp),
//                'engine' => (string)$data->motorisation,
//            'amount_of_cylinders' => '????',
//            'energy_label' => '??',
//            'homologation_protocol' => '??',
                'fuel' => strtolower($data->FuelType) == 'ze' ? 'electric' : strtolower($data->FuelType),
            ],
            'consumption' => [
                'co2_gkm' => intval($data->Co2Nedc)
            ]
        ];
    }

    public function mapNodeToStockVehicle($node, $language = null)
    {
        $stock = new StockVehicle();
        $stock->setId('GRM-' . $node->ChassisNumber);
        $stock->setMake($node->Manufacturer);
        $stock->setModel($node->Model);
        $stock->setDescription($node->Edition);
        $stock->setType($node->Edition);
//        $stock->setCarpassLink(isset($node->carPassHistory) ? $node->carPassHistory : null);
        $stock->setCondition('new');
//        $stock->setYear($node->first_reg);/**/
//        $stock->setMileage(floatval($node->mileage));
        $stock->setEmission(floatval($node->Co2Nedc));

//        if ($node->dealer_id && key_exists((int)$node->dealer_id, $this->ids)) {
//            $stock->setConcessions([$this->ids[(int)$node->dealer_id]]);
//        }

        /**
         * Prices
         */
        $full_price = floatval($node->CatalogPrice) + floatval($node->OptionsPrice);
        $discount = floatval($node->Discount);
        $recycling = floatval($node->RecyclingBonus);
        $sell = $full_price - $discount - $recycling;
        $stock->setPriceAdvantage($discount);
        $stock->setOldPrice($full_price);
        $stock->setPrice($sell);
        $stock->setPriceRecyclingBonus($recycling);

//        $stock->setImageUrls($this->getImages($node->images)); TODO

        $stock->setSpecificOptions($this->getOptions($node->Options));
        $stock->setTechnicalSpecGroups($this->getTechnicalSpecGroups($node));

        return $stock;
    }

}


<?php

namespace App\Controllers\Stock;

class Carflow extends AbstractIntegration
{
    static $URL_FORMAT = 'http://cfmpublic.cloudapp.net/Exports/BranchVehicles?branchId=%s&;useIds=false&;includeDeletedVehicles=false&since=2019-01-01';
    private $ids = [];

    private $languages = [];

    /**
     * CarflowXML constructor.
     * @param $concessions object[]
     */
    public function __construct(array $concessions, ?array $languages = [])
    {
        foreach ($concessions as $con) {
            $this->ids[$con['integration_id']] = $con['concession'];
        }
        $this->languages = $languages;
        $this->downloadFiles();
    }


    /**
     * Use this hook to preload the carflow mappings to transients
     */
    public function downloadFiles()
    {
        foreach ([
                     ['name' => 'brands', 'endpoint' => 'GetBrands', 'valueKey' => 'Name'],
                     ['name' => 'models', 'endpoint' => 'GetModels', 'valueKey' => 'Name'],
                     ['name' => 'bodytypes_nl', 'endpoint' => 'GetBodyworks', 'valueKey' => 'Translation'],
                     ['name' => 'bodytypes_fr', 'endpoint' => 'GetBodyworks?CultureID=2', 'valueKey' => 'Translation'],
                     ['name' => 'options_nl', 'endpoint' => 'GetOptions', 'valueKey' => 'Translation'],
                     ['name' => 'options_fr', 'endpoint' => 'GetOptions?CultureID=2', 'valueKey' => 'Translation'],
                 ] as $type) {
            $this->update_carflow_transients($type);
        }
    }

    private function getFilename($id, $language = null)
    {
        return sprintf('%s/api/carflow/%s.xml', get_template_directory(), $id);
    }

    public function getFiles()
    {
        $files = [];
        foreach ($this->ids as $id => $concession) {
            $files[] = sprintf(ABSPATH . 'api/carflow/%s.xml', $id);
        }
        return $files;
    }

    /**
     * @override
     */
    public function getVehicles(): array
    {
        $vehicles = [];

        foreach ($this->getFiles() as $file) {
            foreach (empty($this->languages) ? [null] : $this->languages as $lang) {
                $this->loopXmlFile($file, 'VehiclePublishData', function ($node, $language) use (&$vehicles, $lang) {
                    $vehicles[] = $this->mapNodeToStockVehicle($node, $lang);
                });
            }
        }

        return $vehicles;
    }

    public function mapNodeToStockVehicle($node, $language = null)
    {
        $culture = $language ? explode('-', $language)[0] : null;

        $stock = new StockVehicle();

        if ($culture) $stock->setLanguage($culture);
        else $culture = 'nl';

        $stock->setId('carflow-' . $node->Id);
        $stock->setMake($this->getCarflowValue('brands', $node->Brand));
        $stock->setModel($this->getCarflowValue('models', $node->Model));
        $stock->setDescription($node->Description);
        $stock->setType($this->getCarflowValue('bodytypes_' . $culture, $node->Bodywork));
        $stock->setCarpassLink(null);
        $stock->setCondition($node->CarCategory == 2 ? 'new' : 'second-hand');
        $stock->setYear($node->ModelYear);
        $stock->setMileage(floatval($node->Mileage));
        $stock->setEmission(floatval($node->Co2Emissions));

        $catalog = (float)str_replace(',', '.', $node->PriceB2C);
        $promo = (float)str_replace(',', '.', $node->PromoPrice);

        if ($promo) {
            $stock->setOldPrice($catalog);
            $stock->setPrice($promo);
            $stock->setPriceAdvantage($catalog - $promo);
        } else {
            $stock->setPrice($catalog);
        }

        if ($node->Branch && key_exists((int)$node->Branch, $this->ids)) {
            $stock->setConcessions([$this->ids[(int)$node->Branch]]);
        }

        $stock->setImageUrls($this->getImages($node->VehicleImages));

        $stock->setSpecificOptions($this->getOptions($node->Options, $culture));
        $stock->setTechnicalSpecGroups($this->getTechnicalSpecGroups($node));

        return $stock;
    }

    /**
     * MAPPINGS
     */
    public function getTechnicalSpecGroups($data): array
    {
        switch ($data->FuelType) {
            case 1:
                $fueltype = 'benzine';
                break;
            case 2:
                $fueltype = 'diesel';
                break;
            case 3:
                $fueltype = 'electric';
                break;
            case 13:
            case 10:
            case 4:
                $fueltype = 'hybrid';
                break;
        }

        return [
            'engine' => [
                'number_of_forward_gears' => (int)$data->NumberOfGears,
                'cylinder_capacity' => intval($data->CylinderContent),
                'drive_type' => (string)$data->Transmission,
                'transmission' => $data->TransmissionType == 1 ? 'manual' : 'automatic',
                'maximum_power_kw_hp' => sprintf('%s (%s)', $data->Kw, $data->DinHp),
                'engine' => (string)$data->Motorisation,
                'amount_of_cylinders' => (int)$data->NumberOfCylinders,
//            'energy_label' => '??',
//            'homologation_protocol' => '??',
                'fuel' => $fueltype,
            ],
            'consumption' => [
                'co2_gkm' => (int)$data->Co2Emissions
            ]
        ];
    }

    private function getImages($images): array
    {
        if (!$images) return [];

        $out = [];
        foreach (reset($images) as $url) {
            $url = preg_replace('/^https?:\/\/(.*)$/', 'https://$1', $url->ImageUrl);
            $out[] = ['url' => $url];
        }

        return $out;
    }

    private function getOptions($options, $language = 'nl'): array
    {
        $out = [];
        foreach (reset($options) as $opt)
            $out[] = ['option' => $this->getCarflowValue('options_' . $language, $opt)];

        return $out;
    }


    /**
     * Carflow Mappings
     */
    private function update_carflow_transients($type)
    {
        $name = 'carflow_' . $type['name'];
        if (get_site_transient($name)) return;

        StockController::log('Renewing transients: ' . $name);
        $xml = simplexml_load_file('http://public.carflowmanager.com/exports/' . $type['endpoint']);

        $out = [];
        foreach ($xml as $node) {
            $out['' . $node->Id] = (string)$node->{$type['valueKey']};
        }

        set_site_transient($name, $out, 604800); // one week
    }

    private function getCarflowValue($type, $key)
    {
        return get_site_transient('carflow_' . $type)['' . $key];
    }
}

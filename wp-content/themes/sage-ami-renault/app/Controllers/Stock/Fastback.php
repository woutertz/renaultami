<?php

namespace App\Controllers\Stock;

class Fastback extends AbstractIntegration
{

    private $fastback_url;
    private $concession_ids = [];
    private $filename;

    public function __construct($fastback_url, $concessions)
    {
        $this->fastback_url = $fastback_url;
        foreach ($concessions as $concession)
            $this->concession_ids[$concession['integration_id']] = $concession['concession'];
        $this->filename = sprintf("/tmp/fastback_%s.xml", get_field('dealer_name', 'options'));

        $this->downloadFiles();
    }

    private function downloadFiles()
    {
        $response = wp_remote_get($this->fastback_url)['body'];

        file_put_contents($this->filename, $response);

    }


    /**
     * @override
     */
    public function getVehicles(): array
    {
        $vehicles = [];
        $ids = $this->concession_ids;

        $this->loopXmlFile($this->filename, 'vehicle', function ($node) use (&$vehicles, $ids) {
            if (key_exists((int)$node->dealer_id, $ids)) {
                $vehicles[] = $this->mapNodeToStockVehicle($node);
            }
        });

        error_log(count($vehicles));

        return $vehicles;
    }


    /**
     * MAPPINGS
     */

    public function mapNodeToStockVehicle($node, $language = null)
    {
        $stock = new StockVehicle();
        $stock->setId('fastback-' . $node->internal_id);
        $stock->setMake($node->make);
        $stock->setModel($node->model);
        $stock->setDescription($node->body_type);
        $stock->setType($node->version);
        $stock->setCarpassLink(isset($node->carPassHistory) ? $node->carPassHistory : null);
        $stock->setCondition($this->getCondition($node->type));
        $stock->setYear($node->first_reg);
        $stock->setMileage(floatval($node->mileage));
        $stock->setEmission(floatval($node->co2));

        if ($node->dealer_id && key_exists((int)$node->dealer_id, $this->concession_ids)) {
            $stock->setConcessions([$this->concession_ids[(int)$node->dealer_id]]);
        }

        $sell = floatval($node->sell_price);
        $catalog = floatval($node->catalog_price);
        $marketprice = floatval($node->market_price);

        if ($catalog) {
            $stock->setOldPrice($catalog);

            if ($marketprice) {
                $stock->setPriceAdvantage(floatval($node->market_price) - $sell);
            }
        }
        $stock->setPrice($sell);

        $stock->setImageUrls($this->getImages($node->images));

        $stock->setSpecificOptions($this->getOptions($node->equipment_options));
        $stock->setTechnicalSpecGroups($this->getTechnicalSpecGroups($node));

        return $stock;
    }

    private function getCondition($cond): string
    {
        $cond = strtolower($cond);
        if ($cond == 'new car')
            return 'new';
        elseif ($cond == 'used car')
            return 'second-hand';

        return 'demo';
    }

    public function getTechnicalSpecGroups($data): array
    {
        preg_match('!\d+!', $data->gears, $number_of_forward_gears);

        return [
            'engine' => [
                'number_of_forward_gears' => count($number_of_forward_gears) > 0 ? $number_of_forward_gears[0] : null,
                'cylinder_capacity' => intval($data->engine_size),
                'drive_type' => (string)$data->transmission,
                'transmission' => starts_with(strtolower($data->gears), 'auto') ? 'automatic' : 'manual',
                'maximum_power_kw_hp' => sprintf('%s (%s)', $data->kw, $data->hp),
                'engine' => (string)$data->motorisation,
//            'amount_of_cylinders' => '????',
//            'energy_label' => '??',
//            'homologation_protocol' => '??',
                'fuel' => strtolower($data->fuel_type) == 'gas' ? 'benzine' : strtolower($data->fuel_type),
            ],
            'consumption' => [
                'co2_gkm' => intval($data->co2)
            ]
        ];
    }

    private function getImages($images): array
    {
        if (!$images) return [];

        $out = [];

        foreach ($images->image as $url) {
            $url = preg_replace('/^https?:\/\/(.*)$/', 'https://$1', $url);
            $out[] = ['url' => $url];
        }

        return $out;
    }

    private function getOptions($options): array
    {
        $out = [];
        foreach (reset($options) as $opt)
            $out[] = ['option' => (string)$opt];

        return $out;
    }
}

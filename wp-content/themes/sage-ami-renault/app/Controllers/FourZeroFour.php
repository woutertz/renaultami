<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class FourZeroFour extends Controller
{
    protected $template = '404';

    public function fourZeroFourQuery()
    {
        $args = array(
            'post_type' => 'page',//it is a Page right?
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'fields' => 'ids',
            'meta_query' => array(
                array(
                    'key' => '_wp_page_template',
                    'value' => 'views/template-404.blade.php', // template name as stored in the dB
                )
            )
        );
        $query = new WP_Query($args);
        return $query;
    }
}

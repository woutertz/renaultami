<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class SingleModel extends Controller
{
    public function gallery()
    {
        $gallery = get_field('gallery');

        $gallery = array_merge($gallery, $gallery, $gallery, $gallery, $gallery, $gallery);

        return App::array_to_object($gallery);
    }

    public static function get_fuel_badge($model_id)
    {
        $fuel_type = get_field('fuel_badge', $model_id);

        return sprintf('images/%s.svg', $fuel_type);
    }
}

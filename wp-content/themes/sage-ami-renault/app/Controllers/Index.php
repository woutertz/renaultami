<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Index extends Controller
{
    public function featuredPost()
    {
        $query = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 1,
            'fields' => 'ids',
            'post_status' => 'publish',
            'order_by' => 'menu_order'
        ));
        return ($query->posts[0]);
    }

    public function newsCategories()
    {
        return get_categories();
    }
}

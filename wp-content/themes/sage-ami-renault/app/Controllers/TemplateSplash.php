<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use function App\asset_path;

class TemplateSplash extends Controller
{
    public function brands()
    {
        $url_regex = '|^https?://|';
        return [
            (object)[
                'name' => 'Renault',
                'media' => (object)get_field('renault_media'),
                'logo' => asset_path('images/renault/logo.svg'),
                'color' => '#FFCC33',
                'url' => !empty(get_field('renault_url'))
                    ? get_field('renault_url')
                    : preg_replace(
                        $url_regex,
                        'https://renault.',
                        home_url('', 'https')
                    )
            ],
            (object)[
                'name' => 'Dacia',
                'media' => (object)get_field('dacia_media'),
                'logo' => asset_path('images/dacia/logo.svg'),
                'color' => '#34A1D7',
                'url' => !empty(get_field('dacia_url'))
                    ? get_field('dacia_url')
                    : preg_replace(
                        $url_regex,
                        'https://dacia.',
                        home_url('', 'https')
                    )
            ]
        ];
    }
}

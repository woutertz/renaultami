@extends('layouts.app')

@section('content')

  @include('components.featured-post')

  @include('components.news-filters')

  <div class="container mb-5">
    <div class="row">
      @while (have_posts()) @php the_post() @endphp
      <div class="col-md-6">

        @include('partials.news-thumbnail', [
          'post_id' => get_the_id()
          ])
      </div>
      @endwhile
    </div>
  </div>

  @include('partials.pagination')

@endsection

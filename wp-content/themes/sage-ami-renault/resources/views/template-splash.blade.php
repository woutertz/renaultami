{{--
  Template Name: Splash Page Template
--}}
  <!doctype html>
<html {!! get_language_attributes() !!}>

{{-- HTML Head --}}
@include('components.head')

{{-- Body start --}}
<body @php body_class() @endphp>

{{-- WP Header scripts --}}
@php do_action('get_header') @endphp

<div class="splash-container">
  {{-- Main header with navigation, search, ... --}}
  @include('components.header', ['is_splash' => true])

  <section class="splash" style="flex: 1">
    @foreach($brands as $brand)
      <a class="splash__item play-on-hover" href="{{ $brand->url }}" id="splash_{{ $brand->name }}">
        <section class="splash__top">
          <div class="splash__media">
            @if ($brand->media->type == 'video')
              <video
                muted loop autoplay>
                <source
                  src="{{ $brand->media->location == 'url' ? $brand->media->url : $brand->media->video_file['url'] }}"
                  type="video/mp4">
              </video>
            @elseif ($brand->media->type == 'image')
              @switch($brand->media->location)
                @case('url')
                <img src="{{ $brand->media->url }}" alt="">
                @break
                @case('file')
                <img
                  src="{{ $brand->media->image_file['url'] }}"
                  srcset="{{ wp_get_attachment_image_srcset($brand->media->image_file['id']) }}"
                  alt="">
              @endswitch()
            @endif
          </div>
          <div class="splash__bottom">
            <div class="splash__caption">
              <span>{{ pll__('Ontdek onze') }}</span>
              <h1>{{ pll__('Producten en services') }}</h1>
              <h3>{{ $brand->name }}</h3>
            </div>
          </div>
        </section>
        <div class="splash__brand">
          <img src="{{ $brand->logo }}" alt="">
          <div class="arrow" style="border-color: {{ $brand->color }}">
            <span class="icon icon--arrow"></span>
          </div>
        </div>
      </a>
    @endforeach
  </section>
</div>

{{-- Footer --}}
<footer class="footer">
  @include('components.footer')
</footer>

{{-- WP Footer scripts --}}
@php do_action('get_footer') @endphp
@php wp_footer() @endphp

</body>

</html>

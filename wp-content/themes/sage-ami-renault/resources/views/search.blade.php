@extends('layouts.app')

@section('content')


  <div class="container my-10">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if (!have_posts())
          <div class="alert alert-warning">
            {{ __('Sorry, no results were found.', 'sage') }}
          </div>
          {!! get_search_form(false) !!}
        @else
          <h1 class="mb-3">
            {!! pll__('Zoekresultaten voor') !!} "{!! get_search_query() !!}"
          </h1>
          <div class="search-result-container">
            @while(have_posts())
              @php(the_post())
              <div class="search-result">
                <div class="head">
                  <img
                    src="{{ empty(get_the_post_thumbnail_url()) ? wp_get_attachment_image_url(get_field('dealer_logo', 'options')) : get_the_post_thumbnail_url()  }}">
                </div>
                <div class="content">
                  <span class="type">{{ pll__(get_post_type()) }}</span>
                  <a href="{{ get_post_type() == 'concession' ? App\pll_permalink('/contact') : get_the_permalink() }}">
                    {{ get_the_title() }}
                  </a>
                </div>
              </div>
            @endwhile
          </div>
        @endif

        {!! get_the_posts_navigation() !!}
      </div>
    </div>
  </div>

@endsection

@include('partials.custom-strings')

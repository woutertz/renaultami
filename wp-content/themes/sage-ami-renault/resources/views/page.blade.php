@extends('layouts.app')

@section('content')

  <div class="container my-5 page-content">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        @while (have_posts()) @php the_post() @endphp
        @php the_content() @endphp
        @endwhile
      </div>
    </div>
  </div>


@endsection

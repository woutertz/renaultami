@extends('layouts.app')

@section('content')

  @while ($four_zero_four_query->have_posts()) @php $four_zero_four_query->the_post() @endphp

  <div class="fourohfour">
    @include('partials.image', [
       'image' => get_field('404_image'),
       'class' => 'fourohfour__image'
    ])
    <div class="fourohfour__inner">
      <h1 class="fourohfour__title">
        {!! get_field('404_title') !!}
      </h1>
      <h2 class="fourohfour__text">
        {!! get_field('404_text') !!}
      </h2>
    </div>
  </div>
  @endwhile
@endsection

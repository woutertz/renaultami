{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')

  @include('components.vertical-map')

  @if (!isset($hide_stock_box) || !$hide_stock_box)
    @include('components.stock-box')
  @endif

  @include('components.banner', [
    'banner' => $atelier_banner->atelier_banner
  ])

  @include('components.latest-news', [
    'title' => 'Laatste Nieuws',
    'posts' => $latest_news
  ])

  @include('components.concession-slider')

  @include('components.map', [
    'concessions' => $all_concessions
  ])

  <div class="container my-2 disclaimer">
    {!! $home_disclaimer !!}
  </div>

@endsection

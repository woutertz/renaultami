@extends('layouts.app')

@section('content')

  {{-- Cascade price box --}}
  @include('components.cascade')


  {{-- Price box --}}
  {{--
  @include('components.price-box')
   --}}

  {{-- Intro text --}}
  @if(!empty($intro))
    <div class="container">
      <div class="row mt-9 mb-8">
        <div class="col-lg-10 offset-lg-1">
          <div class="intro">
            {!! $intro !!}
          </div>
        </div>
      </div>
    </div>
  @endif

  {{-- Image and text rows --}}
  @include('components.image-text', [
    'image_and_text_rows' => $image_and_text_rows
  ])
  {{-- Available Versions --}}
  @include('components.version-slider')

  {{-- Simple image grid gallery --}}
  @include('components.gallery-slider')

  {{-- Available stock models --}}
  @include('components.stock-slider')

  {{-- Financing Text blocks --}}
  @include('components.financing')

  {{-- Financial Disclaimer --}}
  @include('components.financial-disclaimer')

  {{-- Form modals --}}
  @include('components.form-modal', [
          'car' => get_the_ID(),
          'testrit' => false
      ])
  @include('components.form-modal', [
       'car' => get_the_ID(),
       'testrit' => true
   ])

@endsection

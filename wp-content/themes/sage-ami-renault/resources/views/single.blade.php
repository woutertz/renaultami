@extends('layouts.app')

@section('content')

  @while (have_posts()) @php the_post() @endphp

  @include('components.post-hero')

  <div class="container my-10" id="post">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div class="post__intro">
          {!! $intro !!}
        </div>
        <div class="post__content">
          {!! $content !!}
        </div>
      </div>
      <div class="col-md-2">
        @include('partials.share')
      </div>
    </div>
  </div>

  @include('components.latest-news', [
    'classes' => 'pt-15 pb-10 bg-gray',
    'title' => 'Lees verder',
    'posts' => $latest_news
  ])

  @endwhile

@endsection

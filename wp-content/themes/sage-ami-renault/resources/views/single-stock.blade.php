{{-- Stock car detail page --}}

@extends('layouts.app')

@section('content')

  <div class="stock-step-wrap">
    <ul class="stock-steps">
      <li class="stock-step complete">
        {!! pll__('Zoek uw wagen') !!}
      </li>
      <li class="stock-step active">
        {!! pll__('Kies uw wagen') !!}
      </li>
      <li class="stock-step final">
        {!! pll__('Neem een aankoop optie') !!}
      </li>
    </ul>
  </div>

  <div class="wrap mt-4 mb-10">

    {{-- Header row --}}
    <div class="row mb-2 mb-md-4 align-items-center">

      {{-- Back link --}}
      <div class="col-1">
        <a href="{{ get_the_permalink($options->stock_page) }}"
           class="link link--arrow link--arrow-back stock-back">
          <span class="stock-back-text">{!! pll__('Terug') !!}</span>
        </a>
      </div>

      {{-- Name and description --}}
      <div class="col">
        <div class="stock-car__header">
          <div class="stock-car__name">
            {!! get_the_title() !!}
          </div>
          @if(!empty($type))
            <div class="stock-car__type">
              {!! $type !!}
            </div>
          @endif
          <div class="stock-car__description">
            {!! $description !!}
          </div>
        </div>
      </div>

      <div class="col d-flex justify-content-end align-items-center">
        <a href="#" onclick="window.print();return false;" class="print mr-2">
          <span class="icon icon--print"></span>
        </a>
        @include('partials.share')
      </div>
    </div>

    <div class="row mb-10">

      {{-- Gallery --}}
      <div class="col-md-6">
        <div class="stock-car__gallery">

          <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
              @if($image_urls)
                @foreach($image_urls as $img)
                  <div class="swiper-slide">
                    @include('partials.image', ['image' => $img->url])
                  </div>
                @endforeach
              @else
                @foreach($gallery as $item)
                  <div class="swiper-slide">
                    @if($item->type === 'video')
                      <a
                        href="{{ $item->type === 'image' ? wp_get_attachment_image_url($item->image, 'full') : $item->youtube_url }}"
                        class="gallery-slide__link fresco"
                        data-fresco-options="
                          youtube: { autoplay: 1 }
                        ">

                        @include('partials.image', [
                            'image' => $item->image
                        ])
                        <div class="icon icon--video"></div>
                      </a>

                    @else
                      @include('partials.image', [
                          'image' => $item->image
                      ])
                    @endif
                  </div>
                @endforeach
              @endif
            </div>
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
          </div>

          <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
              @if ($image_urls)
                @foreach($image_urls as $img)
                  <div class="swiper-slide">
                    @include('partials.image', ['image' => $img->url])
                  </div>
                @endforeach
              @else
                @foreach($gallery as $item)
                  <div class="swiper-slide">
                    @if($item->type === 'video')
                      <a
                        href="{{ $item->type === 'image' ? wp_get_attachment_image_url($item->image, 'full') : $item->youtube_url }}"
                        class="gallery-slide__link fresco"
                        data-fresco-options="
                          youtube: { autoplay: 1 }
                        ">

                        @include('partials.image', [
                            'image' => $item->image
                        ])
                        <div class="icon icon--video"></div>
                      </a>

                    @else
                      @include('partials.image', [
                          'image' => $item->image
                      ])
                    @endif
                  </div>
                @endforeach
              @endif
            </div>
          </div>
        </div>
      </div>

      {{-- Basic info --}}
      <div class="col-md-4 offset-md-1">
        <div class="stock-car__info">

          {{-- Price --}}
          <div class="stock-car__price-wrap">
            <div class="stock-car__price-label">
              {!! pll__('Momenteel voor') !!}
            </div>
            <div class="stock-car__price">
              {{ App::number_to_money(get_field('price')) }}
            </div>

            {{-- Discount --}}
            @if(get_field('price_advantage'))
              <div class="stock-car__advantage">
                {!! pll__('Voordeel tot') !!}
                <span class="highlight">
                  {{ App::number_to_money(get_field('price_advantage')) }}
                </span>
              </div>
            @endif
          </div>

          {{-- Basic specs --}}
          <div class="stock-car__specs">

            <div class="stock-car__spec row">
              <div class="stock-car__spec-label col-5">
                {!! pll__('Jaar') !!}
              </div>
              <div class="stock-car__spec-value col-6">
                {{ $year }}
              </div>
            </div>

            <div class="stock-car__spec row">
              <div class="stock-car__spec-label col-5">
                {!! pll__('Kilometerstand') !!}
              </div>
              <div class="stock-car__spec-value col-6">
                {{ $mileage }} km
              </div>
            </div>


            <div class="stock-car__spec row">
              <div class="stock-car__spec-label col-5">
                {!! pll__('Fuel') !!}
              </div>
              <div class="stock-car__spec-value col-6">
                {{ pll__($technical_specs['engine']['fuel']) }}
              </div>
            </div>


            <div class="stock-car__spec row">
              <div class="stock-car__spec-label col-5">
                {!! pll__('Transmissie') !!}
              </div>
              <div class="stock-car__spec-value col-6">
                {{ pll__($technical_specs['engine']['transmission']) }}
              </div>
            </div>


            <div class="stock-car__spec row">
              <div class="stock-car__spec-label col-5">
                {!! pll__('Consumption') !!}
              </div>
              <div class="stock-car__spec-value col-6">
                {{ $technical_specs['consumption']['co2_gkm'] ?? '-' }} g/km
              </div>
            </div>

            {{--
            <div class="stock-car__spec row">
              <div class="stock-car__spec-label col-5">
                {!! pll__('Energylabel') !!}
              </div>
              <div class="stock-car__spec-value col-6">
                <div
                  class="energy-label energy-label--{{ $technical_specs['engine']['energy_label'] }}">

                </div>
              </div>
            </div>
            --}}
          </div>

          {{-- Buttons --}}
          <div class="stock-car__buttons">
            <a href="#" class="button button--ghost button--arrow"
               data-toggle="modal"
               data-target="#car-{{ get_the_ID() }}">
              {!! pll__('Test Drive') !!}
            </a>
            @include('components.form-modal', [
                'car' => get_the_ID(),
                'testrit' => true
            ])
            <a href="#" class="button button--arrow" data-toggle="modal"
               data-target="#quote-{{ get_the_ID() }}">
              {!! pll__('Offerte') !!}
            </a>
            @include('components.form-modal', [
                  'car' => get_the_ID(),
                  'testrit' => false
              ])
          </div>

          {{-- Concession info --}}
          @if(!empty($concessions))
            @foreach($concessions as $concession)
              <div class="stock-car__concession-wrap">
                <div class="icon icon--pin"></div>
                <div class="stock-car__concession__content">
                  <div class="stock-car__concession-label">
                    {!! pll__('Deze wagen is beschikbaar in') !!}
                  </div>
                  <div class="stock-car__concession">
                    <div class="stock-car__concession-dealer">
                      {!! $dealership->name !!}
                    </div>
                    <div class="stock-car__concession-name">
                      {!! get_the_title($concession) !!}
                    </div>
                    <a href="tel:{{ get_field('phone', $concession) }}"
                       class="stock-car__concession-phone phone">
                      <div class="icon icon--phone"></div>
                      {{ get_field('phone', $concession) }}
                    </a>
                  </div>
                </div>
              </div>
            @endforeach
          @endif

          @if(!empty($carpass_link))
            <a
              href="{{ $carpass_link }} "
              class="stock-car__carpass" target="_blank">
              <img src="@asset('images/carpass.jpg')"
                   alt="Carpass">

            </a>
          @endif

        </div>
      </div>
    </div>

    <div class="row mb-8">
      @if(!empty($car_option_sets))
        <div class="col-md-5 offset-md-1">
          <div class="stock-car__options">
            <div class="stock-car__section-title">
              {!! pll__('Opties') !!}
            </div>

            @foreach($car_option_sets as $car_option_set)
              <h2
                class="option-set-title">{!! pll__($car_option_set->title) !!}</h2>

              <ul>
                @foreach($car_option_set->car_options as $item)
                  <li> {!! pll__($item->option) !!}</li>
                @endforeach
              </ul>
            @endforeach

          </div>
        </div>
      @endif

      <div class="col-md-4   offset-md-1">
        <div class="stock-car__technical-specs">
          <div class="stock-car__section-title">
            {!! pll__('Technische Specificaties') !!}
          </div>
          <table class="table">
            @foreach($specs as $spec)
              @if($spec->type == 'group')

                <tr class="row">
                  <td class="col bg-gray spec-title collapsed"
                      data-toggle="collapse" data-target=".{{ $spec->name }}">
                    <strong> {!! pll__($spec->label) !!}</strong>
                  </td>
                </tr>

                @foreach($spec->sub_fields as $field)

                  <tr class="row collapse {{ $spec->name }}">
                    <td class="col-6">
                      {!! pll__($field->label) !!}
                    </td>
                    <td class="col-6">
                      {!! pll__($technical_specs[$spec->name][$field->name]) ?: '-' !!}
                    </td>
                  </tr>
                @endforeach

              @endif
            @endforeach
          </table>

        </div>
      </div>
      {{--
      @if(!empty($technical_specs))
        <div class="col-md-5">
          <div class="stock-car__technical-specs">
            <div class="stock-car__section-title">
              {!! pll__('Technische Specificaties') !!}
            </div>
            <table class="table table-striped">
              @foreach($technical_specs as $item)
                <tr>
                  <td>
                    {!! $ite<m->label !!}
                  </td>
                  <td>
                    {!! $item->value !!}
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
        </div>
      @endif
      --}}
    </div>

    @if(!empty($specific_options))
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="stock-car__specific-options">
            <div class="stock-car__section-title">
              {!! pll__('Specifieke Opties') !!}
            </div>
            <ul>
              @foreach($specific_options as $item)
                <li>{!! $item->option !!}</li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    @endif
  </div>

  @if(!empty($concessions))
    @include('components.map', [
      'concessions' => $concessions,
      'title' => pll__('Deze wagen is beschikbaar in')
    ])
  @endif

  {{--
  @include('components.financing', ['hideLegalFixed' => true])
  --}}

@endsection

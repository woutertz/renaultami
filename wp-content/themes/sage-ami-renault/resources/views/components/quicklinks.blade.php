<div class="quicklinks">
  <div class="quicklinks__menu">
    <div class="quicklinks__menu-items">

      @include('partials.quicklink-item', [
          'link' => pll_current_language() == 'nl' ? site_url('/na-verkoop/') :  site_url('/apres-vente/'),
          'icon' => 'service',
          'label' => pll__('Na verkoop')
      ])

      @include('partials.quicklink-item', [
            'link' => get_the_permalink($options->models_page),
            'icon' => 'offers',
            'label' => pll__('Aanbod'),
            'class' => 'js-quote-form'
        ])

      @include('partials.quicklink-item', [
            'link' => '#',
            'class' => 'js-testride-form',
            'icon' => 'wheel',
            'label' => pll__('Testrit')
        ])

      @if ($options->live_chat_link)
        @include('partials.quicklink-item', [
              'link' => $options->live_chat_link,
              'class' => 'js-chat',
              'icon' => 'chat',
              'label' => pll__('Chat')
          ])
      @endif

      {{--
      @include('partials.quicklink-item', [
            'link' => 'https://nl.renault.be/model-configurator.html',
            'icon' => 'configur',
            'label' => pll__('Configurator')
        ])
      --}}

      @include('partials.quicklink-item', [
            'link' => '#',
            'class' => 'js-contact-form',
            'icon' => 'mail',
            'label' => pll__('Afspraak')
        ])

      @include('partials.quicklink-item', [
            'link' => get_the_permalink($options->contact_page),
            'icon' => 'pin',
            'label' => pll__('Concessies')
        ])

    </div>
  </div>
</div>

<div class="grid-gallery {{ $classes or '' }}">
  <div class="container">
    <div class="row">
      @foreach($items as $item)
        <a href="{{ wp_get_attachment_image_url($item, 'large') }}"
          class="grid-gallery__item fresco {{ $loop->index % 3 == 0 ? 'col-md-4': 'col-md-8'}}"
           data-fresco-group="fresco">

          @include('partials.image', [
              'image' => $item
          ])

        </a>
      @endforeach
    </div>
  </div>
</div>

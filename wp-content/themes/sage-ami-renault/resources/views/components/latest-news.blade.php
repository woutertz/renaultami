<div class="latest-news mt-10 mb-5 {{ $classes or '' }}">
  <div class="container">

    <div class="row">
      <div class="col section__header mb-7">
        <h3 class="section-title">
          {{ pll__($title) }}
        </h3>
        <a href="{{ get_post_type_archive_link( 'post' ) }}"
           class="button button--ghost button--big button--arrow">
          {{ pll__('Al het nieuws') }}
        </a>
      </div>
    </div>

    <div class="row">


      @foreach($posts as $post_id)
        <div class="col-md-6">
          @include('partials.news-thumbnail', [
            'post_id' => $post_id
            ])
        </div>
      @endforeach

    </div>
  </div>

</div>

@if(!empty($legal) || !empty($options->legal_fixed))
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="financing">
          <div class="financing-title">
            {!! pll__('EASYfin Classic Financiering') !!}
          </div>
          <div class="financing-box">
            {!! !empty($legal) ? App::wysiwyg_strip($legal) : App::wysiwyg_strip($options->legal_fallback) !!}
          </div>
          @if (! isset($hideLegalFixed) || $hideLegalFixed != true)
            <div class="financing-info">
              @if(!empty($legal_before))
                {!! App::wysiwyg_strip($legal_before) !!}
                <br><br>{{-- Brrrr koud --}}
              @endif
              {!!  App::wysiwyg_strip($options->legal_fixed) !!}

              @if(!$hide_optional_legal)
                <br><br>
                {!!  App::wysiwyg_strip($options->legal_fixed_optional) !!}
              @else
                <br><br>
                {!!  App::wysiwyg_strip($legal_fixed_optional_overwrite) !!}
              @endif
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@endif

@if($versions)

  <div class="version-slider-wrap thumbnail-slider">
    <div class="container">

      <h3 class="version-slider__title">
        {!! pll__('Kies de versie die u wil') !!}
      </h3>

      <div class="version-slider">

        <div class="swiper-wrapper">

          @foreach($versions as $version)
            @include('partials.car-slide', [
              'car' => $version,
              'badge' => $version->version_badge_badge,
            ])
          @endforeach
        </div>

        {{-- Slider navigation with arrows and bullets --}}
        <div class="swiper-navigation ">

          {{-- Pagination bullets --}}
          <div class="swiper-pagination"></div>

          {{-- Navigation arrows --}}
          <div class="slider__arrows slider__arrows--dark">
            <div class="slider__prev"></div>
            <div class="slider__next"></div>
          </div>

        </div>
      </div>
    </div>
  </div>
@endif

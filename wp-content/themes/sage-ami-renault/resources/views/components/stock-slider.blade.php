@if(!empty($stock))
  <div class="stock-slider-wrap thumbnail-slider">
    <div class="wrap">

      <div class="stock-slider__header">
        <h3 class="stock-slider__title">
          {!! pll__('Onze stock') !!}
        </h3>

        <a href="{{ $options->stock_page }}/?_model={{ get_the_ID() }}"
           class="stock-slider__button button button--inverse button--arrow-brand">
          {!! pll__('Bekijk het aanbod') !!}
        </a>
      </div>


      <div class="stock-slider">
        <div class="swiper-wrapper">

          @foreach($stock as $car)
            @include('partials.stock-slide', [
              'car' => App::array_to_object(get_fields($car)),
              'car_id' => $car
            ])
          @endforeach
        </div>

        {{-- Slider navigation with arrows and bullets --}}
        <div class="swiper-navigation">

          {{-- Pagination bullets --}}
          <div class="swiper-pagination"></div>

          {{-- Navigation arrows --}}
          <div class="slider__arrows">
            <div class="slider__prev"></div>
            <div class="slider__next"></div>
          </div>

        </div>
      </div>
    </div>

  </div>
@endif

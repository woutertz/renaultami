<!-- Modal -->
<div class="modal fade" id="{{ $form_id }}"
     tabindex="-1"
     aria-labelledby="testLabel" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">

        <div class="modal-close" data-dismiss="modal"></div>

        {{-- Form steps --}}
        <div class="row">
          <div class="form__steps mb-6">

            <li class="final-step">
              <a href="#" class="">
                {!! pll__('Controleren of alles correct verlopen is') !!}
              </a>
            </li>
          </div>
        </div>

        <div class="row">

          {{-- Left column with image and contact --}}
          <div class="col-md-4">
            <div class="form__title">
              {!! $title_light !!}
            </div>
            <div class="form__product-name">
              {!! $title_bold !!}
            </div>
            <div class="form__product-image">
              @include('partials.image', [
                  'image' => $image
              ])
            </div>
            @if(!$options->modals_hide_phonenumber)
              <div class="form__contact">
                {!! pll__('Problemen met het invullen van dit formulier? Bel ons') !!}
                <a class="phone"
                   href="tel:{{ get_field('phone', $dealership->primary_concession) }}">
                  {{ get_field('phone', $dealership->primary_concession) }}
                </a>
              </div>
            @endif
          </div>

          {{-- Right column with the form --}}
          <div class="col-md-7 offset-md-1">
            <div class="form">
              {!!  do_shortcode($shortcode) !!}
            </div>
          </div>
        </div>
        @if(!empty($options->form_disclaimer))
          <div class="row mt-4">
            <div class="col p-2 text-center disclaimer">
              {!! $options->form_disclaimer !!}
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>

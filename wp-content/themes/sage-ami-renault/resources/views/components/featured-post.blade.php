<div class="featured-post">
  <div class="featured-post__image">
    @include('partials.image', [
              'image' => get_post_thumbnail_id($featured_post)
          ])
  </div>

  <div class="featured-post__inner">
    <div class="featured-post__date">
      {{ get_the_date('', $featured_post) }}
    </div>
    <div class="featured-post__title">
      {!! get_the_title($featured_post) !!}
    </div>
    <a href="{{ get_the_permalink($featured_post) }}"
       class="featured-post__read-more">
      <div class="icon icon--plus"></div> {{ pll__('Lees het artikel') }}
    </a>
  </div>
</div>

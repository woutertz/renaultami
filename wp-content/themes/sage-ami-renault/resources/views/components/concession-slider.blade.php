<div class="concession-slider">

  <div class="swiper-wrapper">
    @foreach($all_concessions as $concession)
      <div class="swiper-slide">
        <div class="slide__image">
          @include('partials.image', [
             'image' => get_post_thumbnail_id($concession)
         ])
        </div>
        <div class="slide__info">
          <div class="slide__title">
            {!! pll__('Over') !!}
            <strong>{!! get_the_title($concession) !!}</strong>
          </div>
          <div class="slide__text">
            {!! App::wysiwyg_strip(get_field('description', $concession)) !!}
          </div>
          <div class="slide__phone">
            {{-- Phone button --}}
            <div class="phone">
              <div class="icon icon--phone"></div>
              <strong>Tel</strong>
              {!! $dealership->name !!}

              <div class="phone-dropdown">
                @foreach($all_concessions as $concession)
                  <a href="tel:{{ get_field('phone', $concession) }}"
                     class="phone-dropdown__concession">
                    <div class="concession__name">
                      {!! get_the_title($concession) !!}
                    </div>
                    <div class="concession__phone">
                      {{ get_field('phone', $concession) }}
                    </div>
                  </a>

                @endforeach
              </div>
            </div>
          </div>
          <a href="{{ get_the_permalink($options->contact_page) }}" class="button button--med button--arrow">
            {!! pll__('Contacteer ons') !!}
          </a>
        </div>
      </div>
    @endforeach
  </div>
  {{-- Pagination bullets --}}
  <div class="swiper-pagination">

  </div>
</div>

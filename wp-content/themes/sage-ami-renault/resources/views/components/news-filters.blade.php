<div id="filters"></div>
<div class="container mt-7 mb-5">
  <div class="row">
    <div class="col">
      <div class="news-filters">

        <div class="news-filters__title">
          {{ pll__('Filter de posts') }}
        </div>


        <div class="news-filters-items">

          <a href="{{ get_post_type_archive_link( 'post' ) }}#filters"
             class="news-filter">
            {{ pll__('Alles') }}
          </a>

          @foreach($news_categories as $category)
            <a href="{{ get_category_link($category->term_id) }}#filters"
               class="news-filter">
              {!! $category->cat_name !!}
            </a>
          @endforeach
        </div>
      </div>

    </div>
  </div>
</div>

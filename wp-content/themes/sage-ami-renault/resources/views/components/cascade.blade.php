<div class="container mt-9">
  <div class="row">
    <div class="col">
      <div class="cascade">

        {{-- Left column --}}
        <div class="cascade__price">

          <div class="row mb-2">
            <div class="col">
              <div class="cascade__name">
                {!! get_the_title() !!}
              </div>
              <div class="cascade__type">
                {!! get_field('cascade_version') !!}
              </div>
            </div>
          </div>

          @if(!empty($old_price))
            <div class="row">
              <div class="col-7">

                @if(!empty($price_popup_text))
                  <span class=""
                        data-toggle="tooltip"
                        data-placement="right"
                        title="{!! $price_popup_text !!}">
                      {!! !empty($price_label) ? $price_label : pll__('Prijs BTWi') !!} <small>(*)</small>
                  </span>
                @else
                  {!! !empty($price_label) ? $price_label : pll__('Prijs BTWi') !!}
                @endif
              </div>
              <div class="col-5 text-right">
                {{ App::number_to_money($old_price) }}
              </div>
            </div>
          @endif

          @php $discount_index = 1 @endphp
          @if(!empty($discounts))
            <div class="row mb-2">
              @foreach($discounts as $discount)
                <div class="col-7">
                  @if(!empty($discount->discount_text))
                    <span class=""
                          data-toggle="tooltip"
                          data-placement="right"
                          title="{!! $discount->discount_text !!}">
                    @endif
                      {!! $discount->discount_label !!}
                      @if(!empty($discount->discount_text))
                        <small>({{ $discount_index++ }})</small>
                      @endif

                      @if(!empty($discount->discount_text))
                  </span>
                  @endif
                </div>
                <div class="col-5 text-right">
                  <strong>- {{ App::number_to_money($discount->discount) }}</strong>
                </div>
              @endforeach
            </div>
          @endif

          @if(!empty($price_after_advantage))
            <div class="row align-items-center">
              <div class="col-7 t-24 t-bold">
                {!! !empty($price_after_advantage_label) ? $price_after_advantage_label :  pll__('Prijs na korting BTWi') !!}
              </div>
              <div class="col-5 text-right t-40 t-bold">
                {{ App::number_to_money($price_after_advantage) }}
              </div>
            </div>
          @endif

          @if(!empty($extra_discounts))
            <div class="row mb-2">
              @foreach($extra_discounts as $discount)
                <div class="col-7">
                  @if(!empty($discount->discount_text))
                    <span class=""
                          data-toggle="tooltip"
                          data-placement="right"
                          title="{!! $discount->discount_text !!}">
                      {!! $discount->discount_label !!} <small>({{ $discount_index++ }})</small>
                  </span>
                  @else
                    {!! $discount->discount_label !!}
                  @endif
                </div>
                <div class="col-5 text-right">
                  <strong>- {{ App::number_to_money($discount->discount) }}</strong>
                </div>
              @endforeach
            </div>
          @endif


          <div class="row py-3 my-3 cascade__price-btwi">
            <div class="col-7 t-24 t-bold">


              @if(!empty($new_price_popup_text))
                <span class=""
                      data-toggle="tooltip"
                      data-placement="right"
                      title="{!! $new_price_popup_text !!}">
                      {!! !empty($new_price_label) ? $new_price_label : pll__('Prijs BTWi') !!} <small>(*)</small>
                  </span>
              @else
                {!! !empty($new_price_label) ? $new_price_label : pll__('Prijs BTWi') !!}
              @endif
            </div>
            <div class="col-5 text-right t-24 t-bold">
              <strong>{{ App::number_to_money($current_price) }}</strong>
            </div>
          </div>


          @if(!empty($extra_messages))
            <div class="row mt-7">
              <div class="col cascade__lease-box">
                @foreach($extra_messages as $message)
                  <div
                    class="cascade__financial-lease-title @if(!$loop->first) d-none @endif">
                    {!! $message->message_content !!}
                  </div>
                @endforeach
                @foreach($extra_messages as $message)
                  {{-- Financial Lease --}}
                  <div
                    class="cascade__financial-lease @if(!$loop->first) d-none @endif">
                    <div class="cascade__lease-title font-weight-bold">
                      {{ $message->message_title }}
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          @endif

          {{-- Lease Box --}}
          @if(!empty($lease_types))
            <div class="row mt-5">
              <div class="col cascade__lease-box">

                <div class="row">
                  <div class="col t-24">
                    @foreach($lease_types as $lease_type)
                      <div
                        class="cascade__financial-lease-title @if(!$loop->first) d-none @endif">
                        {!! $lease_type->lease_name !!}
                      </div>
                    @endforeach
                  </div>
                  @if(count($lease_types) > 1)
                    <div class="col text-right">
                      <div class="cascade__switch">
                        <div class="cascade__switch-name">
                          {{ $lease_types[0]->lease_name }}
                        </div>

                        <input type="checkbox"
                               id="lease-switch"
                               class="js-lease-box-switch">
                        <label for="lease-switch">
                        </label>

                        <div class="cascade__switch-name">
                          {{ $lease_types[1]->lease_name }}
                        </div>

                      </div>
                    </div>
                  @endif
                </div>
                <div>
                  @foreach($lease_types as $lease_type)
                    {{-- Financial Lease --}}
                    <div
                      class="cascade__financial-lease @if(!$loop->first) d-none @endif">
                      <div class="cascade__lease-title">
                        {!! pll__('Liever maandelijks betalen?') !!}
                      </div>

                      <div class="row align-items-center">
                        <div class="col t-24 t-bold">
                          {!! pll__('Prijzen vanaf') !!}
                        </div>
                        <div class="col t-30 t-bold text-right no-break">
                          {{ App::number_to_money($lease_type->lease_monthly_price) }}
                          /{{ pll__('maand') }}
                        </div>
                      </div>

                      @if(!empty($lease_type->lease_last_monthly_payment_price))
                        <div class="row">
                          <div class="col">
                            {!! pll__('Laatste betaling van') !!}
                          </div>
                          <div class="col text-right t-bold">
                            {{ App::number_to_money($lease_type->lease_last_monthly_payment_price) }}
                          </div>
                        </div>
                      @endif
                    </div>
                  @endforeach
                </div>

              </div>
            </div>
          @endif

          <div class="row">
            <div class="col">
              {{-- Quote button --}}
              <a href="#" class="button button--arrow"
                 data-toggle="modal"
                 data-target="#quote-{{ get_the_ID() }}">
                {!! pll__('Ontvang een offerte') !!}
              </a>
            </div>
          </div>

        </div>

        {{-- Right column --}}
        <div class="cascade__info">
          <div class="cascade__thumbnail">
            @include('partials.image', [
            'image' => get_post_thumbnail_id()
            ])
          </div>
          <div class="cascade__specs">
            {!! $standard_equipment !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



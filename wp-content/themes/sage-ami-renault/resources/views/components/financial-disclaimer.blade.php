@if($financial_disclaimer or  $options->financial_disclaimer_fallback )
  <div class="financial-disclaimer is-hidden">
    <div class="financial-disclaimer__close"></div>
    <div class="financial-disclaimer__text">
      {!! $financial_disclaimer ?: $options->financial_disclaimer_fallback !!}
    </div>
  </div>
@endif

{{-- Price box on the Model Detail page --}}

<div class="container">

  <div class="price-box row mt-2 mb-5 mt-md-10 mb-md-10">

    <div class="price__buy col-md-7">

      {{-- Discount / advantage amount --}}
      @if(!empty($total_advantage))
        <div class="price__title">
          {{ pll__('Totaal voordeel tot') }}<span
            class="highlight"> {{ App::number_to_money($total_advantage) }}</span>
        </div>
      @endif

      {{-- Current and optional old price --}}
      <div class="prices">

        {{-- Current price --}}
        <div class="price__now">
          <span class="price__label">{{ pll__('Momenteel aan') }}</span>
          <div class="price">
            {{ App::number_to_money($current_price) }}
          </div>
        </div>

        {{-- Old price --}}
        @if(!empty($old_price))
          <div class="price__normal">
            <span class="price__label">{{ pll__('Catalogus prijs') }}</span>
            <div class="price line-through">
              {{ App::number_to_money($old_price) }}
            </div>
          </div>
        @endif
      </div>

      {{-- Recyclage label --}}
      <span class="price__label">
          {{ pll__('Prime de recyclage conditionnelle déduite') }}
        </span>

      {{-- Quote button --}}
      <a href="#" class="button button--ghost button--arrow" data-toggle="modal"
         data-target="#quote-{{ get_the_ID() }}">
        {!! pll__('Ontvang een offerte') !!}
      </a>
    </div>

    {{-- Lease price box --}}
    <div class="price__lease col-md-5">

      {{-- Title --}}
      <div class="price__title">
        {{ pll__('Prijzen vanaf') }}
      </div>

      {{-- Price --}}
      <div class="prices">
        <div class="price__now">
          <span class="price__label">{{ pll__('Momenteel aan') }}</span>
          <div class="price">
            {{ App::number_to_money($lease_monthly_price) }}
            /{{ pll__('maand') }}
            *
          </div>
        </div>
      </div>

      {{-- Recyclage label --}}
      <span class="price__label">
          {{ pll__('Prime de recyclage conditionnelle déduite') }}
        </span>

      {{-- Quote button --}}
      <a href="#" class="button button--ghost button--arrow" data-toggle="modal"
         data-target="#quote-{{ get_the_ID() }}">
        {!! pll__('Ontvang een offerte') !!}
      </a>
    </div>
  </div>
</div>

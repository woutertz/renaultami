{{-- Static image hero --}}
<div class="hero {{ $video ? 'hero--video' : '' }}">

  {{--
  @if(is_singular('model'))
    <a href="{{ get_the_permalink($options->models_page) }}" class="hero__back">
      {!! pll__('Terug') !!}
    </a>
  @endif
  --}}

  {{-- Background image --}}
  @if(!empty($hero->image))
    <div
      class="hero__image @if ($hero->image_mobile) d-none d-md-block @endif">
      @include('partials.image', [
          'image' => $hero->image
      ])
    </div>
  @endif

  @if(!empty($video))
    <div class="hero__video d-none d-sm-block">

      <video loop autoplay muted>
        <source src="{{ $video }}" type="video/mp4">
      </video>
    </div>
  @endif

  @if($hero->image_mobile)
    <div class="hero__image d-block d-md-none">
      @include('partials.image', [
        'image' => $hero->image_mobile
    ])
    </div>
  @endif

  <div class="hero__gradient"></div>

  {{-- Text --}}
  <div class="hero__copy">

    {{-- Optional badge --}}
    @include('partials.badge', [
        'badge' => $hero->badge,
        'class' => 'hero__badge'
    ])

    {{-- Title and subtitle --}}
    <h2 class="hero__top-title">
      {!! App::non_breaking_hyphens($hero->top_title) !!}
    </h2>
    <h1 class="hero__main-title">
      {!! App::non_breaking_hyphens($hero->main_title) !!}
    </h1>

  </div>

  {{-- Scroll down arrow --}}
  @if($hero->show_scroll_arrow)
    <a href="#main" class="hero__scroll"></a>
  @endif
</div>

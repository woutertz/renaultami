{{-- Topbar --}}

<div class="topbar">
  <nav class="topbar__nav">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu([
        'theme_location' => 'topbar_navigation',
        'menu_class' => 'topbar',
        'items_wrap' => '%3$s'
      ]) !!}
    @endif
  </nav>

  <div class="topbar__social">
    <div class="social-links">
      @if(!empty($dealership->social->facebook))
        <a href="{{ $dealership->social->facebook }}"
           class="social-link facebook">
          Facebook
        </a>
      @endif
      @if(!empty($dealership->social->instagram))
        <a href="{{ $dealership->social->instagram }}" target="_blank"
           class="social-link instagram">
          Instagram
        </a>
      @endif
      @if(!empty($dealership->social->linkedin))
        <a href="{{ $dealership->social->linkedin }}" target="_blank"
           class="social-link linkedin">
          LinkedIn
        </a>
      @endif
      @if(!empty($dealership->social->twitter))
        <a href="{{ $dealership->social->twitter }}" target="_blank"
           class="social-link twitter">
          Twitter
        </a>
      @endif
    </div>
  </div>

  @if(count(pll_the_languages(array('raw'=>1))) > 1)
    <div class="topbar__languages">
      @if(pll_current_language() == 'fr')
        <a href="{{ App::get_translation_url('nl') }}"
           class="language language--active">Nederlands</a>
      @else
        <a href="{{ App::get_translation_url('fr') }}"
           class="language language--active">Français</a>
      @endif
    </div>
  @endif
  @if($options->alternative_brand_link)
    <div class="topbar__discover">
      <a href="{{ $options->alternative_brand_link }}">

        @if(get_theme_mod('theme_brand_setting') == 'renault')
          {!! pll__('Ontdek') !!} <strong>Dacia</strong>
        @else
          {!! pll__('Ontdek') !!} <strong>Renault</strong>
        @endif
      </a>
    </div>
  @endif
</div>

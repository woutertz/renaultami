{{-- Page footer --}}

<footer class="footer @if($brand_name == 'Dacia') footer--dacia @endif">
  <div class="footer__main">
    <div class="container">

      {{-- Sitemap --}}
      <div class="row sitemap">

        @foreach($options->sitemap as $column)
          <div class="col-6 col-md-2 mb-2 mb-md-0">

            <h4 class="sitemap__title">
              {{ $column->title }}
            </h4>

            <nav class="sitemap__menu">

              @foreach($column->menu as $item)
                <a href="{{ $item->link->url }}"
                   class="sitemap__menu-item">
                  {{ $item->link->title }}
                </a>
              @endforeach

            </nav>

          </div>
        @endforeach

        {{-- Contact button --}}
        <div class="col-12 col-md-4 text-right">
          <a href="{{ get_the_permalink($options->contact_page) }}"
             class="button">
            <span class="button__before button__before--mail"></span>
            {!! pll__('Contacteer ons') !!}
          </a>

          {{--
          <div class="newsletter">
            {!!  do_shortcode( App::getForm('newsletter')) !!}
          </div>
          --}}
        </div>
      </div>

      <div class="row">
        <div class="col-md-8 order-1 order-md-0">
          <div class="footer__copyright">
            © @php date('Y') @endphp {{ $options->copyright_text }}
          </div>
        </div>
        <div class="col-md-4 mb-1 mb-md-0 order-0 order-md-1">
          <div class="footer__social">
            <div class="social-links">
              @if(!empty($dealership->social->facebook))
                <a href="{{ $dealership->social->twitter }}" target="_blank"
                   class="social-link facebook black-circle">
                  Facebook
                </a>
              @endif
              @if(!empty($dealership->social->instagram))
                <a href="{{ $dealership->social->instagram }}" target="_blank"
                   class="social-link instagram black-circle">
                  Instagram
                </a>
              @endif
              @if(!empty($dealership->social->linkedin))
                <a href="{{ $dealership->social->linkedin }}" target="_blank"
                   class="social-link linkedin black-circle">
                  LinkedIn
                </a>
              @endif
              @if(!empty($dealership->social->twitter))
                <a href="{{ $dealership->social->twitter }}" target="_blank"
                   class="social-link twitter black-circle">
                  Twitter
                </a>
              @endif
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</footer>

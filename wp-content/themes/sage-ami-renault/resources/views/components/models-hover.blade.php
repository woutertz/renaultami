{{--
  Models hover for navigation. Shows selection of models and link to overview
  page.
 --}}


<div class="models-hover">

  {{-- Model categories navigation --}}
  <div class="models-hover__nav wrap">
    @foreach($options->models_navigation->model_categories as $category)
      <a href="#"
         class="models-hover__nav-item {{ $loop->first ? 'active' : '' }}"
         data-target="{{ App::sluggify($category->name) }}">
        {!! App::wysiwyg_strip($category->name) !!}
      </a>
    @endforeach

    {{-- Close button --}}
    <a href="#" class="models-hover__nav-close">
      <span>{{ pll__('close') }}</span>
      <span class="icon icon--close-white"></span>
    </a>
  </div>

  {{-- Categories with selectin of models --}}
  <div class="models-hover__categories wrap">

    {{-- Single category --}}
    @foreach($options->models_navigation->model_categories as $category)
      <div class="models-hover__category {{ $loop->first ? 'active' : '' }}"
           data-category="{{ App::sluggify($category->name) }}">

        {{-- Title --}}
        <div class="models-hover__category-title">
          {!! App::wysiwyg_strip($category->name) !!}
        </div>

        {{-- Content --}}
        <div class="models-hover__category-content">

          {{-- Selection of models --}}
          <div class="models-hover__models">
            @if($category->models)
              @foreach($category->models as $model)
                <a href="{{ get_the_permalink($model) }}"
                   class="models-hover__model">
                  <div class="model__image">
                    @include('partials.image', [
                        'image' => get_post_thumbnail_id($model)
                    ])
                    @php $fuel_type = get_field('fuel_badge', $model) @endphp
                    @if( !empty($fuel_type) && $fuel_type != 'none' )
                      <div class="model__image--badge">
                        <img src="@asset(SingleModel::get_fuel_badge($model))">
                      </div>
                    @endif
                  </div>
                  <div class="model__title">
                    {!! App::non_breaking_hyphens(get_the_title($model)) !!}
                  </div>
                  <div class="model__price-wrap">
                    <div class="price-label">
                      {{ pll__('Prijs vanaf') }}
                    </div>
                    <div class="model__price">
                      {{ App::number_to_money(get_field('current_price', $model)) }}
                    </div>
                  </div>
                </a>
              @endforeach
              {{-- Link to overview page --}}
              @if(get_theme_mod('theme_brand_setting') == 'renault')
                <a
                  href="{{ get_the_permalink($options->models_page) }}#{{ App::sluggify($category->name) }}"
                  class="models-hover__link">
                  <span class="icon icon--plus"></span>
                  {{ pll__('Alle modellen') }}
                </a>
              @endif
            @endif
          </div>

          {{-- Link to overview page
          <a
            href="{{ get_the_permalink($options->models_page) }}#{{ App::sluggify($category->name) }}"
            class="models-hover__link button button--ghost">
            {{ pll__('Alle modellen') }}
          </a>
          --}}
        </div>

      </div>
    @endforeach

  </div>
</div>

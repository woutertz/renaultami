@while ($stock->have_posts()) @php $stock->the_post() @endphp
@endwhile

<div class="wrap">
  <div class="stock-box">
    <div class="container">

      <div class="row mb-3">

        {{--
        <div class="stock-box__brand">
          {!! $brand_name !!}
        </div>
        --}}
        <div class="stock-box__title">
          {!! pll__('Vind uw wagen') !!}
        </div>
      </div>
      <div class="row mb-5">
        <div class="stock-box__radios">

          <div class="stock-box__radios-facet d-none">
            @if(pll_current_language() == 'nl')
              {!! facetwp_display( 'facet', 'stock_box_nl_condition' ) !!}
            @elseif(pll_current_language() == 'fr')
              {!! facetwp_display( 'facet', 'stock_box_nl_condition' ) !!}
            @endif
          </div>

          <a href="#" class="stock-box__radio" data-value="new">
            <div class="icon icon--car-new"></div>
            {!! pll__('Nieuwe <br/> wagens') !!}
          </a>
          {{--
          <a href="#" class="stock-box__radio" data-value="demo">
            <div class="icon icon--car-demo"></div>
            {!! pll__('Demo <br/> wagens') !!}
          </a>
          --}}
          <a href="#" class="stock-box__radio" data-value="second-hand">
            <div class="icon icon--car-used"></div>
            {!! pll__('Occassie <br/> wagens') !!}
          </a>
        </div>

      </div>
      <div class="row">
        <div class="stock-box__selects">


          <div class="stock-box__select">
            @if(pll_current_language() == 'nl')
              {!! facetwp_display( 'facet', 'stock_box_nl_model' ) !!}
            @elseif(pll_current_language() == 'fr')
              {!! facetwp_display( 'facet', 'stock_box_fr_model' ) !!}
            @endif
          </div>
          <div class="stock-box__select">

            @if(pll_current_language() == 'nl')
              {!! facetwp_display( 'facet', 'stock_box_nl_fuel' ) !!}
            @elseif(pll_current_language() == 'fr')
              {!! facetwp_display( 'facet', 'stock_box_fr_fuel' ) !!}
            @endif
          </div>
          <div class="stock-box__submit">
            <a
              href="{{ get_the_permalink($options->stock_page) }}?{{ $stock_parameters }}"
              class="button button--arrow">
              @if(pll_current_language() == 'nl')
                {!! facetwp_display( 'facet', 'total_nl' ) !!}
              @elseif(pll_current_language() == 'fr')
                {!! facetwp_display( 'facet', 'total_fr' ) !!}
              @endif
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

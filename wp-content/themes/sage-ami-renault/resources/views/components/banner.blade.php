<div class="banner">
  <div class="banner__image">
    @include('partials.image', [
        'image' => $banner->image
    ])
  </div>
  <div class="banner__inner container">
    <div class="row">


      <div class="col-md-7">
        <div class="banner__title d-flex d-md-none">
          <div class="icon icon--atelier"></div>
          <span>
            {!! App::wysiwyg_strip($banner->title) !!}
          </span>
        </div>
        <div class="banner__bullets">
          <ol>
            @foreach($banner->bullets as $bullet)
              <li>
                {!! App::wysiwyg_strip($bullet->text) !!}
              </li>
            @endforeach
          </ol>
        </div>
      </div>
      <div class="banner__cta col-md-5">
        <div class="banner__title d-none d-md-flex">
          <div class="icon icon--atelier"></div>
          <span>
            {!! App::wysiwyg_strip($banner->title) !!}
          </span>

        </div>
        <a href="#" class="button button--calendar" data-toggle="modal"
           data-target="#atelier">
          {!! pll__('Maak een afspraak') !!}
        </a>
      </div>
    </div>

  </div>
</div>

{{-- Form modals --}}
@include('components.form-steps-modal', [
    'form_id' => 'atelier',
    'shortcode' =>  App::getForm('atelier'),
    'title_light' => pll__('Maak een afspraak'),
    'title_bold' => pll__('Atelier'),
    'image' => $options->atelier_form_image
])


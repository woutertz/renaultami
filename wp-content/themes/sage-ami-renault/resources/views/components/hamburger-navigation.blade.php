<nav class="hamburger-navigation">
  {{-- Hamburger toggle --}}
  <div class="hamburger hamburger--squeeze" tabindex="0"
       aria-label="Menu" role="button" aria-controls="navigation">
    <div class="hamburger-box">
      <div class="hamburger-inner"></div>
    </div>
  </div>

  <a class="header__logo-hamburger"
     href="{{ pll_home_url() }}">
    @include('partials.image', [
        'image' => $dealership->logo
    ])
  </a>


  @if (has_nav_menu('mobile_navigation'))
    <div class="hamburger-menu">
      <li class="menu-item menu-home">
        <a href="/" aria-current="page">
          Home
        </a>
      </li>


      <li class="menu-item menu-item-has-children menu-modellen">
        <a href="#" class="js-hamburger-toggle">
          {!! pll__('Modellen') !!}
        </a>

        <ul class="sub-menu">
          @foreach($options->models_mobile_navigation as $item)
            @php $term = get_term( $item->category ); @endphp
            <li class="menu-item menu-item-has-children menu-{{ $term->slug }}">
              <a href="#" class="js-hamburger-toggle">
                {!! $term->name !!}
              </a>
              @if(!empty($item->models))
                <ul class="sub-menu">
                  @foreach($item->models as $model)
                    <li
                      class="menu-item menu-item-model menu-{{ App::sluggify(get_the_title($model)) }}">
                      <a href="{{ get_the_permalink($model) }}">
                        @include('partials.image', [
                            'image' => get_post_thumbnail_id($model)
                        ]) {!! get_the_title($model) !!}
                      </a>
                    </li>
                  @endforeach
                  <li class="menu-item menu-models">
                    <a href="{{ get_the_permalink($options->models_page) }}"
                       aria-current="page" class="button--arrow">
                      {!! pll__('Alle modellen') !!}
                    </a>
                  </li>
                </ul>
              @endif
            </li>
          @endforeach
        </ul>
      </li>
      {!! wp_nav_menu([
        'theme_location' => 'mobile_navigation',
        'menu_class' => 'nav',
        'items_wrap' => '%3$s'
      ]) !!}
    </div>
  @endif


  @if(count(pll_the_languages(array('raw'=>1))) > 1)
    <div class="hamburger__languages">

      @if(pll_current_language() == 'fr')
        <a href="{{ App::get_translation_url('nl') }}"
           class="language language--active">Nederlands</a>
      @else
        <a href="{{ App::get_translation_url('fr') }}"
           class="language language--active">Français</a>
      @endif
    </div>
  @endif

  <div class="hambruger__discover text-center">
    <a href="{{ $options->alternative_brand_link }}">

      @if(get_theme_mod('theme_brand_setting') == 'renault')
        {!! pll__('Ontdek') !!} <strong>Dacia</strong>
      @else
        {!! pll__('Ontdek') !!} <strong>Renault</strong>
      @endif
    </a>
  </div>
</nav>

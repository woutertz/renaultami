{{-- Model category component with hover effects --}}

<div class="wrap">
  <div class="row ">
    <div class="col">

      {{-- Title and read more button --}}
      <div class="section__header mt-10 mb-5">
        <div class="section-title">
          {{ pll__('Onze modellen') }}
        </div>

        @if( $brand_name == 'Dacia' )
        <div class="section-subtitle">
          {{ pll__('Dacia-gamma') }}
        </div>
        @endif

        @if(get_theme_mod('theme_brand_setting') == 'renault')
          <a href="{{ get_the_permalink($options->models_page) }}"
             class="button button--ghost button--big button--arrow">
            {{ pll__('Alle modellen') }}
          </a>
        @endif
      </div>
    </div>
  </div>

  {{-- Desktop Version --}}
  <div class="row d-none d-md-block">
    <div class="col">

      {{-- Vertical model thumbnails with hover effect --}}
      <div class="vertical-models" id="vertical-models">
        <div class="categories js-desktop-categories">

          {{-- Categories --}}
          @foreach($categories as $category)
            <a href="{{ $loop->index }}" data-target="d-{{ $loop->index }}"
               class="category js-category">
              <div class="category__image">
                @include('partials.image', [
                      'image' => $category->image
                  ])
              </div>

              {{-- Title and link --}}
              <div class="category__inner">
                <div class="category__title">
                  {!! App::wysiwyg_strip($category->title) !!}
                </div>
                <div class="category__cta">
                  {{ pll__('Ontdek het aanbod') }}
                </div>
              </div>
            </a>

          @endforeach

        </div>

        {{-- Toggles with selection of models --}}
        @foreach($categories as $category)
          <div class="model model--hidden js-model" id="d-{{ $loop->index }}">

            <div class="row">

              {{-- Close button --}}
              <a href="#vertical-models" class="model__close">
                <img src="@asset('images/close.svg')" alt="">
              </a>

              <div class="col-lg-3">

                {{-- Models Title--}}
                <div class="model__title">
                  {!! App::wysiwyg_strip($category->title) !!}
                </div>
              </div>

              {{-- Models --}}
              <div class="col-lg-9">

                <div class="row models">
                  @foreach($category->models as $model)
                    @include('partials.model-thumbnail', [
                          'model' => App::array_to_object([
                              "id" => $model,
                              "fields" => get_fields($model),
                          ]),
                          "classes" => 'col-12 col-sm-6 col-md-4'
                      ])
                  @endforeach
                </div>

                {{-- Link to model overview page --}}
                <div class="row">
                  <div class="col">
                    @if(get_theme_mod('theme_brand_setting') == 'renault')
                      @isset($category->link->url)
                        <a href="{{ $category->link->url }}"
                           class="model__cta button button--big button--arrow button--ghost">
                          {!! pll__('Alle modellen') !!}
                        </a>
                      @endisset
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>

    </div>
  </div>

  {{-- Mobile Version --}}
  <div class="row d-block d-md-none">
    <div class="col">

      {{-- Vertical model thumbnails with hover effect --}}
      <div class="vertical-models" id="vertical-models">
        <div class="categories">

          {{-- Categories --}}
          @foreach($categories as $category)
            <a href="{{ $loop->index }}" data-target="m-{{ $loop->index }}"
               class="category js-category">
              <div class="category__image">
                @include('partials.image', [
                      'image' => $category->image
                  ])
              </div>

              {{-- Title and link --}}
              <div class="category__inner">
                <div class="category__title">
                  {!! App::wysiwyg_strip($category->title) !!}
                </div>
                <div class="category__cta">
                  {{ pll__('Ontdek het aanbod') }}
                </div>
              </div>
            </a>

            <div class="model model--hidden js-model" id="m-{{ $loop->index }}">

              <div class="row">

                {{-- Close button --}}
                <a href="#vertical-models" class="model__close">
                  <img src="@asset('images/close.svg')" alt="">
                </a>

                <div class="col-lg-3">

                  {{-- Models Title--}}
                  <div class="model__title">
                    {!! App::wysiwyg_strip($category->title) !!}
                  </div>
                </div>

                {{-- Models --}}
                <div class="col-lg-9">

                  <div class="row models">
                    @foreach($category->models as $model)
                      @include('partials.model-thumbnail', [
                            'model' => App::array_to_object([
                                "id" => $model,
                                "fields" => get_fields($model),
                            ]),
                            "classes" => 'col-12 col-sm-6 col-md-4'
                        ])
                    @endforeach
                  </div>

                  {{-- Link to model overview page --}}
                  @if(get_theme_mod('theme_brand_setting') == 'renault')
                    <div class="row">
                      <div class="col">
                        @isset($category->link->url)
                          <a href="{{ $category->link->url }}"
                             class="model__cta button button--big button--arrow button--ghost">
                            {!! pll__('Alle modellen') !!}
                          </a>
                        @endisset
                      </div>
                    </div>
                  @endif
                </div>
              </div>
            </div>

          @endforeach

        </div>
      </div>

    </div>
  </div>

</div>

{{--
  Image and text row
  --}}

@if(!empty($image_and_text_rows))
  <div class="wrap pb-6">

    @foreach($image_and_text_rows as $row)
      <div
        class="image-text {{ $row->image_position ? 'image-text--reverse' : '' }} mb-5 mb-md-11">
        <div class="image">
          <div class="image-ratio">
            @include('partials.image', [
                'image' => $row->image
            ])
          </div>
          @if($row->show_call_to_action)
            @if($row->call_to_action_group->call_to_action_type == 'page')
              <a href="{{ $row->call_to_action_group->call_to_action_link }}"
                 class="call-to-action">
                {!! App::wysiwyg_strip($row->call_to_action_group->call_to_action_text) !!}
              </a>
            @elseif($row->call_to_action_group->call_to_action_type == 'url')
              <a href="{{ $row->call_to_action_group->call_to_action_url }}"
                 class="call-to-action">
                {!! App::wysiwyg_strip($row->call_to_action_group->call_to_action_text) !!}
              </a>
            @elseif($row->call_to_action_group->call_to_action_type == 'form-quote')
              <a href="#"
                class="call-to-action" data-toggle="modal"
                data-target="#quote-{{ get_the_ID() }}">
                {!! App::wysiwyg_strip($row->call_to_action_group->call_to_action_text) !!}
              </a>
            @elseif($row->call_to_action_group->call_to_action_type == 'form-testride')
              <a
                href="#"
                class="call-to-action" data-toggle="modal"
                data-target="#car-{{ get_the_ID() }}">
                {!! App::wysiwyg_strip($row->call_to_action_group->call_to_action_text) !!}
              </a>
            @endif
          @endif
        </div>
        <div class="text {{ $row->show_call_to_action ? 'text--bottom' : '' }}">
          {!! $row->text !!}
        </div>
      </div>
    @endforeach
  </div>
@endif

{{-- Model categories --}}
<div class="model-categories">

  {{-- Model Category --}}
  @foreach($model_categories as $category)
    <div class="model-category"
         id="{{ App::sluggify($category->name) }}">

      {{-- Banner with image and title --}}
      <div class="wrap">
        <div class="model-category__header">
          @include('partials.image', [
              'image' => $category->image
          ])

          <div class="model-category__copy">
            <h2 class="model-category__title">
              {!! App::wysiwyg_strip($category->name) !!}
            </h2>
          </div>

        </div>
      </div>

      {{-- Models grid --}}
      <div class="model-category__models">
        <div class="container">
          <div class="row">

            @if($category->models)
              @foreach($category->models as $model)
                <div class="col-6 col-lg-4 col-xl-3">
                  @include('partials.model-thumbnail', [
                      'model' => App::array_to_object([
                          "id" => $model,
                          "fields" => get_fields($model)
                      ])
                  ])
                </div>
              @endforeach
            @endif

          </div>
        </div>
      </div>

    </div>
  @endforeach

</div>

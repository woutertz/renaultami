<!-- Modal -->
<div class="modal fade" id="{{ $testrit ? 'car-' . $car : 'quote-' .$car }}"
     tabindex="-1"
     aria-labelledby="testLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">

        <div class="modal-close" data-dismiss="modal"></div>

        {{-- Form steps --}}
        <div class="row">
          <div class="form__steps mb-6">
            <ol class="breadcrumb" data-form="caldera_form_1"
                id="caldera-forms-breadcrumb_1">
              <li class="complete">
                <a>
                  {!! pll__('Kies een voertuig') !!}
                </a>
              </li>
              <li class="active">
                <a>
                  {!! pll__('Gelieve uw persoonlijke informatie in te vullen') !!}
                </a>
              </li>
              <li class="final-step">
                <a href="#" class="">
                  {!! pll__('Controleren of alles correct verlopen is') !!}
                </a>
              </li>
            </ol>
          </div>
        </div>

        <div class="row">

          {{-- Left column with image and contact --}}
          <div class="col-md-4">
            <div class="form__title">
              @if($testrit)
                {!! pll__('Boek een testrit met') !!}
              @else
                {!! pll__('Ontvang een offerte voor') !!}
              @endif
            </div>
            <div class="form__product-name">
              {!! get_the_title($car) !!}
            </div>
            <div class="form__product-image">
              @php($image_id = get_post_thumbnail_id($car))
              @include('partials.image', [
                  'image' => $image_id > 0 ? $image_id : get_field('image_urls', $car)[0]['url']
              ])
            </div>
            @if(!$options->modals_hide_phonenumber)
              <div class="form__contact">
                {!! pll__('Problemen met het invullen van dit formulier? Bel ons') !!}
                <a class="phone"
                   href="tel:{{ get_field('phone', $dealership->primary_concession) }}">
                  {{ get_field('phone', $dealership->primary_concession) }}
                </a>
              </div>
            @endif
          </div>

          {{-- Right column with the form --}}
          <div class="col-md-7 offset-md-1">
            <div class="form">
              @if($testrit)
                {!!  do_shortcode( App::getForm('test_ride')) !!}
              @elseif(!$testrit)
                {!!  do_shortcode( App::getForm('quote')) !!}
              @endif
            </div>
          </div>
        </div>

        @if(!empty($options->form_disclaimer))
          <div class="row">
            <div class="col p-2 text-center disclaimer">
              {!! $options->form_disclaimer !!}
            </div>
          </div>
        @endif
      </div>

    </div>
  </div>
</div>

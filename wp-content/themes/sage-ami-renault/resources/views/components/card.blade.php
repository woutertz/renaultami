{{-- Cards: news thumbnails, concession box, info box, ... --}}

<div class="{{ $concession ? 'card card--boxed' : 'card' }}">
  <div class="card__thumbnail">

    @if($concession)
      @include('partials.image', [
          'image' => get_post_thumbnail_id($concession)
      ])
    @else
      <img src="//placehold.it/420x200" alt="">
    @endif

    @if($icon)
      <div class="card__icon">
        <div class="icon {{ $icon ?  'icon--' . $icon : 'icon--plus'}}"></div>
      </div>
    @endif
  </div>

  <div class="card__copy">


    @if($concession)
      <div class="card__sub-title">
        {!! $dealership->name !!}
      </div>
    @endif

    <div class="card__title">
      @if($concession)
        {!! get_the_title($concession) !!}
      @elseif($title)
        {!! get_the_title($concession) !!}
      @endif
    </div>

    <div class="card__content">
      @if($concession)
        <div class="card__concession-info">
          @include('partials.concession-info', [
            'concession' => App::array_to_object([
              "id" => $concession,
              "fields" => get_fields($concession)
            ])
          ])
        </div>
      @elseif($text)
        <div class="card__text">
          {!! $text !!}
        </div>
      @endif

      <div class="card__buttons">
        <a href="#" class="button button--arrow" data-toggle="modal"
           data-target="#contact" data-phone="{{ get_field('phone', $concession) }}">
          {!! pll__('Contact') !!}
        </a>
        {{-- Todo: Real link --}}
        <a
          href="{!! pll_current_language() == 'fr' ? '/a-propos' : '/over-ons' !!}"
          class="button button--arrow button--ghost button--ghost-light">
          {!! pll__('Bekijk het team') !!}
        </a>
      </div>
    </div>
  </div>


</div>

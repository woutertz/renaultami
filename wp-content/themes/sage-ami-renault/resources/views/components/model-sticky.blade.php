<div class="model-sticky sticky wrap">
  <div class="model-sticky__name">
    {{ $hero->hero->hero_image->top_title }}
    <strong>{{ $hero->hero->hero_image->main_title }} </strong>
  </div>

  <div class="model-sticky__links">
    @if($brochure)
      <a href="{{ $brochure }}" target="_blank" class="model-sticky__link">
        <div class="model-sticky__link-icon icon icon--brochure"></div>
        {{ pll__('Download de folder') }}
      </a>
    @endif
    <a href="#" class="model-sticky__link" data-toggle="modal"
       data-target="#car-{{ get_the_ID() }}">
      <div class="model-sticky__link-icon icon icon--wheel"></div>
      {{ pll__('Vraag een testrit aan') }}
    </a>

    @if($options->takeover_url)
      <a href="{{ $options->takeover_url }}" target="_blank"
         class="model-sticky__link">
        <div class="model-sticky__link-icon icon icon--pig"></div>
        {{ pll__('Schat uw wagen') }}
      </a>
    @endif
    <a
      href="{{ get_the_permalink($options->stock_page) }}?_model={{ get_the_ID() }}"
      class="model-sticky__link">
      <div class="model-sticky__link-icon icon icon--stock"></div>
      {{ pll__('Bekijk de stock') }}
    </a>
  </div>

  <a href="#" class="button button--arrow" data-toggle="modal"
     data-target="#quote-{{ get_the_ID() }}">
    {!! pll__('Ontvang een offerte') !!}
  </a>
</div>

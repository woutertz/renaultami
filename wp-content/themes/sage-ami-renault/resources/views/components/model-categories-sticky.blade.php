{{-- Sticky category nav --}}
<ul class="model-categories__nav sticky wrap">
  @foreach($model_categories as $category)
    <li>
      <a href="#{{ App::sluggify($category->name) }}" class="category">

        <div class="category__title">
          {!! App::wysiwyg_strip($category->name) !!}
        </div>
      </a>
    </li>
  @endforeach
</ul>

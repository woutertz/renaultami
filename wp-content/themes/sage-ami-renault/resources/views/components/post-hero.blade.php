<div class="post-hero mt-7">
  <div class="post-hero__inner">
    <a href="{{ get_post_type_archive_link( 'post' ) }}" class="post__back">
      {{ pll__('Terug naar overzicht') }}
    </a>
    <div class="post__info">
      <div class="post__date">
        {{ get_the_date() }}
      </div>
      <div class="post__title">
        {!! get_the_title() !!}
      </div>
    </div>

    <a href="#post" class="icon icon--plus"></a>
  </div>
  <div class="post-hero__image">
    @include('partials.image', [
        'image' => get_post_thumbnail_id()
    ])
  </div>
</div>

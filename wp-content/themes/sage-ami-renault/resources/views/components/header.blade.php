{{-- Header --}}

<header class="header">

  {{-- Dealer logo --}}
  <a class="header__logo"
     href="{{ pll_home_url() }}">
    @include('partials.image', [
        'image' => $dealership->logo
    ])
  </a>

  {{-- Content --}}
  <div class="header__content">

    {{-- Top part --}}
    <div class="header__info">

      @if(!isset($is_splash) || !$is_splash)
        {{-- Hamburger toggle --}}
        <div class="hamburger hamburger--squeeze" tabindex="0"
             aria-label="Menu" role="button" aria-controls="navigation">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
      @endif


      {{-- Dealer logo Mobile --}}
      <a class="header__logo-mobile"
         href="{{ pll_home_url() }}">
        @include('partials.image', [
            'image' => $dealership->logo
        ])
      </a>

      {{-- Phone button --}}
      @php $one_concession = $has_one_concession @endphp
      <div class="phone @if($one_concession) single @endif">
        <div class="icon icon--phone"></div>
        @if($one_concession)
          @php $concession = reset($all_concessions) @endphp
          <strong><a href="tel:{{ get_field('phone', $concession) }}">{{ pll__('Bel Ons') }}</a></strong>

        @else
          <strong>{{ pll__('Bel ons') }}</strong>
          <div class="phone-dropdown">
            @foreach($all_concessions as $concession)
              <a href="tel:{{ get_field('phone', $concession) }}"
                 class="phone-dropdown__concession">
                <div class="concession__name">
                  {!! get_the_title($concession) !!}
                </div>
                <div class="concession__phone">
                  {{ get_field('phone', $concession) }}
                </div>
              </a>
            @endforeach
          </div>
        @endif
      </div>

      {{-- Opening hours --}}
      <div class="opening-hours">
        <strong>{{ $opening_hours_today->open ? pll__('Open') : pll__('Gesloten') }}</strong>
        {{ pll__('Vandaag') }} {!! $opening_hours_today->opening_text !!}

        <div class="opening-hours-dropdown">
          @foreach($opening_hours_all as $opening_hours)
            <div class="opening-hours__set">
              <div class="opening-hours__table">
                <table class="table table-borderless table-sm">
                  <thead>
                  <tr>
                    <td colspan="2">
                      {!! $opening_hours->opening_hours_set_name !!}
                    </td>
                  </tr>
                  </thead>
                  @foreach($opening_hours->opening_hours_menu as $day)
                    <tr
                      class="{{ $loop->index == date('w') - 1 ? 'today' : '' }}">
                      <td>{!! App::day_of_week($loop->index) !!}</td>
                      <td>{!! $day->open ? $day->opening_hours_text : pll__('Gesloten') !!}</td>
                    </tr>
                  @endforeach
                </table>
              </div>
            </div>
          @endforeach
        </div>
      </div>

      {{-- Brand logo --}}
      <div class="header__brand">
        @if(isset($is_splash) && $is_splash)
          @foreach($brands as $brand)
            <img src="{{ $brand->logo }}" alt="">
          @endforeach
        @else
          <img src="{{ $brand_logo }}" alt="">
        @endif
      </div>
    </div>

    @if(!isset($is_splash) || !$is_splash)

      {{-- Bottom part --}}
      <div class="header__navigation">
        <nav class="header__menu">

          {{-- Home icon --}}
          <a href="{{ pll_home_url() }}" class="icon icon--home">
            Home
          </a>

          {{-- Menu --}}
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu([
              'theme_location' => 'primary_navigation',
              'menu_class' => 'nav',
              'items_wrap' => '%3$s'
            ]) !!}
          @endif
        </nav>

        {{-- Contact --}}
        <a href="#"
           class="header__contact" data-toggle="modal"
           data-target="#contact">
          <span class="pre pre--mail"></span>
          <span class="header__contact-text">
          {!! pll__('Contacteer ons') !!}
        </span>
        </a>

        {{-- Search --}}
        <a href="" class="header__search">
          <span class="pre pre--search"></span>
          <span class="header__search-text">
          {!! pll__('Zoeken') !!}
        </span>
        </a>

        <div class="header__search-form">
          <div class="icon icon--search"></div>
          {!! get_search_form(false) !!}
          <div class="header__search-form-close"></div>
        </div>

      </div>
  </div>

  {{-- Model hover navigation --}}
  @include('components.models-hover')

  @endif

</header>


{{-- Form modals --}}
@include('components.form-simple-modal', [
    'form_id' => 'contact',
    'shortcode' => App::getForm('contact'),
    'title_light' => pll__('Vragen of opmerkingen?'),
    'title_bold' => pll__('Contacteer ons'),
    'image' => $options->contact_form_image
])


@include('components.form-steps-modal', [
    'form_id' => 'custom-testride',
    'shortcode' => App::getForm('custom_test_ride'),
    'title_light' => pll__('Boek een testrit met'),
    'title_bold' => '',
    'image' => $options->test_ride_form_image
])


@include('components.form-steps-modal', [
    'form_id' => 'custom-quote',
    'shortcode' => App::getForm('custom_quote'),
    'title_light' => pll__('Ontvang een offerte voor'),
    'title_bold' => '',
    'image' => $options->quote_form_image
])

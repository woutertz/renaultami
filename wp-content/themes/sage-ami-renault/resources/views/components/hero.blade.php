{{-- Hero image or slider --}}
@isset($hero->hero)

  {{-- Clone field fix --}}
  @php $hero = $hero->hero @endphp

  @if($hero->hero_type == 'slider')
    {{-- Hero slider --}}
    @include('components.hero-slider', [
        'slider' => $hero->hero_slider
    ])

  @elseif($hero->hero_type == 'image')
    {{-- Hero Image --}}
    @include('components.hero-image', [
          'hero' => $hero->hero_image,
          'video' => $hero->hero_video
      ])
  @endif

  @isset($usage)
    <div class="hero-usage-overlay">
      @if(!empty($usage->l100km) || $usage->l100km === '0')
        <div>
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
               x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:white 0 0 60 60;" xml:space="preserve"
               fill="white">
          <g>
            <path d="M47.338,39.175c0-4.439-4.38-6.719-4.38-13.259v-2.04c0-5.64-1.08-7.92-4.319-8.939v-2.04
              c4.68,0.899,7.619,4.56,8.52,10.62h-2.221v2.399c0,5.939,4.44,8.22,4.44,13.259c0,3.66-1.98,5.82-5.34,5.82
              c-3.42,0-5.399-2.16-5.399-5.82v-9.239c0-1.56-0.601-2.159-2.16-2.159h-2.22v19.379h2.159v2.039H10.68v-2.039h2.22V17.157
              c0-2.88,1.38-4.26,4.26-4.26h12.84c2.88,0,4.26,1.38,4.26,4.26v8.579h2.22c2.88,0,4.26,1.381,4.26,4.26v9.18
              c0,2.521,1.08,3.78,3.3,3.78S47.338,41.696,47.338,39.175z M29.999,14.937H17.16c-1.56,0-2.16,0.66-2.16,2.22v29.999h17.159V17.157
              C32.159,15.597,31.559,14.937,29.999,14.937z M17.219,17.216h12.78v10.561h-12.78V17.216z M27.959,25.796v-6.6h-8.7v6.6H27.959z"></path>
          </g>
        </svg>
          {{ $usage->l100km }} l/100km {{ $usage->wltp ? 'WLTP' : 'NEDC' }}
        </div>
      @endif
      @if(!empty($usage->gkm) || $usage->gkm === '0')
        <div>
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
               x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:white 0 0 60 60;" xml:space="preserve"
               fill="white">
          <g>
            <path d="M19.379,39.296c-3.6,1.379-7.08,2.52-10.26,3.42l-0.54-1.98c3.24-1.02,6.48-2.1,9.72-3.3c-0.72-1.56-1.14-3.36-1.14-5.28
              c0-9.18,7.619-14.939,19.799-14.939h12.84l1.56,1.68c-2.04,17.34-8.76,26.039-20.099,26.039
              C26.099,44.935,21.839,42.776,19.379,39.296z M36.359,31.796c-4.74,2.459-9.899,4.739-14.939,6.719
              c2.1,2.701,5.64,4.381,9.84,4.381c10.02,0,16.079-7.98,17.939-23.64h-12.24c-11.039,0-17.699,4.86-17.699,12.899
              c0,1.68,0.36,3.18,1.021,4.561c5.1-1.98,10.199-4.2,15.239-6.721L36.359,31.796z"></path>
          </g>
        </svg>
          {{ $usage->gkm }} g/km {{ $usage->wltp ? 'WLTP' : 'NEDC' }}
        </div>
      @endif
    </div>
  @endisset

@endisset

@if(!empty($gallery))
  <div class="gallery-slider-wrap thumbnail-slider">
    <div class="wrap">

      <div class="gallery-slider__header">
        <h3 class="gallery-slider__title">
          {!! pll__('Meer inspiratie') !!}
        </h3>
      </div>
    </div>

    <div class="gallery-slider">

      <div class="swiper-wrapper">

        @foreach($gallery as $item)
          @if($loop->index == 0 or $loop->index % 6 == 0 or $loop->index % 6 == 1 or $loop->index % 6 == 3 or $loop->index % 6 == 5)
            <div class="gallery-slide swiper-slide">
              @endif
              <a
                href="{{ $item->type === 'image' ? wp_get_attachment_image_url($item->image, 'full') : $item->youtube_url }}"
                class="gallery-slide__link fresco"
                data-fresco-group="fresco"
                data-fresco-options="
                  youtube: { autoplay: 1 }
                ">
                @include('partials.image', [
                        'image' => $item->image
                    ])

                @if($item->type === 'video')
                  <div class="icon icon--video"></div>
                @endif
              </a>
              @if($loop->index == 0 or $loop->index % 6 == 0 or $loop->index % 6 == 2 or $loop->index % 6 == 4 or $loop->index % 6 == 5 or $loop->index % 6 == 5)
            </div>
          @endif

        @endforeach

      </div>

      {{-- Slider navigation with arrows and bullets --}}
      <div class="swiper-navigation">

        {{-- Pagination bullets --}}
        <div class="swiper-pagination"></div>

        {{-- Navigation arrows --}}
        <div class="slider__arrows slider__arrows--dark">
          <div class="slider__prev"></div>
          <div class="slider__next"></div>
        </div>

      </div>


    </div>
  </div>
@endif

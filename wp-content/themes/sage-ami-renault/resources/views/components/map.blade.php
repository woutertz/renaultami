<div class="map">
  <div class="map__iframe">
    <div class="map__acf" data-zoom="9">
      @foreach($concessions as $concession)
        @php
          $concession_id = $concession;
          $concession = App::array_to_object(get_fields($concession));
        @endphp
        <div class="marker"
             data-lat="{{ $concession->location->lat }}"
             data-lng="{{ $concession->location->lng }}"
             data-icon="{{ $marker_icon }}"
             data-concession-id="{{ $concession_id }}"
        >

        </div>

      @endforeach
    </div>
  </div>
  <div class="map__content">
    <h4 class="map__title">
      {!!  $title or pll__('Onze concessies') !!}
    </h4>
    @foreach($concessions as $concession)
      @include('partials.concession-info', [
            'concession' => App::array_to_object([
              "id" => $concession,
              "fields" => get_fields($concession),
              "title" => get_the_title($concession)
            ])
          ])
    @endforeach

  </div>
</div>

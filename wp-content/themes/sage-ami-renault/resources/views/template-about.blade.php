{{--
  Template Name: About Page Template
--}}

@extends('layouts.app')

@section('content')

  <div class="container">
    <div class="row mt-9 mb-8">
      <div class="col-lg-10 offset-lg-1">
        <div class="intro">
          {!! $intro !!}
        </div>
      </div>
    </div>
  </div>


  <div class="wrap pb-6">

    @foreach($image_and_text_rows as $row)
      <div
        class="image-text {{ $row->image_position ? 'image-text--reverse' : '' }} mb-5 mb-md-11">
        <div class="image">
          <div class="image-ratio">
            @include('partials.image', [
                'image' => $row->image
            ])
          </div>
        </div>
        <div class="text">
          {!! $row->text !!}
        </div>
      </div>
    @endforeach
  </div>

  @include('components.grid-gallery', [
    'items' => $gallery,
    'classes' => 'mb-5 mb-md-17'
  ])


  @if(!empty($concessions))
    <div class="container">

      @foreach($concessions as $concession)
        @php
          $concession_fields = get_fields($concession) ;
        @endphp

        @if($concession_fields['team_members'])
          <div class="row mb-10">
            <div class="col-12">
              <div class="section-title mb-5">
                {{ pll__('Team') }} {!! get_the_title($concession) !!}
              </div>
            </div>
            @foreach($concession_fields['team_members'] as $person)
              <div class="col-lg-4 col-md-6 mb-2">
                @include('partials.team-card',
                    ['person' => (object) [
                        'name' => get_the_title($person),
                        'phone' => get_field('phone', $person),
                        'email' => get_field('email', $person),
                        'description' => get_field('description', $person),
                        'image' => get_field('thumbnail', $person),
                        'concession' => $concession
                      ]
                    ])
              </div>
            @endforeach
          </div>

        @endif
      @endforeach

    </div>

  @endif

@endsection

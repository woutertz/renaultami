<!doctype html>
<html {!! get_language_attributes() !!}>

{{-- HTML Head --}}
@include('components.head')

{{-- Body start --}}
<body @php body_class() @endphp>

@if(!empty($dealership->body_script))
  {!! $dealership->body_script !!}
@endif

{{-- WP Header scripts --}}
@php do_action('get_header') @endphp

{{-- Black topbar with contact, social, language switch and brand link --}}
@include('components.topbar')

{{-- Main header with navigation, search, ... --}}
@include('components.header')

{{-- Mobile navigation --}}
@include('components.hamburger-navigation')

{{-- Model links sticky --}}
@if(is_singular('model'))
  @include('components.model-sticky')
@endif



{{-- Page hero, static image or slider --}}
@include('components.hero')



{{-- Main page content --}}
<main class="main" id="main" role="document">

  {{-- Model Archive sticky --}}


  {{-- Content --}}
  @yield('content')


  @include('components.quicklinks')

</main>

{{-- Footer --}}
<footer class="footer">
  @include('components.footer')
</footer>

{{-- WP Footer scripts --}}
@php do_action('get_footer') @endphp
@php wp_footer() @endphp

</body>

</html>

{{--
  Template Name: Model Archive Template
--}}

@extends('layouts.app')

@section('content')

  {{-- Sticky categories nav --}}
  @include('components.model-categories-sticky')

  {{-- Model categories banners and grid --}}
  @include('components.model-categories')

@endsection

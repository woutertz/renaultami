<div class="team-card">
  <div class="team-card__image">
  @if($person->image)
      @include('partials.image', [
          'image' => $person->image
      ])
    @else
      <img src="@asset('images/team-placeholder.jpg')" alt="">
    @endif
  </div>
  <div class="team-card__name">
    {!! $person->name !!}
  </div>
  <div class="team-card__job">
    {!! $person->description !!}
  </div>
  <a href="tel:{{ $person->phone }}" class="team-card__phone">
    <div class="icon-wrap">
      <div class="icon icon--phone"></div>
    </div>
    {{ strlen($person->phone > 0) ? $person->phone : get_field('phone', $person->concession) }}
  </a>
  <a
    href="mailto:{{ !empty($person->email) ? $person->email : get_field('email', $person->concession) }}"
    class="team-card__email">
    <div class="icon-wrap">
      <div class="icon icon--mail"></div>
    </div>
    {{ pll__('Stuur een email') }}
  </a>
</div>

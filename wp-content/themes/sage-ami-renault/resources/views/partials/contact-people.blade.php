@php($has_multiple = sizeof($people) > 1)
<div class="row mb-9 w-100">
  @foreach($people as $person)
    <a href="mailto:{{ $person->email }}" class="h-100-l {{ $has_multiple ? 'col-lg-6' : 'col-lg-12' }}">
      <div class="contact-card px-3 h-100 @if($has_multiple)skewed {{ $loop->first ? 'left' : 'right' }}@endif">
        <div class="contact-card__image">
          @include('partials.image', [
              'image' => $person->image
            ])
        </div>

        <div class="contact-card__info">
          <div class="contact-card__label">
            {{ pll__('Uw contactpersoon') }}
          </div>
          <span class="contact-card__name">
              {{ $person->name }}
            </span>
          @if($person->concession)
            <span class="contact-card__concession">
              <div class="icon icon--pin"></div> {{  $person->concession->post_title }}
            </span>
          @endif
          <span class="contact-card__phone">
                {{ $person->phone }}
              </span>
          <div class="contact-card__email link link--arrow">
            <div
              class="icon icon--mail"></div> {{ pll__('Stuur een mail') }}
          </div>
        </div>
      </div>
    </a>
  @endforeach
</div>

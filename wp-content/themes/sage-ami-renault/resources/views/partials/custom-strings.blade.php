<div class="d-none">

  {{ pll__('Vragen of opmerkingen?') }}
  {!! pll__('Engine') !!}
  {!! pll__('Maximum power kW (hp)') !!}
  {!! pll__('Transmission') !!}
  {!! pll__('Drive type') !!}
  {!! pll__('Number of forward gears') !!}
  {!! pll__('Cylinder capacity (cm³)') !!}
  {!! pll__('Amount of cylinders') !!}
  {!! pll__('Energy Label') !!}
  {!! pll__('Homologation protocol') !!}
  {!! pll__('Fuel') !!}
  {!! pll__('CO2 (g/km)') !!}
  {!! pll__('Urban cycle (l/100 km)') !!}
  {!! pll__('Road cycle (l/100 km)') !!}
  {!! pll__('Full cycle (l/100 km)') !!}
  {!! pll__('Performance') !!}
  {!! pll__('Top speed (km/h)') !!}
  {!! pll__('Acceleration 0 - 100 km / h (s)') !!}
  {!! pll__('Tires') !!}
  {!! pll__('Standard front / rear tires') !!}
  {!! pll__('Rear brakes') !!}
  {!! pll__('Front brakes') !!}
  {!! pll__('Weight') !!}
  {!! pll__('Maximum permissible weight of the tow') !!}
  {!! pll__('Maximum permissible weight (HTG)') !!}
  {!! pll__('Curb weight') !!}
  {!! pll__('Measurements') !!}
  {!! pll__('Total width with the exterior mirrors folded out') !!}
  {!! pll__('Total length') !!}
  {!! pll__('Total width') !!}
  {!! pll__('Total height') !!}
  {!! pll__('Max. loading length') !!}
  {!! pll__('Max. loading length from the floor to the rear seat') !!}
  {!! pll__('Max. loading length with the rear seats folded down') !!}
  {!! pll__('Minimum luggage space (dm3)') !!}
  {!! pll__('Maximum luggage space (dm³)') !!}
  {!! pll__('Fuel tank (l)') !!}
  {!! pll__('Misc') !!}
  {!! pll__('Number of seats') !!}
  {!! pll__('Number of doors') !!}
  {!! pll__('Tax assets') !!}
  {!! pll__('Turning circle between curbs / walls (m)') !!}
  {!! pll__('Staat') !!}
  {!! pll__('Model') !!}
  {!! pll__('Prijs') !!}
  {!! pll__('Jaar') !!}
  {!! pll__('Transmissie') !!}
  {!! pll__('Brandstof') !!}
  {!! pll__('Laatste Nieuws') !!}


  {!!  pll__('Na verkoop') !!}
  {!!  pll__('Aanbod') !!}
  {!! pll__('Testrit') !!}
  {!! pll__('Configurator') !!}
  {!! pll__('Afspraak') !!}
  {!! pll__('Concessies') !!}
  {!! pll__('benzine') !!}
  {!! pll__('diesel') !!}
  {!! pll__('automatic') !!}
  {!! pll__('manueel') !!}
  {!! pll__('manual') !!}

  {!! pll__('Kilometerstand (Minste)') !!}
  {!! pll__('Kilometerstand (Meeste)') !!}
  {!! pll__('Jaar (Nieuwste)') !!}
  {!! pll__('Jaar (Oudste)') !!}
  {!! pll__('Prijs (Hoogste)') !!}
  {!! pll__('Prijs (Laagste)') !!}




  {!! pll__('New') !!}
  {!! pll__('Second Hand') !!}
  {!! pll__('Demo') !!}

  {!! pll__('Lees verder') !!}


  {!! pll__('Ja, dat wil ik niet missen') !!}
  {!! pll__('Nee, dat weiger ik') !!}
  {!! pll__('Gelieve de algemene voorwaarden te accepteren') !!}
  {!! pll__('Ja, ik ga akkoord met de algemene voorwaarden') !!}
  {!! pll__('Verzenden') !!}
  {!! pll__('Vul uw persoonlijke informatie in') !!}
  {!! pll__('Merk') !!}
  {!! pll__('Kies een datum, locatie, en maak een afspraak') !!}

{{--  Post Types--}}
  {!! pll__('concession') !!}
  {!! pll__('place') !!}
  {!! pll__('model') !!}
  {!! pll__('stock') !!}
  {!! pll__('team') !!}

</div>

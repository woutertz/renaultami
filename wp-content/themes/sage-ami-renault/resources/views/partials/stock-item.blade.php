<div class="stock-item"
     data-slug="{{ get_post_field( 'post_name', get_post())  }}"
     data-saved="false"
     data-id="{{ get_the_ID() }}">
  <div class="row">
    <div class="col-md-3">
      <a href="{{ get_the_permalink() }}" class="stock-item__image">
        @if(!empty(get_field('image_urls')))
          @include('partials.image', [
            'image' => get_field('image_urls')[0]['url']
          ])
        @else
          @include('partials.image', [
            'image' => get_post_thumbnail_id()
          ])
        @endif
        @if(!empty(get_field('badge')))
          <div class="stock-item__badge">
            {!! App::wysiwyg_strip(get_field('badge')) !!}
          </div>
        @endif
        @if(!empty(get_field('ribbon')))
          <div class="stock-item__ribbon">
            <div class="stock-item__ribbon-inner">

              {!! App::wysiwyg_strip(get_field('ribbon')) !!}

            </div>
          </div>
        @endif
      </a>

      @if(!empty(get_field('price_advantage')))

        <div class="stock-item__advantage">

          @if(pll_current_language() == 'nl')
            {{ pll__('Bespaar tot') }}
            <div class="highlight">
              {{ App::number_to_money(get_field('price_advantage')) }}
            </div>
          @endif

          @if(pll_current_language() == 'fr')
            <div class="highlight mr-2">
              {{ App::number_to_money(get_field('price_advantage')) }}
            </div>{{ pll__('Bespaar tot') }}
          @endif

          <div class="t-14 t-light t-italic mt-1">
            @if (get_field('price_recycling_bonus'))
              {{ sprintf(pll__('Recyclagepremie van %s inbegrepen'), App::number_to_money(get_field('price_recycling_bonus'))) }}
            @else
              {{ pll__('Premie inbegrepen') }}
            @endif
          </div>
        </div>
      @endif


    </div>
    <div class="col-md-7 pl-md-3">
      <div class="stock-item__header">

        <div class="stock-item__name">
          <a href="{{ get_the_permalink() }}" class="stock-item__title">
            {!! get_the_title() !!}
          </a>
          <div class="stock-item__subtitle">
            {!! get_field('type') !!}
          </div>
        </div>
        <div class="stock-item__price-wrap">
          <div class="stock-item__price-label">
            {!! pll__('Momenteel voor') !!}
          </div>
          @if(!empty(get_field('price')))
            <div class="stock-item__price highlight highlight--half">
              {{ App::number_to_money(get_field('price')) }}
            </div>
          @endif
        </div>
      </div>

      {{--
      <div class="stock-item__features">
        {!! get_field('description') !!}
      </div>
      --}}

      @php
        $techspecs = get_field('technical_spec_groups');
      @endphp

      <div class="row">
        <div class="col-md-7">
          <div class="stock-item__specs">
            <div class="stock-item__spec row">
              <div class="col">{!! pll__('Jaar') !!}</div>
              <div class="col"><strong>{!! get_field('year') !!}</strong></div>
            </div>
            <div class="stock-item__spec row">
              <div class="col">{!! pll__('Mileage') !!}</div>
              <div class="col"><strong>{!! get_field('mileage') !!}
                  km</strong></div>
            </div>
            <div class="stock-item__spec row">
              <div class="col">{!! pll__('Fuel') !!}</div>
              <div class="col">
                <strong>{!! pll__($techspecs['engine']['fuel']) ?? 'Undefined' !!}</strong>
              </div>
            </div>
            <div class="stock-item__spec row">
              <div class="col">{!! pll__('Transmission') !!}</div>
              <div class="col">
                <strong>{!! pll__($techspecs['engine']['transmission']) ?? 'Undefined' !!}</strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5">

          @php $concessions = get_field('concessions') @endphp
          @if($concessions[0])
            <div class="stock-item__contact d-none d-md-block">
              {!! pll__('Beschikbaar in') !!}

              @foreach($concessions as $concession)
                <div class="stock-item__concession">
                  <div class="icon-wrap">
                    <div class="icon icon--pin"></div>
                  </div>
                  {!! get_the_title($concession) !!}
                </div>
                <div class="stock-item__phone">
                  <div class="icon-wrap">
                    <div class="icon icon--phone"></div>
                  </div>
                  {{ get_field('phone', $concession) }}
                </div>
                <br>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
    <div class="col-md-2 d-flex d-md-block">
      <a href="#" class="icon-link icon-link__compare js-stock-compare">
        <span class="icon-wrap-square">
          <span class="icon icon--compare"></span>
        </span>
        {!! pll__('Vergelijken') !!}
      </a>
      <a href="#" class="icon-link icon-link__save js-stock-save">
        <span class="icon-wrap-square">
          <span class="icon icon--wishlist"></span>
        </span>
        {!! pll__('Opslaan') !!}
      </a>
      <a href="#" class="icon-link" data-toggle="modal"
         data-target="#car-{{ get_the_ID() }}">
        <span class="icon-wrap-square">
          <span class="icon icon--wheel"></span>
        </span>
        {!! pll__('Testrit') !!}
        @include('components.form-modal', [
            'car' => get_the_ID(),
            'testrit' => true
        ])
      </a>
      <a href="#" class="icon-link" data-toggle="modal"
         data-target="#quote-{{ get_the_ID() }}">
        <span class="icon-wrap-square">
          <span class="icon icon--calculator"></span>
        </span>
        {!! pll__('Gratis offerte') !!}
        @include('components.form-modal', [
            'car' => get_the_ID(),
            'testrit' => false
        ])
      </a>
      <a href="{{ get_the_permalink() }}" class="button button--arrow">
        {!! pll__('Bekijk') !!}
      </a>
    </div>
  </div>
</div>

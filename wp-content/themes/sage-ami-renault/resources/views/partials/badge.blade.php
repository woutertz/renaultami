{{-- Badge --}}

@if($badge->show_badge)
  <div class="badge {{ $class or '' }}">
      <span class="badge__light">
        {{ $badge->badge_light_text }}
      </span>
    <span class="badge__bold">
        {{ $badge->badge_bold_text }}
    </span>
  </div>
@endif

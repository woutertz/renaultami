<div class="stock-filter">
  <a href="#{{ $facet }}" class="stock-filter__title collapsed"
     data-toggle="collapse">
    {!! pll__($title) !!}
  </a>
  <div class="stock-filter__options collapse show" id="{{ $facet }}">
    {!! facetwp_display( 'facet', $facet ) !!}
  </div>
</div>

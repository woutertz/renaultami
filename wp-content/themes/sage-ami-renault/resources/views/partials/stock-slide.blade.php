{{-- Model slide for versions and stock --}}
<div class="car-slide swiper-slide">

  {{-- Thumbnail image --}}
  <a href="{{ get_the_permalink($car_id) }}" class="model-thumbnail__image">
    @php($image_id = get_post_thumbnail_id($car_id))
    @include('partials.image', [
        'image' => $image_id > 0 ? $image_id : $car->image_urls[0]->url
    ])
  </a>


  <div class="model-thumbnail__inner">

    <div class="icon icon--plus"></div>
    <a href="{{ get_the_permalink($car_id) }}" class="model-thumbnail__name">
      {{ get_the_title($car_id) }}
    </a>

    <div class="row">
      @if(!empty($car->old_price))
        <div class="col">
          <div class="model-thumbnail__price-label">
            {{ pll__('Stock prijs') }}
          </div>

          <div class="model-thumbnail__old-price">
            {{ App::number_to_money($car->old_price) }}
          </div>
        </div>
      @endif
      @if(!empty($car->price))
        <div class="col">
          <div class="model-thumbnail__price-label">
            {{ pll__('Huidige prijs') }}
          </div>
          <div class="model-thumbnail__current-price">
            {{ App::number_to_money($car->price) }}
          </div>
        </div>
      @endif
    </div>

    @if(!empty($car->price_advantage))
      <div class="model-thumbnail__advantage">
        {{ pll__('Bespaar tot') }}
        <div class="highlight">
          {{ App::number_to_money($car->price_advantage) }}
        </div>
      </div>
    @endif

    @if(!empty($car->description))
      <div class="model-thumbnail__description">
        {!! $car->description !!}
      </div>
    @endif

    <a
      href="{{ get_the_permalink($car_id) }}"
      class="link link--arrow">
      {!! pll__('Ontdek dit model') !!}
    </a>

  </div>

</div>

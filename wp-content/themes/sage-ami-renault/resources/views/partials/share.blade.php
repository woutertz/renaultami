<div class="share js-share">
  <span class="icon icon--share"></span>
  <div class="share__links">
    <a target="_blank"
       class="share-link share-facebook"
       href="https://www.facebook.com/sharer/sharer.php?u={{ get_the_permalink() }}">
    </a>
    <a target="_blank"
       class="share-link share-twitter"
       href="https://twitter.com/intent/tweet?text={{ get_the_title() }}<br/>{{ get_the_permalink() }} ">
    </a>
    <a target="_blank"
       class="share-link share-linkedin"
       href="https://www.linkedin.com/shareArticle?mini=true&url={{ get_the_permalink() }}/&title={{ get_the_title() }}&summary={{ get_the_permalink() }}&source=">
    </a>
  </div>
</div>

<a href="{{ get_the_permalink($post_id) }}" class="news-thumbnail">
  <div class="news-thumbnail__image">
    @include('partials.image', [
        'image' => get_post_thumbnail_id($post_id)
    ])
  </div>
  <div class="news-thumbnail__inner">
    <div class="icon icon--plus"></div>
    <div class="news-thumbnail__date">
      {{ get_the_date('', $post_id) }}
    </div>
    <div class="news-thumbnail__title">
      {!! get_the_title($post_id) !!}
    </div>
  </div>
</a>

{{-- Model thumbnail with optional badge, discount --}}

<a href="{{ get_the_permalink($model->id) }}"
   class="model-thumbnail {{ $classes or '' }}">

  {{-- Optional badge --}}
  @isset($model->fields->hero->hero->hero_image)
    @if($model->fields->hero->hero->hero_image->badge->show_badge)
      <div class="model-thumbnail__badge">
        @include('partials.badge', [
            'badge' => $model->fields->hero->hero->hero_image->badge
        ])
      </div>
    @endif
  @endisset

  <div class="model-thumbnail__image">
    @include('partials.image', [
        'image' => get_post_thumbnail_id($model->id)
    ])
  </div>

  <h3 class="model-thumbnail__name">
    {{ get_the_title($model->id) }}
  </h3>

  @if(!empty($model->fields->total_advantage))
    <div class="model-thumbnail__advantage model-thumbnail__advantage--top">
      {{ pll__('Bespaar tot') }}
      <div class="highlight">
        {{ App::number_to_money($model->fields->total_advantage) }}
      </div>
    </div>
  @endif

  <div class="row">
    @if(!empty($model->fields->old_price))
      <div class="col-6">
        <div class="model-thumbnail__price-label">
          {{ pll__('Prijs vanaf') }}
        </div>

        <div class="model-thumbnail__old-price">
          {{ App::number_to_money($model->fields->old_price) }}
        </div>
      </div>
    @endif
    @if(!empty($model->fields->current_price))
      <div class="col-6">
        <div class="model-thumbnail__price-label">
          {{ pll__('Actie prijs') }}
        </div>
        <div class="model-thumbnail__current-price">
          {{ App::number_to_money($model->fields->current_price) }}
        </div>
      </div>
    @endif
  </div>

  @if(!empty($model->fields->total_advantage))
    <div class="model-thumbnail__advantage model-thumbnail__advantage--bottom">
      {{ pll__('Bespaar tot') }}
      <div class="highlight">
        {{ App::number_to_money($model->fields->total_advantage) }}
      </div>
    </div>
  @endif

  <div class="cta">
    {{ pll__('Meer informatie') }}
  </div>

</a>

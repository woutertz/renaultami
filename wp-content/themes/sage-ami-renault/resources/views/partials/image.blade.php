{{-- Responsive Image --}}

<img src="{{ gettype($image) === 'string' ? $image : wp_get_attachment_image_url($image, 'full') }}"
     srcset="{{ wp_get_attachment_image_srcset($image) }}"
     alt="{{ $alt or '' }}"
     class="{{ $class or '' }}">

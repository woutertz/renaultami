<div class="concession-info" data-id="{{ $concession->id }}">
  <div class="concession-info__contact">
    @isset($concession->title)
      <div class="concession-info__title">
        {!! $concession->title !!}
      </div>
    @endisset
    <div class="concession-info__address">
      {{ $concession->fields->location->street_name }} {{ $concession->fields->location->street_number }}
      <br>
      {{ $concession->fields->location->post_code }} {{ $concession->fields->location->city }}
      <br>
    </div>
    <div class="concession-info__phone">
      <div class="icon icon--phone"></div>
      {{ $concession->fields->phone }}
    </div>
    <div class="concession-info__social">
      <div class="social-links">
        @if(!empty($concession->fields->social->facebook))
          <a href="{{ $concession->fields->social->facebook }}" class="social-link facebook black-circle">
            Facebook
          </a>
        @endif
        @if(!empty($concession->fields->social->instagram))
          <a href="{{ $concession->fields->social->instagram }}" class="social-link instagram black-circle">
            Instagram
          </a>
        @endif
        @if(!empty($concession->fields->social->linkedin))
          <a href="{{ $concession->fields->social->linkedin }}" class="social-link linkedin black-circle">
            LinkedIn
          </a>
        @endif
        @if(!empty($concession->fields->social->twitter))
          <a href="{{ $concession->fields->social->twitter }}" class="social-link twitter black-circle">
            Twitter
          </a>
        @endif
      </div>
    </div>
  </div>

  <div class="concession-info__icons">
    <a href="tel:{{ $concession->fields->phone }}"
       class="concession-info__icon">
      <div class="icon icon--contact"></div>
      {!! pll__('Contact') !!}
    </a>
    <a
      href="https://maps.google.com/?q={{ $concession->fields->location->address }}"
      class="concession-info__icon"
      target="_blank">
      <div class="icon icon--directions"></div>
      {!! pll__('Route') !!}
    </a>
  </div>

  <div class="concession-info__opening-hours">
    <a class="opening-hours__link collapsed" data-toggle="collapse"
       href="#opening-hours-{{ $concession->id }}"
       role="button" aria-expanded="false" aria-controls="opening-hours">
      {{ pll__('Openingsuren') }}
    </a>
    <div class="collapse" id="opening-hours-{{ $concession->id }}">

      @foreach(App::openingHoursAllByID($concession->id) as $opening_hours)
        <div class="opening-hours__set">
          <div class="opening-hours__table">
            <table class="table table-borderless table-sm">
              <thead>
              <tr>
                <td colspan="2">
                  {!! $opening_hours->opening_hours_set_name !!}
                </td>
              </tr>
              </thead>
              @foreach($opening_hours->opening_hours_menu as $day)
                <tr
                  class="{{ $loop->index == date('w') - 1 ? 'today' : '' }}">
                  <td>{!! App::day_of_week($loop->index) !!}</td>
                  <td>{!! $day->open ? $day->opening_hours_text : pll__('Gesloten') !!}</td>
                </tr>
              @endforeach
            </table>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>

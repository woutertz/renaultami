<a href="{{ $link }}" class="quicklinks__menu-item {{ $class or '' }}">
  <div class="quicklinks__menu-item-icon icon icon--{{ $icon }}-white">
  </div>
  <div class="quicklinks__menu-item-title">
    {!! $label !!}
  </div>
</a>

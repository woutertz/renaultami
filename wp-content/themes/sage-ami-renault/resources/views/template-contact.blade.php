{{--
  Template Name: Contact Page Template
--}}

@extends('layouts.app')

@section('content')

  <div class="wrap pt-11 pb-15 bg-light">
    <div class="row mb-6">
      <div class="col">
        <h2 class="section-title">
          {!! pll__('Onze concessies') !!}
        </h2>
      </div>
    </div>
    <div class="row">

      @foreach($concessions as $concession)
        <div class="col-md-6 col-xl-4 mb-2">

          @include('components.card', [
              'icon' => 'location',
              'concession' => $concession
           ])
        </div>
      @endforeach
    </div>

    @if(!empty($distributeurs))
      <div class="row mb-4 mt-8">
        <div class="col">
          <h2 class="section-title">
            {!! pll__('Distributeurs') !!}
          </h2>
        </div>
      </div>
      <div class="row">
        @foreach($distributeurs as $item)
          <div class="col-6 col-md-3 col-xl-2 mb-2">
            <div class="distributor bg-white p-3">
              {!! $item->distributeur !!}
            </div>
          </div>
        @endforeach
      </div>
    @endif
  </div>

  @include('components.map', [
    'concessions' => $concessions
  ])



@endsection

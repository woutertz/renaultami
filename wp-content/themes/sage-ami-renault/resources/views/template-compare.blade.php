{{--
  Template Name: Compare Template
--}}

@extends('layouts.app')

@section('content')



  <div class="wrap my-5 compare">
    <div class="row mb-3">
      <div class="col">
        <h2 class="section-title">{!! pll__('Vergelijken') !!}</h2>
      </div>
    </div>
    <div class="row">
      <div class="col">

        <table class="table">
          <tr class="row">
            <th class="col-3"></th>
            @foreach($compared_cars as $car)
              <th class="col-3">
                <div class="compare-car">
                  <div class="compare-car__image">
                    @include('partials.image', [
                    'image' => get_post_thumbnail_id($car['id'])
                  ])
                  </div>
                  <h3 class="compare-car__title">
                    {!! get_the_title($car['id']) !!}
                  </h3>
                  <div class="compare-car__description">
                    {!! get_field('description', $car['id']) !!}
                  </div>
                  <div
                    class="stock-item__price highlight highlight--half mb-2">
                    {{ App::number_to_money(get_field('price', $car['id'])) }}
                  </div>
                  <br/>
                  <a href="{{ get_the_permalink($car['id']) }}"
                     class="button button--small">
                    {!! pll__('Meer informatie') !!}
                  </a>
                </div>
              </th>
            @endforeach
          </tr>
          @foreach($specs as $spec)
            @if($spec->type == 'group')

              <tr class="row">
                <td class="col-{{ (count($compared_cars) + 1) * 3 }} bg-gray">
                  <strong> {!! pll__($spec->label) !!}</strong>
                </td>
              </tr>

              @foreach($spec->sub_fields as $field)

                <tr class="row">
                  <td class="col-3">
                    <strong>{!! pll__($field->label) !!}</strong>
                  </td>
                  @foreach($compared_cars as $car)

                    <td class="col-3">
                      @if(!empty($car['fields']['technical_spec_groups']))
                        {!! pll__($car['fields']['technical_spec_groups'][$spec->name][$field->name]) ?: '-' !!}
                      @else
                        -
                      @endif
                    </td>
                  @endforeach
                </tr>
              @endforeach

            @endif

          @endforeach

        </table>
      </div>

    </div>
  </div>


@endsection

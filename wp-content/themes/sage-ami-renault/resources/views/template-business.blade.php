{{--
  Template Name: Business Template
--}}

@extends('layouts.app')

@section('content')

  <div class="container my-3 my-md-7">
    <div class="row mb-7">
      <div class="col-12">
        @include('partials.contact-people', ['people' => $contact_people])
      </div>

      <div class="col-lg-10 offset-lg-1">
        {{-- Intro --}}
        <div class="business__intro">
          {!! $intro !!}
        </div>

      </div>


    </div>


    {{-- Services --}}
    <div class="row">
      @foreach($services as $service)
        <div class="col-md-6">
          <div class="service">
            <div class="service__thumbnail">
              @include('partials.image', [
                'image' => $service->image
              ])
            </div>

            <div class="service__inner">
              <div class="service__title">
                {!! $service->title !!}
              </div>
              <div class="service__text">
                {!! $service->text !!}
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>


@endsection

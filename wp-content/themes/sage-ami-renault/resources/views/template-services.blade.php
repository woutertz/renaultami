{{--
  Template Name: Services Template
--}}

@extends('layouts.app')

@section('content')

  <div class="wrap my-10">
    {{-- Atelier banner --}}
    @include('components.banner', [
      'banner' => $atelier_banner->atelier_banner
    ])
  </div>


  {{-- Services --}}
  <div class="container mb-7" id="our-service">
    <div class="row">
      <div class="col">
        <div class="section__header mb-7">
          <div class="section-title">
            {{ pll__('Onze Diensten') }}
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      @foreach($services as $service)
        <div class="col-md-6">
          <div class="service">
            <div class="service__thumbnail">
              @include('partials.image', [
                'image' => $service->image
              ])


            </div>

            <div class="service__inner">
              <div
                class="icon {{ $service->icon ?  'icon--' . $service->icon : '' }}">

              </div>
              <div class="service__title">
                {!! $service->title !!}
              </div>
              <div class="service__text">
                {!! $service->text !!}
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>


@endsection

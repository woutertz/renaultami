{{--
  Template Name: Stock Archive Template
--}}

@extends('layouts.app')

@section('content')
  {{-- if using carflow,                            and last sync was more than 3 hours ago (1000x60x60x3) --}}
  @if ($options->carflow_plugin && (microtime(true)*1000) - $options->stock_last_sync > 10800000)
    <div id="carflow_xml" class="d-none"></div>
    <script type="text/javascript"
            src="https://cfmapistorp01.blob.core.windows.net/resources/Scripts/Utils.min.js"></script>
    <script type="text/javascript"
            src="https://cfmapistorp01.blob.core.windows.net/resources/Scripts/plugin.ui.min.js"></script>
    <script type="text/javascript"
            src="https://cfmapistorp01.blob.core.windows.net/resources/Scripts/Stock.min.js"></script>
    <script type="text/javascript">
      CFM.API.init({
        Environment: CFM.Environment.PROD,
        Culture: "nl-BE",
        SpinnerColor: CFM.SpinnerColor.BLACK
      })
      CFM.API.enable('{{ $options->carflow_function_code }}', '#carflow_xml')
      console.log('[CarFlow] Syncing..')

      let vehicles = null, tries = 0
      let checkExists = setInterval(() => {
        vehicles = document.querySelectorAll('#carflow_xml vehicles')
        console.log('[CarFlow] Checking for data');
        if (++tries >= 50) clearInterval(checkExists)

        if (vehicles.length) {
          clearInterval(checkExists)
          let xml = new XMLSerializer().serializeToString(vehicles[0])

          jQuery.ajax({
            type: 'post',
            url: '{{ $options->ajax_url}}',
            data: {
              action: 'sync_carflow',
              vehicles: xml
            },
            success: () => console.log('[CarFlow] Done'),
            error: (e) => jQuery('#carflow_xml').html(JSON.stringify(e))
          })
        }
      }, 500)
    </script>
  @endif


  <div class="stock-archive-wrap d-none">
    {{-- Header with current filters, result count and sort button --}}
    <div class="stock-header wrap">
      <div onclick="FWP.reset()" class="stock-header__clear">
        {!! pll__('Alle filters wissen') !!}
      </div>
      <div class="stock-header__active-filters">
        {!! facetwp_display( 'selections' ) !!}
      </div>
      <div class="stock-header__total">
        @if(pll_current_language() == 'nl')
          {!! facetwp_display( 'facet', 'total_nl' ) !!}
        @elseif(pll_current_language() == 'fr')
          {!! facetwp_display( 'facet', 'total_fr' ) !!}
        @endif
      </div>

      <div class="stock-header__sort">
        <div class="stock-header__sort-label">
          {!! pll__('Sorteren op') !!}
        </div>
        <div class="stock-header__sort-dropdown">
          {!!  facetwp_display( 'sort' ) !!}
        </div>
      </div>
      <div class="stock-compare py-6">
        <div class="wrap">
          <div class="row mb-3">
            <div class="col">
              <h3 class="stock-compare-title">
                {!! pll__('Wagens vergelijken') !!}
              </h3>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-8">
              <div class="compare-description">
                {!! App::wysiwyg_strip($compare_text) !!}
              </div>
            </div>
            <div class="col-md-4">
              <div class="compare-buttons">
                <a href="#"
                   class="button button--ghost js-compare-close mr-1 mb-1">
                  {!! pll__('Sluiten') !!}
                </a>
                <a href="{{ get_the_permalink($options->compare_page) }}"
                   class="button button--disabled button--arrow js-compare-go">
                  {!! pll__('Vergelijk de voertuigen') !!}
                </a>
              </div>
            </div>
          </div>
          <div class="row js-compare-boxes">
            @for($i = 0; $i < 3; $i++)
              <div class="col-md-4 compare-box" data-id="">
                <div class="compare__add-car">
                  <div class="compare__add-car-icon"></div>
                  <div class="compare__add-car-text">
                    {!! pll__('Voeg een wagen toe') !!}
                  </div>
                </div>
                <div class="compare__car">
                  <div class="compare__car-image">
                    <img src="//placehold.it/400x400" alt="">
                    <div class="compare__car-remove js-compare-remove">
                    </div>
                  </div>
                  <div class="compare__car-inner">
                    <div class="compare__car-info">
                      <div>
                        <div class="compare__car-title">
                        </div>
                        <div class="compare__car-type">
                        </div>
                      </div>


                      <div class="compare__car-price-wrap">
                        <div class="compare__car-price-label">
                          {!! pll__('Momenteel voor') !!}
                        </div>
                        <div
                          class="compare__car-price highlight highlight--half">
                        </div>
                      </div>
                    </div>

                    <div class="compare__car-description">
                    </div>
                  </div>

                </div>
              </div>
            @endfor
          </div>
        </div>
      </div>
    </div>


    <div class="wrap mb-10">


      <div class="row stock-actions-row">
        <div class="col stock-filters__mobile">
          <div class="icon icon--filter"></div>
          Filter by
        </div>
        <div class="col">
          <div class="stock-actions">
            <a href="#"
               class="button button--ghost button--small stock-action__compare js-stock-compare-link">
              {!! pll__('Compare') !!}
              <span class="counter">0</span>
            </a>
            <a href="{{ get_the_permalink() }}"
               class="stock-action__save js-stock-save-link">
              <span class="icon icon--wishlist"></span>
              <span class="counter">0</span>
            </a>
          </div>
        </div>
      </div>

      {{-- Results --}}
      <div class="row mt-8">
        <div class="col-md-3">

          {{-- Filters --}}
          <div class="stock-filters">

            {{-- Make checkbox filter --}}
            @include('partials.stock-filter', [
              'facet' => 'make',
              'title' => pll__('Merk')
              ])

            {{-- Condition checkbox filter --}}
            @include('partials.stock-filter', [
              'facet' => 'condition',
              'title' => 'Staat'
              ])

            {{-- Model checkbox filter --}}
            @include('partials.stock-filter', [
              'facet' => 'model',
              'title' => 'Model'
              ])

            {{-- Price slider filter --}}
            @include('partials.stock-filter', [
              'facet' => 'price',
              'title' => 'Prijs'
              ])

            {{-- Mileage slider filter --}}
            @include('partials.stock-filter', [
              'facet' => 'mileage',
              'title' => 'Km'
              ])

            {{-- Year slider filter --}}
            @include('partials.stock-filter', [
              'facet' => 'year',
              'title' => 'Jaar'
              ])

            {{-- Transmission checkbox filter --}}
            @include('partials.stock-filter', [
              'facet' => 'transmission',
              'title' => 'Transmissie'
              ])

            {{-- Fuel checkbox filter --}}
            @include('partials.stock-filter', [
              'facet' => 'fuel',
              'title' => 'Brandstof'
              ])
          </div>
        </div>

        {{-- Results --}}
        <div class="col-md-9">
          <div class="stock-results" id="carflow_stock">
            @while ($stock->have_posts()) @php $stock->the_post() @endphp
            @include('partials.stock-item')
            @endwhile
            {!! facetwp_display( 'facet', 'pagination' ) !!}
          </div>

          <a href="https://stock.renault.be/{{ pll_current_language() }}@if($options->dealer_renault_stock_name)?f={{ $options->dealer_renault_stock_name }}@endif"
             class="button button--ghost">
            {!! pll__('Niet gevonden wat je zoekt?') !!}
          </a>
        </div>

      </div>
    </div>

  </div>
@endsection

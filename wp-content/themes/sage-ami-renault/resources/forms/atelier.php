<?php
/**
 * Caldera Forms - PHP Export
 * NL - Atelier
 * @see https://calderaforms.com/doc/exporting-caldera-forms/
 * @version    1.9.2
 * @license   GPL-2.0+
 *
 */


/**
 * Hooks to load form.
 * Remove "caldera_forms_admin_forms" if you do not want this form to show in admin entry viewer
 */
add_filter("caldera_forms_get_forms", "slug_register_caldera_forms_atelier");
add_filter("caldera_forms_admin_forms", "slug_register_caldera_forms_atelier");
/**
 * Add form to front-end and admin
 *
 * @param array $forms All registered forms
 *
 * @return array
 */
function slug_register_caldera_forms_atelier($forms)
{
    $forms["atelier"] = apply_filters("caldera_forms_get_form-atelier", array());
    return $forms;
}

;

/**
 * Filter form request to include form structure to be rendered
 *
 * @param $form array form structure
 * @since 1.3.1
 *
 */
add_filter('caldera_forms_get_form-atelier', function ($form) {
    return array(
        'ID' => 'atelier',
        '_last_updated' => 'Tue, 29 Sep 2020 11:37:15 +0000',
        'cf_version' => '1.9.2',
        'name' => 'Atelier',
        'scroll_top' => 0,
        'success' => pll__('Formulier is verzonden. Bedankt.'),
        'db_support' => 1,
        'pinned' => 1,
        'pin_roles' =>
            array(
                'all_roles' => 1,
            ),
        'hide_form' => 1,
        'check_honey' => 1,
        'avatar_field' => NULL,
        'form_ajax' => 1,
        'custom_callback' => '',
        'layout_grid' =>
            array(
                'fields' =>
                    array(
                        'fld_7925448' => '1:1',
                        'fld_concession' => '1:1',
                        'fld_602988' => '1:1',
                        'fld_6832118' => '1:1',
                        'fld_4043974' => '1:1',
                        'fld_7917472' => '2:1',
                        'fld_1699037' => '2:1',
                        'fld_5889824' => '3:1',
                        'fld_6483745' => '3:2',
                        'fld_8639417' => '4:1',
                        'fld_5173322' => '4:2',
                        'fld_7036788' => '5:1',
                        'fld_2157170' => '5:1',
                        'fld_370540' => '5:1',
                    ),
                'structure' => '12#12|6:6|6:6|12',
            ),
        'fields' =>
            array(
                'fld_7925448' =>
                    array(
                        'ID' => 'fld_7925448',
                        'type' => 'html',
                        'label' => 'html__fld_7925448',
                        'slug' => 'html_fld_7925448',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Kies uw datum, locatie en maak een afspraak</h2>'),
                            ),
                    ),
                'fld_concession' =>
                    array(
                        'ID' => 'fld_concession',
                        'type' => 'dropdown',
                        'label' => pll__('Locatie'),
                        'slug' => 'location',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto' => 1,
                                'auto_type' => 'post_type',
                                'taxonomy' => 'category',
                                'post_type' => 'concession',
                                'value_field' => 'id',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_602988' =>
                    array(
                        'ID' => 'fld_602988',
                        'type' => 'date_picker',
                        'label' => pll__('Gewenste datum van afspraak'),
                        'slug' => 'date',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => 'input-date',
                                'default' => '',
                                'format' => 'dd-mm-yyyy',
                                'autoclose' => 1,
                                'start_view' => 'month',
                                'start_date' => '0d',
                                'end_date' => '',
                                'language' => 'nl',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_6832118' =>
                    array(
                        'ID' => 'fld_6832118',
                        'type' => 'dropdown',
                        'label' => pll__('Gewenst tijdstip van afspraak'),
                        'slug' => 'time',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => 'input-time',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'option' =>
                                    array(
                                        'opt794466' =>
                                            array(
                                                'calc_value' => '6:00',
                                                'value' => '6:00',
                                                'label' => '6:00',
                                            ),
                                        'opt1449585' =>
                                            array(
                                                'calc_value' => '6:30',
                                                'value' => '6:30',
                                                'label' => '6:30',
                                            ),
                                        'opt2961569' =>
                                            array(
                                                'calc_value' => '7:00',
                                                'value' => '7:00',
                                                'label' => '7:00',
                                            ),
                                        'opt3949866' =>
                                            array(
                                                'calc_value' => '7:30',
                                                'value' => '7:30',
                                                'label' => '7:30',
                                            ),
                                        'opt4353760' =>
                                            array(
                                                'calc_value' => '8:00',
                                                'value' => '8:00',
                                                'label' => '8:00',
                                            ),
                                        'opt6031982' =>
                                            array(
                                                'calc_value' => '8:30',
                                                'value' => '8:30',
                                                'label' => '8:30',
                                            ),
                                        'opt6588888' =>
                                            array(
                                                'calc_value' => '9:00',
                                                'value' => '9:00',
                                                'label' => '9:00',
                                            ),
                                        'opt8300847' =>
                                            array(
                                                'calc_value' => '9:30',
                                                'value' => '9:30',
                                                'label' => '9:30',
                                            ),
                                        'opt9287249' =>
                                            array(
                                                'calc_value' => '10:00',
                                                'value' => '10:00',
                                                'label' => '10:00',
                                            ),
                                        'opt9524820' =>
                                            array(
                                                'calc_value' => '10:30',
                                                'value' => '10:30',
                                                'label' => '10:30',
                                            ),
                                        'opt11080051' =>
                                            array(
                                                'calc_value' => '11:00',
                                                'value' => '11:00',
                                                'label' => '11:00',
                                            ),
                                        'opt12490473' =>
                                            array(
                                                'calc_value' => '11:30',
                                                'value' => '11:30',
                                                'label' => '11:30',
                                            ),
                                        'opt13550995' =>
                                            array(
                                                'calc_value' => '12:00',
                                                'value' => '12:00',
                                                'label' => '12:00',
                                            ),
                                        'opt13845593' =>
                                            array(
                                                'calc_value' => '12:30',
                                                'value' => '12:30',
                                                'label' => '12:30',
                                            ),
                                        'opt15585411' =>
                                            array(
                                                'calc_value' => '13:00',
                                                'value' => '13:00',
                                                'label' => '13:00',
                                            ),
                                        'opt15992066' =>
                                            array(
                                                'calc_value' => '13:30',
                                                'value' => '13:30',
                                                'label' => '13:30',
                                            ),
                                        'opt17216010' =>
                                            array(
                                                'calc_value' => '14:00',
                                                'value' => '14:00',
                                                'label' => '14:00',
                                            ),
                                        'opt18758380' =>
                                            array(
                                                'calc_value' => '14:30',
                                                'value' => '14:30',
                                                'label' => '14:30',
                                            ),
                                        'opt18891861' =>
                                            array(
                                                'calc_value' => '15:00',
                                                'value' => '15:00',
                                                'label' => '15:00',
                                            ),
                                        'opt20684955' =>
                                            array(
                                                'calc_value' => '15:30',
                                                'value' => '15:30',
                                                'label' => '15:30',
                                            ),
                                        'opt21888764' =>
                                            array(
                                                'calc_value' => '16:00',
                                                'value' => '16:00',
                                                'label' => '16:00',
                                            ),
                                        'opt22591031' =>
                                            array(
                                                'calc_value' => '16:30',
                                                'value' => '16:30',
                                                'label' => '16:30',
                                            ),
                                        'opt23247394' =>
                                            array(
                                                'calc_value' => '17:00',
                                                'value' => '17:00',
                                                'label' => '17:00',
                                            ),
                                        'opt24635773' =>
                                            array(
                                                'calc_value' => '17:30',
                                                'value' => '17:30',
                                                'label' => '17:30',
                                            ),
                                        'opt25175811' =>
                                            array(
                                                'calc_value' => '18:00',
                                                'value' => '18:00',
                                                'label' => '18:00',
                                            ),
                                        'opt26488003' =>
                                            array(
                                                'calc_value' => '18:30',
                                                'value' => '18:30',
                                                'label' => '18:30',
                                            ),
                                        'opt28297584' =>
                                            array(
                                                'calc_value' => '19:00',
                                                'value' => '19:00',
                                                'label' => '19:00',
                                            ),
                                        'opt28361356' =>
                                            array(
                                                'calc_value' => '19:30',
                                                'value' => '19:30',
                                                'label' => '19:30',
                                            ),
                                        'opt29560595' =>
                                            array(
                                                'calc_value' => '20:00',
                                                'value' => '20:00',
                                                'label' => '20:00',
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_4043974' =>
                    array(
                        'ID' => 'fld_4043974',
                        'type' => 'button',
                        'label' => pll__('Volgende'),
                        'slug' => 'volgende',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'next',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
                'fld_7917472' =>
                    array(
                        'ID' => 'fld_7917472',
                        'type' => 'html',
                        'label' => 'html__fld_7917472',
                        'slug' => 'html_fld_7917472',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Gelieve uw persoonlijke informatie in te vullen</h2>'),
                            ),
                    ),
                'fld_1699037' =>
                    array(
                        'ID' => 'fld_1699037',
                        'type' => 'radio',
                        'label' => pll__('Aanspreking'),
                        'slug' => 'title',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt966614' =>
                                            array(
                                                'calc_value' => 'Mevr.',
                                                'value' => 'Mrs.',
                                                'label' => pll__('Mevr.'),
                                            ),
                                        'opt2079643' =>
                                            array(
                                                'calc_value' => 'Dhr.',
                                                'value' => 'Mr.',
                                                'label' => pll__('Dhr.'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5889824' =>
                    array(
                        'ID' => 'fld_5889824',
                        'type' => 'text',
                        'label' => pll__('Wat is uw voornaam'),
                        'slug' => 'firstname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_6483745' =>
                    array(
                        'ID' => 'fld_6483745',
                        'type' => 'text',
                        'label' => pll__('Wat is uw familienaam'),
                        'slug' => 'lastname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_8639417' =>
                    array(
                        'ID' => 'fld_8639417',
                        'type' => 'email',
                        'label' => pll__('Wat is uw email adres'),
                        'slug' => 'email',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5173322' =>
                    array(
                        'ID' => 'fld_5173322',
                        'type' => 'phone_better',
                        'label' => pll__('Telefoonnummer'),
                        'slug' => 'mobilephone',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_7036788' =>
                    array(
                        'ID' => 'fld_7036788',
                        'type' => 'radio',
                        'label' => pll__('Op de hoogte blijven van onze beste aanbiedingen en het laatste nieuws?'),
                        'slug' => 'marketingoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt1319058' =>
                                            array(
                                                'calc_value' => 'Ja, dat wil ik niet missen',
                                                'value' => 'Y',
                                                'label' => pll__('Ja, dat wil ik niet missen'),
                                            ),
                                        'opt1412629' =>
                                            array(
                                                'calc_value' => 'Nee, dat weiger ik',
                                                'value' => 'N',
                                                'label' => pll__('Nee, dat weiger ik'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_2157170' =>
                    array(
                        'ID' => 'fld_2157170',
                        'type' => 'checkbox',
                        'label' => pll__('Gelieve de algemene voorwaarden te accepteren'),
                        'slug' => 'tosoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'option' =>
                                    array(
                                        'opt2081117' =>
                                            array(
                                                'calc_value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'label' => pll__('Ja, ik ga akkoord met de algemene voorwaarden'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_370540' =>
                    array(
                        'ID' => 'fld_370540',
                        'type' => 'button',
                        'label' => pll__('Verzenden'),
                        'slug' => 'verzenden',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'submit',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
            ),
        'auto_progress' => 1,
        'page_names' =>
            array(
                0 => pll__('Kies een datum, locatie, en maak een afspraak'),
                1 => pll__('Vul uw persoonlijke informatie in'),
            ),
        'mailer' =>
            array(
//                'on_insert' => 1,
                'sender_name' => 'Caldera Forms Notification',
                'sender_email' => 'woutervandewille@gmail.com',
                'reply_to' => '',
                'email_type' => 'html',
                'recipients' => '',
                'bcc_to' => '',
                'email_subject' => pll__('Test Drive'),
                'email_message' => '{summary}',
            ),
        'variables' =>
            array(
                'keys' =>
                    array(
                        0 => 'type',
                        1 => 'dealer',
                    ),
                'values' =>
                    array(
                        0 => 'atelier',
                        1 => get_field('dealer_name', 'options'),
                    ),
                'types' =>
                    array(
                        0 => 'passback',
                        1 => 'passback',
                    ),
            ),
        'settings' =>
            array(
                'responsive' =>
                    array(
                        'break_point' => 'sm',
                    ),
            ),
        'conditional_groups' =>
            array(
                'conditions' =>
                    array(),
            ),
        'processors' =>
            array(),
        'privacy_exporter_enabled' => false,
        'version' => '1.9.2',
        'db_id' => '190',
        'type' => 'primary',
        '_external_form' => 1,
    );
});

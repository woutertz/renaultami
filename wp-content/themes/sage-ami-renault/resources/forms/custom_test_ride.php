<?php
/**
 * Caldera Forms - PHP Export
 * NL - Custom Test Ride
 * @see https://calderaforms.com/doc/exporting-caldera-forms/
 * @version    1.9.2
 * @license   GPL-2.0+
 *
 */


/**
 * Hooks to load form.
 * Remove "caldera_forms_admin_forms" if you do not want this form to show in admin entry viewer
 */
add_filter("caldera_forms_get_forms", "slug_register_caldera_forms_customtestride");
add_filter("caldera_forms_admin_forms", "slug_register_caldera_forms_customtestride");
/**
 * Add form to front-end and admin
 *
 * @param array $forms All registered forms
 *
 * @return array
 */
function slug_register_caldera_forms_customtestride($forms)
{
    $forms["custom_test_ride"] = apply_filters("caldera_forms_get_form-custom_test_ride", array());
    return $forms;
}

;

/**
 * Filter form request to include form structure to be rendered
 *
 * @param $form array form structure
 * @since 1.3.1
 *
 */
add_filter('caldera_forms_get_form-custom_test_ride', function ($form) {
    return array(
        'ID' => 'custom_test_ride',
        '_last_updated' => 'Tue, 29 Sep 2020 07:20:38 +0000',
        'cf_version' => '1.9.2',
        'name' => 'Custom Test Ride',
        'scroll_top' => 0,
        'success' => pll__('Formulier is verzonden. Bedankt.'),
        'db_support' => 1,
        'pinned' => 1,
        'hide_form' => 1,
        'check_honey' => 1,
        'avatar_field' => '',
        'form_ajax' => 1,
        'custom_callback' => '',
        'layout_grid' =>
            array(
                'fields' =>
                    array(
                        'fld_326888' => '1:1',
                        'fld_1034170' => '1:1',
                        'fld_concession' => '1:1',
                        'fld_1493263' => '1:1',
                        'fld_171644' => '2:1',
                        'fld_426074' => '2:1',
                        'fld_1818195' => '2:1',
                        'fld_7108395' => '3:1',
                        'fld_4785533' => '3:2',
                        'fld_2379424' => '4:1',
                        'fld_3409284' => '4:2',
                        'fld_3637244' => '5:1',
                        'fld_6759934' => '5:1',
                        'fld_3955077' => '5:1',
                    ),
                'structure' => '12#12|6:6|6:6|12',
            ),
        'fields' =>
            array(
                'fld_concession' =>
                    array(
                        'ID' => 'fld_concession',
                        'type' => 'dropdown',
                        'label' => pll__('Locatie'),
                        'slug' => 'location',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto' => 1,
                                'auto_type' => 'post_type',
                                'taxonomy' => 'category',
                                'post_type' => 'concession',
                                'value_field' => 'id',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_326888' =>
                    array(
                        'ID' => 'fld_326888',
                        'type' => 'html',
                        'label' => 'Title',
                        'slug' => 'html__fld_326888',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Kies een model</h2>'),
                            ),
                    ),
                'fld_1034170' =>
                    array(
                        'ID' => 'fld_1034170',
                        'type' => 'dropdown',
                        'label' => pll__('Kies een model'),
                        'slug' => 'model_dropdown_test_ride',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto' => 1,
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'model',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_1493263' =>
                    array(
                        'ID' => 'fld_1493263',
                        'type' => 'button',
                        'label' => pll__('Volgende'),
                        'slug' => 'volgende',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'next',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
                'fld_171644' =>
                    array(
                        'ID' => 'fld_171644',
                        'type' => 'html',
                        'label' => 'html__fld_171644',
                        'slug' => 'html__fld_171644',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Gelieve uw persoonlijke informatie in te vullen</h2>'),
                            ),
                    ),
                'fld_426074' =>
                    array(
                        'ID' => 'fld_426074',
                        'type' => 'radio',
                        'label' => pll__('Aanspreking'),
                        'slug' => 'title',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt339770' =>
                                            array(
                                                'calc_value' => 'Mevr.',
                                                'value' => 'Mrs.',
                                                'label' => pll__('Mevr.'),
                                            ),
                                        'opt2002695' =>
                                            array(
                                                'calc_value' => 'Dhr.',
                                                'value' => 'Mr.',
                                                'label' => pll__('Dhr.'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_1818195' =>
                    array(
                        'ID' => 'fld_1818195',
                        'type' => 'hidden',
                        'label' => 'Post',
                        'slug' => 'post',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => '%model_dropdown_test_ride%',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_7108395' =>
                    array(
                        'ID' => 'fld_7108395',
                        'type' => 'text',
                        'label' => pll__('Wat is uw voornaam'),
                        'slug' => 'firstname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_4785533' =>
                    array(
                        'ID' => 'fld_4785533',
                        'type' => 'text',
                        'label' => pll__('Wat is uw familienaam'),
                        'slug' => 'lastname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_2379424' =>
                    array(
                        'ID' => 'fld_2379424',
                        'type' => 'email',
                        'label' => pll__('Wat is uw email adres'),
                        'slug' => 'email',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_3409284' =>
                    array(
                        'ID' => 'fld_3409284',
                        'type' => 'phone_better',
                        'label' => pll__('Wat is uw telefoonnummer'),
                        'slug' => 'mobilephone',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_3637244' =>
                    array(
                        'ID' => 'fld_3637244',
                        'type' => 'radio',
                        'label' => pll__('Op de hoogte blijven van onze beste aanbiedingen en het laatste nieuws?'),
                        'slug' => 'marketingoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt1851316' =>
                                            array(
                                                'calc_value' => 'Ja, dat wil ik niet missen',
                                                'value' => 'Y',
                                                'label' => pll__('Ja, dat wil ik niet missen'),
                                            ),
                                        'opt1326833' =>
                                            array(
                                                'calc_value' => 'Nee, dat weiger ik',
                                                'value' => 'N',
                                                'label' => pll__('Nee, dat weiger ik'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_6759934' =>
                    array(
                        'ID' => 'fld_6759934',
                        'type' => 'checkbox',
                        'label' => pll__('Gelieve de algemene voorwaarden te accepteren'),
                        'slug' => 'tosoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'option' =>
                                    array(
                                        'opt1551547' =>
                                            array(
                                                'calc_value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'label' => pll__('Ja, ik ga akkoord met de algemene voorwaarden'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_3955077' =>
                    array(
                        'ID' => 'fld_3955077',
                        'type' => 'button',
                        'label' => pll__('Verzenden'),
                        'slug' => 'verzenden',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'submit',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
            ),
        'auto_progress' => 1,
        'page_names' =>
            array(
                0 => pll__('Kies een model'),
                1 => pll__('Vul uw persoonlijke informatie in'),
            ),
        'mailer' =>
            array(
//                'on_insert' => 1,
                'sender_name' => 'Caldera Forms Notification',
                'sender_email' => 'woutervandewille@gmail.com',
                'reply_to' => '',
                'email_type' => 'html',
                'recipients' => '',
                'bcc_to' => '',
                'email_subject' => pll__('Custom Test Ride'),
                'email_message' => '{summary}',
            ),
        'variables' =>
            array(
                'keys' =>
                    array(
                        0 => 'type',
                        1 => 'dealer',
                        2 => 'model',
                    ),
                'values' =>
                    array(
                        0 => 'test_drive',
                        1 => get_field('dealer_name', 'options'),
                        2 => '%model_dropdown_test_ride%',
                    ),
                'types' =>
                    array(
                        0 => 'passback',
                        1 => 'passback',
                        2 => 'passback',
                    ),
            ),
        'settings' =>
            array(
                'responsive' =>
                    array(
                        'break_point' => 'sm',
                    ),
            ),
        'conditional_groups' =>
            array(
                'conditions' =>
                    array(),
            ),
        'processors' =>
            array(),
        'privacy_exporter_enabled' => false,
        'version' => '1.9.2',
        'db_id' => '189',
        'type' => 'primary',
        '_external_form' => 1,
    );
});

<?php
/**
 * Caldera Forms - PHP Export
 * NL - Quote
 * @see https://calderaforms.com/doc/exporting-caldera-forms/
 * @version    1.9.2
 * @license   GPL-2.0+
 *
 */


/**
 * Hooks to load form.
 * Remove "caldera_forms_admin_forms" if you do not want this form to show in admin entry viewer
 */
add_filter("caldera_forms_get_forms", "slug_register_caldera_forms_quote");
add_filter("caldera_forms_admin_forms", "slug_register_caldera_forms_quote");
/**
 * Add form to front-end and admin
 *
 * @param array $forms All registered forms
 *
 * @return array
 */
function slug_register_caldera_forms_quote($forms)
{
    $forms["quote"] = apply_filters("caldera_forms_get_form-quote", array());
    return $forms;
}

;

/**
 * Filter form request to include form structure to be rendered
 *
 * @param $form array form structure
 * @since 1.3.1
 *
 */
add_filter('caldera_forms_get_form-quote', function ($form) {
    return array(
        'ID' => 'quote',
        '_last_updated' => 'Fri, 25 Sep 2020 08:08:20 +0000',
        'cf_version' => '1.9.2',
        'name' => 'Quote',
        'scroll_top' => 0,
        'success' => pll__('Formulier is verzonden. Bedankt.'),
        'db_support' => 1,
        'pinned' => 1,
        'pin_roles' =>
            array(
                'all_roles' => 1,
            ),
        'hide_form' => 1,
        'check_honey' => 1,
        'avatar_field' => NULL,
        'form_ajax' => 1,
        'custom_callback' => '',
        'layout_grid' =>
            array(
                'fields' =>
                    array(
                        'fld_9080096' => '1:1',
                        'fld_2636073' => '1:1',
                        'fld_9967311' => '2:1',
                        'fld_concession' => '2:2',
                        'fld_2136250' => '2:1',
                        'fld_9876911' => '2:2',
                        'fld_5038331' => '3:1',
                        'fld_5125329' => '3:2',
                        'fld_341813' => '4:1',
                        'fld_5037493' => '4:1',
                        'fld_5590880' => '4:1',
                    ),
                'structure' => '12|6:6|6:6|12',
            ),
        'fields' =>
            array(
                'fld_concession' =>
                    array(
                        'ID' => 'fld_concession',
                        'type' => 'dropdown',
                        'label' => pll__('Locatie'),
                        'slug' => 'location',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto' => 1,
                                'auto_type' => 'post_type',
                                'taxonomy' => 'category',
                                'post_type' => 'concession',
                                'value_field' => 'id',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_9080096' =>
                    array(
                        'ID' => 'fld_9080096',
                        'type' => 'html',
                        'label' => 'html__fld_9080096',
                        'slug' => 'html__fld_9080096',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Gelieve uw persoonlijke informatie in te vullen</h2>'),
                            ),
                    ),
                'fld_2636073' =>
                    array(
                        'ID' => 'fld_2636073',
                        'type' => 'hidden',
                        'label' => 'Post',
                        'slug' => 'post',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => '{embed_post:ID}',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_9967311' =>
                    array(
                        'ID' => 'fld_9967311',
                        'type' => 'radio',
                        'label' => pll__('Aanspreking'),
                        'slug' => 'title',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt63837' =>
                                            array(
                                                'calc_value' => 'Mevr.',
                                                'value' => 'Mrs.',
                                                'label' => pll__('Mevr.'),
                                            ),
                                        'opt1654422' =>
                                            array(
                                                'calc_value' => 'Dhr.',
                                                'value' => 'Mr.',
                                                'label' => pll__('Dhr.'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_2136250' =>
                    array(
                        'ID' => 'fld_2136250',
                        'type' => 'text',
                        'label' => pll__('Wat is uw voornaam'),
                        'slug' => 'firstname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_9876911' =>
                    array(
                        'ID' => 'fld_9876911',
                        'type' => 'text',
                        'label' => pll__('Wat is uw familienaam'),
                        'slug' => 'lastname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5038331' =>
                    array(
                        'ID' => 'fld_5038331',
                        'type' => 'email',
                        'label' => pll__('Wat is uw email adres'),
                        'slug' => 'email',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5125329' =>
                    array(
                        'ID' => 'fld_5125329',
                        'type' => 'phone_better',
                        'label' => pll__('Wat is uw telefoonnummer'),
                        'slug' => 'mobilephone',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_341813' =>
                    array(
                        'ID' => 'fld_341813',
                        'type' => 'radio',
                        'label' => pll__('Op de hoogte blijven van onze beste aanbiedingen en het laatste nieuws?'),
                        'slug' => 'marketingoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt158439' =>
                                            array(
                                                'calc_value' => 'Ja, dat wil ik niet missen',
                                                'value' => 'Y',
                                                'label' => pll__('Ja, dat wil ik niet missen'),
                                            ),
                                        'opt1426517' =>
                                            array(
                                                'calc_value' => 'Nee, dat weiger ik',
                                                'value' => 'N',
                                                'label' => pll__('Nee, dat weiger ik'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5037493' =>
                    array(
                        'ID' => 'fld_5037493',
                        'type' => 'checkbox',
                        'label' => pll__('Gelieve de algemene voorwaarden te accepteren'),
                        'slug' => 'tosoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'option' =>
                                    array(
                                        'opt1844296' =>
                                            array(
                                                'calc_value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'label' => pll__('Ja, ik ga akkoord met de algemene voorwaarden'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5590880' =>
                    array(
                        'ID' => 'fld_5590880',
                        'type' => 'button',
                        'label' => pll__('Verzenden'),
                        'slug' => 'verzenden',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'submit',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
            ),
        'auto_progress' => 1,
        'page_names' =>
            array(
                0 => 'Page 1',
            ),
        'mailer' =>
            array(
//                'on_insert' => 1,
                'sender_name' => 'Caldera Forms Notification',
                'sender_email' => 'woutervandewille@gmail.com',
                'reply_to' => '',
                'email_type' => 'html',
                'recipients' => '',
                'bcc_to' => '',
                'email_subject' => pll__('Quote request'),
                'email_message' => '{summary}',
            ),
        'variables' =>
            array(
                'keys' =>
                    array(
                        0 => 'type',
                        1 => 'dealer',
                        2 => 'model',
                    ),
                'values' =>
                    array(
                        0 => 'quote',
                        1 => get_field('dealer_name', 'options'),
                        2 => '{embed_post:post_title}',
                    ),
                'types' =>
                    array(
                        0 => 'passback',
                        1 => 'passback',
                        2 => 'passback',
                    ),
            ),
        'settings' =>
            array(
                'responsive' =>
                    array(
                        'break_point' => 'sm',
                    ),
            ),
        'conditional_groups' =>
            array(
                'conditions' =>
                    array(),
            ),
        'processors' =>
            array(),
        'privacy_exporter_enabled' => false,
        'version' => '1.9.2',
        'db_id' => '175',
        'type' => 'primary',
        '_external_form' => 1,
    );
});

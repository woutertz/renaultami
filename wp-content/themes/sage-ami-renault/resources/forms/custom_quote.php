<?php
/**
 * Caldera Forms - PHP Export
 * NL - Custom Quote
 * @see https://calderaforms.com/doc/exporting-caldera-forms/
 * @version    1.9.2
 * @license   GPL-2.0+
 *
 */


/**
 * Hooks to load form.
 * Remove "caldera_forms_admin_forms" if you do not want this form to show in admin entry viewer
 */
add_filter("caldera_forms_get_forms", "slug_register_caldera_forms_customquote");
add_filter("caldera_forms_admin_forms", "slug_register_caldera_forms_customquote");
/**
 * Add form to front-end and admin
 *
 * @param array $forms All registered forms
 *
 * @return array
 */
function slug_register_caldera_forms_customquote($forms)
{
    $forms["custom_quote"] = apply_filters("caldera_forms_get_form-custom_quote", array());
    return $forms;
}

;

/**
 * Filter form request to include form structure to be rendered
 *
 * @param $form array form structure
 * @since 1.3.1
 *
 */
add_filter('caldera_forms_get_form-custom_quote', function ($form) {
    return array(
        'ID' => 'custom_quote',
        '_last_updated' => 'Tue, 29 Sep 2020 07:20:13 +0000',
        'cf_version' => '1.9.2',
        'name' => 'Custom Quote',
        'scroll_top' => 0,
        'success' => pll__('Formulier is verzonden. Bedankt.'),
        'db_support' => 1,
        'pinned' => 1,
        'hide_form' => 1,
        'check_honey' => 1,
        'avatar_field' => '',
        'form_ajax' => 1,
        'custom_callback' => '',
        'layout_grid' =>
            array(
                'fields' =>
                    array(
                        'fld_3795522' => '1:1',
                        'fld_171626' => '1:1',
                        'fld_concession' => '1:1',
                        'fld_3979549' => '1:1',
                        'fld_9161863' => '2:1',
                        'fld_1890283' => '2:1',
                        'fld_8070138' => '2:1',
                        'fld_6909379' => '3:1',
                        'fld_2243046' => '3:2',
                        'fld_169927' => '4:1',
                        'fld_9387100' => '4:2',
                        'fld_7440740' => '5:1',
                        'fld_2052308' => '5:1',
                        'fld_2276253' => '5:1',
                    ),
                'structure' => '12#12|6:6|6:6|12',
            ),
        'fields' =>
            array(
                'fld_concession' =>
                    array(
                        'ID' => 'fld_concession',
                        'type' => 'dropdown',
                        'label' => pll__('Locatie'),
                        'slug' => 'location',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto' => 1,
                                'auto_type' => 'post_type',
                                'taxonomy' => 'category',
                                'post_type' => 'concession',
                                'value_field' => 'id',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_3795522' =>
                    array(
                        'ID' => 'fld_3795522',
                        'type' => 'html',
                        'label' => 'html__fld_3795522',
                        'slug' => 'html__fld_3795522',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Kies een model</h2>'),
                            ),
                    ),
                'fld_171626' =>
                    array(
                        'ID' => 'fld_171626',
                        'type' => 'dropdown',
                        'label' => pll__('Kies een model'),
                        'slug' => 'model_dropdown_quote',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto' => 1,
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'model',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_3979549' =>
                    array(
                        'ID' => 'fld_3979549',
                        'type' => 'button',
                        'label' => pll__('Volgende'),
                        'slug' => 'volgende',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'next',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
                'fld_9161863' =>
                    array(
                        'ID' => 'fld_9161863',
                        'type' => 'html',
                        'label' => 'html__fld_9161863',
                        'slug' => 'html__fld_9161863',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Gelieve uw persoonlijke informatie in te vullen</h2>'),
                            ),
                    ),
                'fld_1890283' =>
                    array(
                        'ID' => 'fld_1890283',
                        'type' => 'radio',
                        'label' => pll__('Aanspreking'),
                        'slug' => 'title',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt687550' =>
                                            array(
                                                'calc_value' => 'Mevr.',
                                                'value' => 'Mrs.',
                                                'label' => pll__('Mevr.'),
                                            ),
                                        'opt1922329' =>
                                            array(
                                                'calc_value' => 'Dhr.',
                                                'value' => 'Mr.',
                                                'label' => pll__('Dhr.'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_8070138' =>
                    array(
                        'ID' => 'fld_8070138',
                        'type' => 'hidden',
                        'label' => 'Post',
                        'slug' => 'post',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => '%model_dropdown_quote%',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_6909379' =>
                    array(
                        'ID' => 'fld_6909379',
                        'type' => 'text',
                        'label' => pll__('Wat is uw voornaam'),
                        'slug' => 'firstname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_2243046' =>
                    array(
                        'ID' => 'fld_2243046',
                        'type' => 'text',
                        'label' => pll__('Wat is uw familienaam'),
                        'slug' => 'lastname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_169927' =>
                    array(
                        'ID' => 'fld_169927',
                        'type' => 'email',
                        'label' => pll__('Wat is uw email adres'),
                        'slug' => 'email',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_9387100' =>
                    array(
                        'ID' => 'fld_9387100',
                        'type' => 'phone_better',
                        'label' => pll__('Wat is uw telefoonnummer'),
                        'slug' => 'mobilephone',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_7440740' =>
                    array(
                        'ID' => 'fld_7440740',
                        'type' => 'radio',
                        'label' => pll__('Op de hoogte blijven van onze beste aanbiedingen en het laatste nieuws?'),
                        'slug' => 'marketingoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt1454590' =>
                                            array(
                                                'calc_value' => 'Ja, dat wil ik niet missen',
                                                'value' => 'Y',
                                                'label' => pll__('Ja, dat wil ik niet missen'),
                                            ),
                                        'opt1879803' =>
                                            array(
                                                'calc_value' => 'Nee, dat weiger ik',
                                                'value' => 'N',
                                                'label' => pll__('Nee, dat weiger ik'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_2052308' =>
                    array(
                        'ID' => 'fld_2052308',
                        'type' => 'checkbox',
                        'label' => pll__('Gelieve de algemene voorwaarden te accepteren'),
                        'slug' => 'tosoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'option' =>
                                    array(
                                        'opt1097663' =>
                                            array(
                                                'calc_value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'label' => pll__('Ja, ik ga akkoord met de algemene voorwaarden'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_2276253' =>
                    array(
                        'ID' => 'fld_2276253',
                        'type' => 'button',
                        'label' => pll__('Verzenden'),
                        'slug' => 'verzenden',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'submit',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
            ),
        'auto_progress' => 1,
        'page_names' =>
            array(
                0 => pll__('Kies een model'),
                1 => pll__('Vul uw persoonlijke informatie in'),
            ),
        'mailer' =>
            array(
//                'on_insert' => 1,
                'sender_name' => 'Caldera Forms Notification',
                'sender_email' => 'woutervandewille@gmail.com',
                'reply_to' => '',
                'email_type' => 'html',
                'recipients' => '',
                'bcc_to' => '',
                'email_subject' => pll__('Custom Quote'),
                'email_message' => '{summary}',
            ),
        'variables' =>
            array(
                'keys' =>
                    array(
                        0 => 'type',
                        1 => 'dealer',
                        2 => 'model',
                    ),
                'values' =>
                    array(
                        0 => 'quote',
                        1 => get_field('dealer_name', 'options'),
                        2 => '%model_dropdown_quote%',
                    ),
                'types' =>
                    array(
                        0 => 'passback',
                        1 => 'passback',
                        2 => 'passback',
                    ),
            ),
        'settings' =>
            array(
                'responsive' =>
                    array(
                        'break_point' => 'sm',
                    ),
            ),
        'conditional_groups' =>
            array(
                'conditions' =>
                    array(),
            ),
        'processors' =>
            array(),
        'privacy_exporter_enabled' => false,
        'version' => '1.9.2',
        'db_id' => '187',
        'type' => 'primary',
        '_external_form' => 1,
    );
});

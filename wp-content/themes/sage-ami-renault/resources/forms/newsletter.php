<?php
/**
 * Caldera Forms - PHP Export
 * NL - Newsletter
 * @see https://calderaforms.com/doc/exporting-caldera-forms/
 * @version    1.9.2
 * @license   GPL-2.0+
 *
 */


/**
                     * Hooks to load form.
                     * Remove "caldera_forms_admin_forms" if you do not want this form to show in admin entry viewer
                     */
                    add_filter( "caldera_forms_get_forms", "slug_register_caldera_forms_newsletter" );
                    add_filter( "caldera_forms_admin_forms", "slug_register_caldera_forms_newsletter" );
                    /**
                     * Add form to front-end and admin
                     *
                     * @param array $forms All registered forms
                     *
                     * @return array
                     */
                    function slug_register_caldera_forms_newsletter( $forms ) {
                        $forms["newsletter"] = apply_filters( "caldera_forms_get_form-newsletter", array() );
                        return $forms;
                    };

/**
 * Filter form request to include form structure to be rendered
 *
 * @since 1.3.1
 *
 * @param $form array form structure
 */
add_filter( 'caldera_forms_get_form-newsletter', function( $form ){
 return array(
  'ID' => 'newsletter',
  '_last_updated' => 'Tue, 08 Sep 2020 15:14:52 +0000',
  'cf_version' => '1.9.2',
  'name' => 'NL - Newsletter',
  'scroll_top' => 0,
  'success' => pll__('Formulier is verzonden. Bedankt.'),
  'db_support' => 1,
  'pinned' => 1,
  'hide_form' => 1,
  'check_honey' => 1,
  'avatar_field' => NULL,
  'form_ajax' => 1,
  'custom_callback' => '',
  'layout_grid' =>
  array(
    'fields' =>
    array(
      'fld_8180573' => '1:1',
      'fld_9821692' => '1:2',
    ),
    'structure' => '7:5',
  ),
  'fields' =>
  array(
    'fld_8180573' =>
    array(
      'ID' => 'fld_8180573',
      'type' => 'email',
      'label' => pll__('E-mailadres'),
      'hide_label' => 1,
      'slug' => 'email',
      'conditions' =>
      array(
        'type' => '',
      ),
      'caption' => '',
      'config' =>
      array(
        'custom_class' => '',
        'placeholder' => pll__('E-mailadres'),
        'default' => '',
        'email_identifier' => 0,
        'personally_identifying' => 0,
      ),
    ),
    'fld_9821692' =>
    array(
      'ID' => 'fld_9821692',
      'type' => 'button',
      'label' => pll__('Inschrijven'),
      'slug' => 'inschrijven',
      'conditions' =>
      array(
        'type' => '',
      ),
      'caption' => '',
      'config' =>
      array(
        'custom_class' => '',
        'type' => 'submit',
        'class' => 'button',
        'target' => '',
      ),
    ),
  ),
  'page_names' =>
  array(
    0 => 'Page 1',
  ),
  'mailer' =>
  array(
//    'on_insert' => 1,
    'sender_name' => 'Caldera Forms Notification',
    'sender_email' => 'woutervandewille@gmail.com',
    'reply_to' => '',
    'email_type' => 'html',
    'recipients' => '',
    'bcc_to' => '',
    'email_subject' => pll__('Newsletter'),
    'email_message' => '{summary}',
  ),
     'variables' =>
         array(
             'keys' =>
                 array(
                     0 => 'type',
                     1 => 'dealer',
                 ),
             'values' =>
                 array(
                     0 => 'newsletter',
                     1 => get_field('dealer_name', 'options'),
                 ),
             'types' =>
                 array(
                     0 => 'passback',
                     1 => 'passback',
                 ),
         ),
     'settings' =>
         array(
             'responsive' =>
                 array(
                     'break_point' => 'sm',
    ),
  ),
  'conditional_groups' =>
  array(
    'conditions' =>
    array(
    ),
  ),
  'processors' =>
  array(
  ),
  'privacy_exporter_enabled' => false,
  'version' => '1.9.2',
  'db_id' => '148',
  'type' => 'primary',
  '_external_form' => 1,
);
} );

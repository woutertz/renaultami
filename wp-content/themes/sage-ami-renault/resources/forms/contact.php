<?php
/**
 * Caldera Forms - PHP Export
 * NL - Contact
 * @see https://calderaforms.com/doc/exporting-caldera-forms/
 * @version    1.9.2
 * @license   GPL-2.0+
 *
 */


/**
 * Hooks to load form.
 * Remove "caldera_forms_admin_forms" if you do not want this form to show in admin entry viewer
 */
add_filter("caldera_forms_get_forms", "slug_register_caldera_forms_contact");
add_filter("caldera_forms_admin_forms", "slug_register_caldera_forms_contact");
/**
 * Add form to front-end and admin
 *
 * @param array $forms All registered forms
 *
 * @return array
 */
function slug_register_caldera_forms_contact($forms)
{
    $forms["contact"] = apply_filters("caldera_forms_get_form-contact", array());
    return $forms;
}

;

/**
 * Filter form request to include form structure to be rendered
 *
 * @param $form array form structure
 * @since 1.3.1
 *
 */
add_filter('caldera_forms_get_form-contact', function ($form) {
    return array(
        'ID' => 'contact',
        '_last_updated' => 'Fri, 25 Sep 2020 08:08:27 +0000',
        'cf_version' => '1.9.2',
        'name' => 'Contact',
        'scroll_top' => 0,
        'success' => pll__('Formulier is verzonden. Bedankt.'),
        'db_support' => 1,
        'pinned' => 1,
        'hide_form' => 1,
        'check_honey' => 1,
        'avatar_field' => NULL,
        'form_ajax' => 1,
        'custom_callback' => '',
        'layout_grid' =>
            array(
                'fields' =>
                    array(
                        'fld_6492706' => '1:1',
                        'fld_8502932' => '2:1',
                        'fld_concession' => '2:2',
                        'fld_8806465' => '2:1',
                        'fld_3305531' => '2:2',
                        'fld_2531281' => '3:1',
                        'fld_5452203' => '3:2',
                        'fld_5544254' => '4:1',
                        'fld_3854942' => '4:1',
                        'fld_7750224' => '4:1',
                        'fld_6710846' => '4:1',
                    ),
                'structure' => '12|6:6|6:6|12',
            ),
        'fields' =>
            array(
                'fld_concession' =>
                    array(
                        'ID' => 'fld_concession',
                        'type' => 'dropdown',
                        'label' => pll__('Locatie'),
                        'slug' => 'location',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default_option' => '',
                                'auto' => 1,
                                'auto_type' => 'post_type',
                                'taxonomy' => 'category',
                                'post_type' => 'concession',
                                'value_field' => 'id',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_6492706' =>
                    array(
                        'ID' => 'fld_6492706',
                        'type' => 'html',
                        'label' => 'html__fld_6492706',
                        'slug' => 'html__fld_6492706',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default' => pll__('<h2>Gelieve uw persoonlijke informatie in te vullen</h2>'),
                            ),
                    ),
                'fld_8502932' =>
                    array(
                        'ID' => 'fld_8502932',
                        'type' => 'radio',
                        'label' => pll__('Aanspreking'),
                        'slug' => 'title',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt424598' =>
                                            array(
                                                'calc_value' => 'Mevr.',
                                                'value' => 'Mrs.',
                                                'label' => pll__('Mevr.'),
                                            ),
                                        'opt1162444' =>
                                            array(
                                                'calc_value' => 'Dhr.',
                                                'value' => 'Mr.',
                                                'label' => pll__('Dhr.'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_8806465' =>
                    array(
                        'ID' => 'fld_8806465',
                        'type' => 'text',
                        'label' => pll__('Wat is uw voornaam'),
                        'slug' => 'firstname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_3305531' =>
                    array(
                        'ID' => 'fld_3305531',
                        'type' => 'text',
                        'label' => pll__('Wat is uw familienaam'),
                        'slug' => 'lastname',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'type_override' => 'text',
                                'mask' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_2531281' =>
                    array(
                        'ID' => 'fld_2531281',
                        'type' => 'email',
                        'label' => pll__('Wat is uw email adres'),
                        'slug' => 'email',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5452203' =>
                    array(
                        'ID' => 'fld_5452203',
                        'type' => 'phone_better',
                        'label' => pll__('Wat is uw telefoonnummer'),
                        'slug' => 'mobilephone',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_5544254' =>
                    array(
                        'ID' => 'fld_5544254',
                        'type' => 'paragraph',
                        'label' => pll__('Waarmee kunnen we u helpen?'),
                        'slug' => 'question',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'placeholder' => '',
                                'rows' => 4,
                                'default' => '',
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_3854942' =>
                    array(
                        'ID' => 'fld_3854942',
                        'type' => 'radio',
                        'label' => pll__('Op de hoogte blijven van onze beste aanbiedingen en het laatste nieuws?'),
                        'slug' => 'marketingoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt311007' =>
                                            array(
                                                'calc_value' => 'Ja, dat wil ik niet missen',
                                                'value' => 'Y',
                                                'label' => pll__('Ja, dat wil ik niet missen'),
                                            ),
                                        'opt1133287' =>
                                            array(
                                                'calc_value' => 'Nee, dat weiger ik',
                                                'value' => 'N',
                                                'label' => pll__('Nee, dat weiger ik'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_7750224' =>
                    array(
                        'ID' => 'fld_7750224',
                        'type' => 'checkbox',
                        'label' => pll__('Gelieve de algemene voorwaarden te accepteren'),
                        'slug' => 'tosoptin',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'required' => 1,
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'default_option' => '',
                                'auto_type' => '',
                                'taxonomy' => 'category',
                                'post_type' => 'post',
                                'value_field' => 'name',
                                'orderby_tax' => 'name',
                                'orderby_post' => 'name',
                                'order' => 'ASC',
                                'default' => '',
                                'show_values' => 1,
                                'option' =>
                                    array(
                                        'opt1913733' =>
                                            array(
                                                'calc_value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'value' => 'Ja, ik ga akkoord met de algemene voorwaarden',
                                                'label' => pll__('Ja, ik ga akkoord met de algemene voorwaarden'),
                                            ),
                                    ),
                                'email_identifier' => 0,
                                'personally_identifying' => 0,
                            ),
                    ),
                'fld_6710846' =>
                    array(
                        'ID' => 'fld_6710846',
                        'type' => 'button',
                        'label' => pll__('Verzenden'),
                        'slug' => 'verzenden',
                        'conditions' =>
                            array(
                                'type' => '',
                            ),
                        'caption' => '',
                        'config' =>
                            array(
                                'custom_class' => '',
                                'type' => 'submit',
                                'class' => 'button',
                                'target' => '',
                            ),
                    ),
            ),
        'page_names' =>
            array(
                0 => 'Page 1',
            ),
        'mailer' =>
            array(
//                'on_insert' => 1,
                'sender_name' => 'Caldera Forms Notification',
                'sender_email' => 'woutervandewille@gmail.com',
                'reply_to' => '',
                'email_type' => 'html',
                'recipients' => '',
                'bcc_to' => '',
                'email_subject' => pll__('Contact'),
                'email_message' => '{summary}',
            ),
        'variables' =>
            array(
                'keys' =>
                    array(
                        0 => 'type',
                        1 => 'dealer',
                    ),
                'values' =>
                    array(
                        0 => 'contact',
                        1 => get_field('dealer_name', 'options'),
                    ),
                'types' =>
                    array(
                        0 => 'passback',
                        1 => 'passback',
                    ),
            ),
        'settings' =>
            array(
                'responsive' =>
                    array(
                        'break_point' => 'sm',
                    ),
            ),
        'conditional_groups' =>
            array(
                'conditions' =>
                    array(),
            ),
        'processors' =>
            array(),
        'privacy_exporter_enabled' => false,
        'version' => '1.9.2',
        'db_id' => '176',
        'type' => 'primary',
        '_external_form' => 1,
    );
});

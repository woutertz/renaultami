/**
 * initMap
 *
 * Renders a Google Map onto the selected jQuery element
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @return  object The map instance.
 */
function initMap($el) {

  // Find marker elements within map.
  var $markers = $el.find('.marker');

  // Create gerenic map.
  var mapArgs = {
    zoom: $el.data('zoom') || 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true
  };
  var map = new google.maps.Map($el[0], mapArgs);

  // Add markers.
  map.markers = [];
  $markers.each(function () {
    initMarker($(this), map);
  });

  // Center map based on markers.
  centerMap(map);

  // Return map instance.
  return map;
}

/**
 * initMarker
 *
 * Creates a marker for the given jQuery element and map.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @param   object The map instance.
 * @return  object The marker instance.
 */
function initMarker($marker, map) {

  // Get position from marker.
  var lat = $marker.data('lat');
  var lng = $marker.data('lng');
  var concession_id = $marker.data('concession-id');
  var latLng = {
    lat: parseFloat(lat),
    lng: parseFloat(lng)
  };


  // Create marker instance.
  var marker = new google.maps.Marker({
    position: latLng,
    map: map,
    icon: $marker.data('icon'),
    concession_id: concession_id
  });

  google.maps.event.addListener(marker, 'click', function () {

    map.setCenter(marker.getPosition());
    map.panBy(-(window.innerWidth / 10), 0);

    $(".concession-info").removeClass('active');
    $(".concession-info[data-id='" + marker.concession_id + "']").addClass('active');
  });

  // Append to reference for later use.
  map.markers.push(marker);

}

/**
 * centerMap
 *
 * Centers the map showing all markers in view.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   object The map instance.
 * @return  void
 */
function centerMap(map) {

  // Create map boundaries from all map markers.
  var bounds = new google.maps.LatLngBounds();
  map.markers.forEach(function (marker) {
    bounds.extend({
      lat: marker.position.lat(),
      lng: marker.position.lng()
    });
  });

  if (map.markers.length == 1) {
    map.setCenter({
      lat: bounds.getCenter().lat(),
      lng: bounds.getCenter().lng() - 0.07, // move marker a bit to the right
    })
    map.zoom = 12
  } else {
    map.fitBounds(bounds, {
      // padding
      left: 600,
      right: 50,
    });
  }
}

let initTimeout = null

$(document).ready(function () {
  initTimeout = setTimeout(() => {
    initialize()
  }, 1000)
});

// Render maps on facet refresh
$(document).on('facetwp-loaded', function () {
  clearTimeout(initTimeout)
  initialize()
});

function initialize() {
  $('.map__acf').each(function () {
    initMap($(this));
  });
}

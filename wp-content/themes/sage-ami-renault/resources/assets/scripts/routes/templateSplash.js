export default {
  init() {

    $('.play-on-hover').hover(playVideo, stopVideo)

    function playVideo(e) {
      // $(this).find('video').get(0).play()

      const id = $(this).attr('id')
      $('.splash__item')
        .filter((i, el) => $(el).attr('id') !== id)
        .addClass('darken')
    }

    function stopVideo(e) {
      // $(this).find('video').get(0).pause()
      $('.splash__item').removeClass('darken')
    }


  },
  finalize() {
  }
}

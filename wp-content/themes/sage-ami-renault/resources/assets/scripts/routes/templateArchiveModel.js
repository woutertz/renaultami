import Gumshoe from 'gumshoejs';

export default {
  init () {

    const nav = document.querySelector('.model-categories__nav');

    // Category nav
    const spy = new Gumshoe('.model-categories__nav a', {
      offset: function () {
        return nav.getBoundingClientRect().height;
      }
    });

  },
  finalize () {
    // JavaScript to be fired on all pages, after page specific JS is fired

  },
};

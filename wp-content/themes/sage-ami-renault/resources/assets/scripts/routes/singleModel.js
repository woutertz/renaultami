import Gumshoe from 'gumshoejs';
import Swiper from "swiper";

export default {
  init () {

    const versionSlider = new Swiper('.version-slider', {
      loop: true,
      slidesPerView: 1,
      spaceBetween: 20,
      slidesPerGroup: 1,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.slider__next',
        prevEl: '.slider__prev',
      },

      breakpoints: {
        768: {
          slidesPerView: 2,
          spaceBetween: 20,
          slidesPerGroup: 2,
        },
        1200: {
          slidesPerView: 3,
          spaceBetween: 30,
          slidesPerGroup: 3,
        }
      }
    });

    const stockSlider = new Swiper('.stock-slider', {
      loop: true,
      slidesPerView: 1,
      spaceBetween: 20,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.slider__next',
        prevEl: '.slider__prev',
      },

      breakpoints: {
        576: {
          slidesPerView: 2,
          spaceBetween: 20,
          slidesPerGroup: 2,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 20,
          slidesPerGroup: 3,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 20,
          slidesPerGroup: 4,
        }
      }
    });

    const gallerySlider = new Swiper('.gallery-slider', {
      loop: false,
      slidesPerView: 1.5,
      spaceBetween: 15,
      centeredSlides: true,
      freeMode: true,
      initialSlide: 5,
      speed: 500,
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.slider__next',
        prevEl: '.slider__prev',
      },

      breakpoints: {
        768: {
          slidesPerView: 2.5,
          spaceBetween: 15,
        },
        1200: {
          slidesPerView: 2.5,
          spaceBetween: 15,
        },
      }
    });

    let hiddenClicked = false;
    $('.financial-disclaimer').removeClass('is-hidden');

    /**
     $(window).scroll(function () {
      if ($(window).scrollTop() > 1 && !hiddenClicked) {
        $('.financial-disclaimer').removeClass('is-hidden');
      } else {
        $('.financial-disclaimer').addClass('is-hidden');
      }
    });
     */

    $('.financial-disclaimer__close').click(function () {
      $('.financial-disclaimer').addClass('is-hidden');
      hiddenClicked = true;
    })

    $('.js-lease-box-switch').change(function () {
      if (this.checked) {
        $('.cascade__financial-lease:nth-of-type(1), .cascade__financial-lease-title:nth-of-type(1)').addClass('d-none');
        $('.cascade__financial-lease:nth-of-type(2), .cascade__financial-lease-title:nth-of-type(2)').removeClass('d-none');
      } else {
        $('.cascade__financial-lease:nth-of-type(1), .cascade__financial-lease-title:nth-of-type(1)').removeClass('d-none');
        $('.cascade__financial-lease:nth-of-type(2), .cascade__financial-lease-title:nth-of-type(2)').addClass('d-none');
      }
    });


  },
  finalize () {
    // JavaScript to be fired on all pages, after page specific JS is fired

  },
};

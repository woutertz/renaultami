import Cookies from 'js-cookie';

export default {
  init () {

    let base_compare_url = $('.js-compare-go').attr('href');

    updateSaved();
    updateCompared();

    $(document).on('facetwp-loaded', function () {
      updateSaved();
      updateCompared();
    });


    /**
     *  Wishlist / Saved cars
     */
    function updateSaved () {
      let saved_cookie = Cookies.get('saved') ? JSON.parse(Cookies.get('saved')) : [];
      let url = '?_saved=';

      for (let i in saved_cookie) {
        $('.stock-item[data-slug="' + saved_cookie[i] + '"]').addClass('saved');
        url += saved_cookie[i] + '%2C';
      }
      $('.js-stock-save-link').attr('href', function (href, index) {
        return '//' + location.host + location.pathname + url;
      });


      $('.js-stock-save-link .counter').addClass('pulse').text(saved_cookie.length);
      $('.js-stock-save-link .counter').on("animationend", function () {
        $(this).removeClass('pulse');
      });

      if (window.location.href.indexOf('_saved') !== -1) {
        $('.stock-action__save').addClass('active');
      }

      if (saved_cookie.length < 1) {
        $('.js-stock-save-link').removeClass('active');
      }

      $('.js-stock-save-link.active').attr('href', function (href, index) {
        return '//' + location.host + location.pathname;
      });
    }


    $(document).on('click', '.js-stock-save', function (e) {
      e.preventDefault();

      let item = $(this).closest('.stock-item');
      let slug = item.data('slug').toString();
      let saved = item.hasClass('saved');
      let saved_cookie = Cookies.get('saved') ? JSON.parse(Cookies.get('saved')) : [];

      if (saved) {
        item.removeClass('saved');
        saved_cookie = saved_cookie.filter(e => e !== slug);
      } else {
        item.addClass('saved');
        saved_cookie.push(slug);
      }

      let saved_cookie_filtered = saved_cookie.filter(function (item, pos) {
        return saved_cookie.indexOf(item) === pos;
      })

      Cookies.set('saved', JSON.stringify(saved_cookie_filtered), {
        expires: 7,
        path: ''
      });

      updateSaved();

    });


    /**
     *  Compare cars click
     */
    $('.js-stock-compare').click(function (e) {
      e.preventDefault();

      let item = $(this).closest('.stock-item');
      let id = parseInt(item.attr('data-id'));

      let compared = item.hasClass('compared');
      let compared_cookie = Cookies.get('compared') ? JSON.parse(Cookies.get('compared')) : [];

      if (compared) {
        item.removeClass('compared');
        compared_cookie = compared_cookie.filter(e => e !== id);
      } else {
        item.addClass('compared');
        compared_cookie.push(id);
      }

      let compared_cookie_filtered = compared_cookie.filter(function (item, pos) {
        return compared_cookie.indexOf(item) === pos;
      })

      Cookies.set('compared', JSON.stringify(compared_cookie_filtered), {
        expires: 7,
        path: ''
      });

      updateCompared();
    });

    /**
     *  Updated compared cookies list
     */
    function updateCompared () {

      $('.js-compare-boxes .compare-box').removeClass('has-car');

      // Get cookie
      let compared_cookie = Cookies.get('compared') ? JSON.parse(Cookies.get('compared')) : [];

      // Setup url
      let url = '?q=';

      // Reset classes
      $('.stock-item').removeClass('compared');
      // Reset data id
      $('.compare-box').attr('data-id', '');

      // Check items and set compared
      compared_cookie.forEach(function (value, i) {
        let stock_item = $('.stock-item[data-id="' + value + '"]');
        let box = $(' .js-compare-boxes .compare-box:nth-child(' + (i + 1) + ')');


        stock_item.addClass('compared');
        url += value + ',';

        box.addClass('has-car');
        box.attr('data-id', value);
        box.find('.compare__car-image img').attr('src', stock_item.find('.stock-item__image img').attr('src'));
        box.find('.compare__car-title').text(stock_item.find('.stock-item__title').text());
        box.find('.compare__car-description').text(stock_item.find('.stock-item__subtitle').text());
        box.find('.compare__car-price').text(stock_item.find('.stock-item__price').text());
      });

      // Set url of button
      $('.js-compare-go').attr('href', function (href, index) {
        return base_compare_url + url;
      });


      // Set counter
      $('.js-stock-compare-link .counter').addClass('pulse').text(compared_cookie.length);
      $('.js-stock-compare-link .counter').on("animationend", function () {
        $(this).removeClass('pulse');
      });

      // Button disabled
      if (compared_cookie.length > 1) {
        $('.js-compare-go').removeClass('button--disabled');
      } else {
        $('.js-compare-go').addClass('button--disabled');
      }


    };


    /**
     *  Compare window open
     */
    $('.js-stock-compare-link').click(function (e) {
      e.preventDefault();
      FWP.reset();
      $('.stock-compare, .js-stock-compare-link').toggleClass('active');
    });

    /**
     *  Compare window close
     */
    $('.js-compare-close').click(function (e) {
      e.preventDefault();
      $('.stock-compare, .js-stock-compare-link').removeClass('active');
    })

    /**
     * Remove car from compare window
     */
    $('.js-compare-remove').click(function (e) {
      e.preventDefault();

      let item = $(this).closest('.compare-box');
      let id = parseInt(item.attr('data-id'));


      let compared_cookie = Cookies.get('compared') ? JSON.parse(Cookies.get('compared')) : [];

      compared_cookie = compared_cookie.filter(e => e !== id);

      let compared_cookie_filtered = compared_cookie.filter(function (item, pos) {
        return compared_cookie.indexOf(item) === pos;
      });

      Cookies.set('compared', JSON.stringify(compared_cookie_filtered), {
        expires: 7,
        path: ''
      });


      updateCompared();
    });

    /**
     *  Refresh styles on filter
     */
    $(document).on('facetwp-refresh', function () {
      $('.facetwp-template').addClass('loading')
    });

    /**
     * Mobile stock filters
     */
    $('.stock-filters__mobile').click(function () {
      $('.stock-filters').toggle();
    });


  },
  finalize () {
    // JavaScript to be fired on all pages, after page specific JS is fired
    $(document).on('facetwp-loaded', function () {
      $('.facetwp-template').removeClass('loading');
      $('.stock-archive-wrap').removeClass('d-none');
    });

  },
};

import Swiper from "swiper";

export default {
  init () {

    var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 15,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      slidesPerView: 1,
      centeredSlides: true,
      initialSlide: 1,
    });

    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 15,
      slidesPerView: 3,
      slideToClickedSlide: true,
      centeredSlides: true,
      initialSlide: 1,
    });

    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;


  },
  finalize () {
    // JavaScript to be fired on all pages, after page specific JS is fired

  },
};
